import alun.genepi.LociVariables;
import alun.genepi.MaxLoder;
import alun.genio.LinkageDataSet;
import alun.genio.LinkageInterface;
import alun.util.StringFormatter;

/**
 * This program calculates two point lod scores on a grid of points.
 * <ul>
 * Usage : <b> java TwoPointLods input.par input.ped </b> </li>
 * </ul>
 * where
 * <ul>
 * <li> <b> input.par </b> is a LINKAGE parameter file </li>
 * <li> <b> input.ped </b> is a LINKAGE pedigree file </li>
 * </ul>
 * The results are written to the standard output file as
 * a bare table. There is a line of output for each marker
 * containing the lod score for recombination fractions of
 * <ul><b> 0.0, 0.01, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 0.99, 1.0 </b> </ul>
 * respectively.
 */

public class TwoPointLods {
  public static void main(String[] args) {
    try {
      double[] thetas = {0.0, 0.01, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 0.99, 1.0};
/*
			thetas = new double[101];
			for (int i=0; i<thetas.length; i++)
				thetas[i] = i/(thetas.length-1.0);
*/

      LinkageDataSet[] data = null;

      switch (args.length) {
        case 2:
          LinkageDataSet x = new LinkageDataSet(args[0], args[1]);
          data = x.splitByPedigree();
          for (int i = 0; i < data.length; i++) {
            data[i].downCode(false);
          }
          break;
        default:
          System.err.println("Usage: java MaxTwoPointLods input.par input.ped");
          System.exit(1);
      }

      LociVariables[] lvs = new LociVariables[data.length];
      for (int i = 0; i < data.length; i++) {
        lvs[i] = new LociVariables(new LinkageInterface(data[i]));
      }

      for (int j = 1; j < lvs[0].nLoci(); j++) {
        MaxLoder ml = new MaxLoder(lvs, j);
        for (int i = 0; i < thetas.length; i++) {
          System.out.print(" " + StringFormatter.format(ml.f(thetas[i]), 2, 4));
        }
        System.out.println();
      }
    } catch (Exception e) {
      System.err.println("Caught in TwoPointLods:main()");
      e.printStackTrace();
    }
  }
}
