import alun.genepi.ObligatoryErrors;
import alun.genio.GeneticDataSource;
import alun.genio.LinkageDataSet;
import alun.genio.LinkageInterface;

/**
 * This program checks genotype data on pedigrees and indicates whether
 * the data at any particular locus is inconsistent with Mendelian
 * inheritance.
 *
 * <ul>
 * Usage : <b> java ObligateErrros input.par input.ped </b> </li>
 * </ul>
 * where
 * <ul>
 * <li> <b> input.par </b> is a LINKAGE parameter file </li>
 * <li> <b> input.ped </b> is a LINKAGE pedigree file </li>
 * </ul>
 *
 * <p>
 * The program works through each pedigree and locus in turn and writes
 * a list of errors to the standard output file.
 * <p>
 * A more complete analysis which finds the posterior error probabilities for
 * each genotype call, and hence makes finding the source of error easier, is done by
 * <a href="CheckErrors.html"> CheckErrors </a>.
 */

public class ObligateErrors extends ObligatoryErrors {
  public static void main(String[] args) {
    try {
      LinkageDataSet x = null;

      switch (args.length) {
        case 2:
          x = new LinkageDataSet(args[0], args[1]);
          x.downCode(false);
          break;
        default:
          System.err.println("Usage: java ObligateErrors input.par input.ped");
          System.exit(1);
      }

      LinkageDataSet[] xx = x.splitByPedigree();
      for (int i = 0; i < xx.length; i++) {
        GeneticDataSource d = new LinkageInterface(xx[i]);
        for (int j = 0; j < d.nLoci(); j++) {
          if (obligatoryError(d, j)) {
            System.out.println("Pedigree " + (1 + i) + " " + xx[i].name()
              + "\tlocus " + (1 + j) + " " + d.locName(j) + "\tobligatory error");
          }
        }
      }
    } catch (Exception e) {
      System.err.println("Caught in ObligateErrors:main().");
      e.printStackTrace();
    }
  }
}
