import alun.genio.LinkageDataSet;

import java.io.FileWriter;
import java.io.PrintWriter;

/**
 * This program reduces the number of alleles in the models for
 * the given genetic loci to include  only alleles that appear in
 * the pedigree file plus one catchall unseen alleles that has
 * the total frequency of the original unseen alleles.
 *
 * <ul>
 * Usage : <b> java DownCodeAlleles input.par input.ped [output.par] [output.ped] </b> </li>
 * </ul>
 * where
 * <ul>
 * <li> <b> input.par </b> is the original LINKAGE parameter file </li>
 * <li> <b> input.ped </b> is the original LINKAGE pedigree file </li>
 * <li> <b> output.par </b> is the file where the new down coded parameters will be put.
 * The default is to write to standard output.</li>
 * <li> <b> output.ped </b> is the file where the new down coded pedigree data will be put.
 * The default is to write to standard output.</li>
 * </ul>
 */

public class DownCodeAlleles {
  public static void main(String[] args) {
    try {
      int harddowncode = 2;

      LinkageDataSet ld = null;
      PrintWriter parout = new PrintWriter(System.out);
      PrintWriter pedout = new PrintWriter(System.out);

      switch (args.length) {
        case 5:
          if (args[4].equals("-vs")) {
            harddowncode = 0;
          }
          else if (args[4].equals("-s")) {
            harddowncode = 1;
          }
        case 4:
          pedout = new PrintWriter(new FileWriter(args[3]));
        case 3:
          parout = new PrintWriter(new FileWriter(args[2]));
        case 2:
          ld = new LinkageDataSet(args[0], args[1]);
          break;
        default:
          System.err.println("Usage: java DownCodeAlleles input.par input.ped [output.par] [output.ped}");
          System.exit(0);
      }

      ld.downCode(harddowncode);
      parout.println(ld.getParameterData());
      parout.flush();
      pedout.println(ld.getPedigreeData());
      pedout.flush();
    } catch (Exception e) {
      System.err.println("Caught in DownCodeAlleles:main()");
      e.printStackTrace();
    }
  }
}
