/**
 * This program finds approximations to the maximum likelihood estimates of
 * haplotype frequencies from a sample of genotyped individuals.
 * <ul>
 * Usage : <b> java ApproxGCHap input.par input.ped [threshold] [max_iterations] </b>
 * </ul>
 * where
 * <ul>
 * <li> <b> input.par </b> is a LINKAGE parameter file </li>
 * <li> <b> input.ped </b> is a LINKAGE pedigree file. Only the
 * genotype data from this file are used, the individuals are
 * assumed to be unrelated.</li>
 * <li> <b> threshold </b> is a minimum value for a haplotype frequency. At each iteration
 * any haplotype with a lower estimated frequency will be eliminated to save time
 * and space. The default value is 0. </li>
 * <li> <b> max_iterations </b> is an upper bound on the number of iteration performed at
 * each stage of the algorithm. The default value is infinite, in which case
 * the algorithm continues to convergence. </li>
 *
 * <p>
 * Like the program GCHap, the program uses the gene counting method, or EM algorithm,
 * to iterate between reconstructing the haplotypes and estimating
 * haplotype frequencies. However, to save time and space haplotypes with
 * low frequency are eliminated at each stage.
 *
 * <p>
 * The results are written to the standard output file.
 * There is a line for each haplotype with a positive frequency MLE,
 * the frequency is given first in the line followed by the alleles
 * of the haplotype.
 * These are followed by two lines for each individual giving the
 * reconstructed haplotypes, one haplotype on each line.
 */
public class ApproxGCHap extends alun.gchap.ApproxGCHap {
}
