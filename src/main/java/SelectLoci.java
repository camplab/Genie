import alun.genio.LinkageDataSet;

import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.Vector;

/**
 * This program selects a subset of loci from LINKAGE parameter and pedigree
 * files.
 *
 * <ul>
 * Usage : <b> java SelectLoci input.par input.ped output.par output.ped l1 [l2] ...</b> </li>
 * </ul>
 * where
 * <ul>
 * <li> <b> input.par </b> is the original LINKAGE parameter file </li>
 * <li> <b> input.ped </b> is the original LINKAGE pedigree file </li>
 * <li> <b> output.par </b> is the file where the selected loci will be put.</li>
 * <li> <b> output.ped </b> is the file where the matching pedigree data will be put.</li>
 * <li> <b> l1 [l2] ... </b> is a list of at least one locus index. The indexes match the order of the
 * loci in the input parameter file. The first locus has index 0, the last of n has index n-1.</li>
 * </ul>
 */

public class SelectLoci {
  public static void main(String[] args) {
    try {
      if (args.length < 5) {
        System.err.println("Usage: java SelectLoci input.par input.ped output.par output.ped l1 [l2] ...");
      }

      LinkageDataSet l = new LinkageDataSet(args[0], args[1]);
      PrintWriter parout = new PrintWriter(new FileWriter(args[2]));
      PrintWriter pedout = new PrintWriter(new FileWriter(args[3]));

      Vector<Integer> out = new Vector<Integer>();
      for (int i = 4; i < args.length; i++) {
        if (args[i].contains("-")) {
          String[] s = args[i].split("-");
          int a = new Integer(s[0]).intValue();
          int b = new Integer(s[1]).intValue();
          for (int k = a; k <= b; k++) {
            out.add(new Integer(k));
          }
        }
        else {
          out.add(new Integer(args[i]));
        }
      }

      int[] x = new int[out.size()];
      for (int i = 0; i < x.length; i++) {
        x[i] = out.get(i).intValue();
      }

      l = new LinkageDataSet(l, x);
      parout.print(l.getParameterData());
      parout.flush();
      pedout.print(l.getPedigreeData());
      pedout.flush();
    } catch (Exception e) {
      System.err.println("Caught in SelectLoci.main()");
      e.printStackTrace();
    }
  }
}
