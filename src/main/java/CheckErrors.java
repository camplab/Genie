import alun.genepi.Error;
import alun.genepi.*;
import alun.genio.GeneticDataSource;
import alun.genio.LinkageDataSet;
import alun.genio.LinkageInterface;
import alun.markov.Function;
import alun.markov.GraphicalModel;
import alun.util.StringFormatter;

/**
 * This program finds the posterior probabilities of errors for
 * genotypes on individuals in pedigrees given a set of observed genotypes
 * and the pedigree structure.
 * <ul>
 * Usage : <b> java CheckErrors input.par input.ped [thresh1] [thresh2] [prior]</b>
 * </ul>
 * where
 * <ul>
 * <li> <b> input.par </b> is a LINKAGE parameter file </li>
 * <li> <b> input.ped </b> is a LINKAGE pedigree file </li>
 * <li> <b> thresh1 </b> specifies the minimum value for the overall probability that there
 * is at least one genotyping error at a particular locus above which errors are reported. The default is 50%. </li>
 * <li> <b> thresh2 </b> specifies the minimum value for reporting individual error probabilities, given that the
 * overall error probability exceeds <b> thresh1 </b>. The default is 25%. </li>
 * <li> <b> prior </b> is the prior probability that any genotyping call is in error. The default is 1%.</li>
 * </ul>
 *
 * <p>
 * This analyzes the data in the input files, locus by locus and kindred by kindred.
 * It computes the posterior probability that each genotype call is in error using
 * a standard graphical modeling forward-backward algorithm.
 * It replaces the program <a href="GMCheck.html"> GMCheck </a>.
 * <p>
 * For large or complex pedigrees, or large (> 15) numbers of alleles per locus
 * use the <b> -Xms </b> and <b> -Xmx </b> options to increase the space available to the java program.
 * <p>
 * The method is described in
 * <a href="http://bioinformatics.oxfordjournals.org/cgi/reprint/bti485?ijkey=KcqzKdbCHLJPeV2&keytype=ref">
 * Thomas (2005), Bioinformatics.
 * </a>
 */

public class CheckErrors extends FindErrorProbs {
  public static void main(String[] args) {
    try {
      LinkageDataSet x = null;
      double overallthresh = 0.5;
      double margthresh = 0.25;
      double prior = 0.01;
      int[] zero = {0};
      int[] zeroone = {0, 1};
      int[] one = {1};

      switch (args.length) {
        case 5:
          prior = new Double(args[4]).doubleValue();

        case 4:
          margthresh = new Double(args[3]).doubleValue();

        case 3:
          overallthresh = new Double(args[2]).doubleValue();

        case 2:
          x = new LinkageDataSet(args[0], args[1]);
          break;

        default:
          System.err.println("Usage: java CheckErrors parfile pedfile [main_threshold] [marginal_threshold] [error_prior]");
          System.exit(1);
      }

      LinkageDataSet[] xx = x.splitByPedigree();

      for (int i = 0; i < xx.length; i++) {
        GeneticDataSource d = new LinkageInterface(xx[i]);
        for (int j = 0; j < d.nLoci(); j++) {
          System.err.println("LOCUS " + j);
          double[] freqs = d.alleleFreqs(j);
          double[] back = new double[freqs.length];
          for (int k = 0; k < back.length; k++) {
            back[k] = freqs[k];
          }
          xx[i].countAlleleFreqs(j);
          freqs = d.alleleFreqs(j);
          for (int k = 0; k < back.length; k++) {
            if (freqs[k] > 0) {
              freqs[k] = back[k];
            }
          }

          try {
            LocusVariables g = new LocusVariables(d, j, prior);
            GraphicalModel m = null;

            // Reduce the state space.
            m = new GenotypeModel(g.makeUnrelatedProduct());
            m.allocateOutputTables();
            m.allocateInvolTables();
            m.reduceStates();

            // Set up graphical model and tracked variables.
            m = new GenotypeModel(g.makeLocusProduct());

            Genotype[] geno = g.genotypes();
            Error[] erro = g.errors();

            // Find the likelihood of no error.
            for (int k = 0; k < erro.length; k++) {
              if (erro[k] != null) {
                erro[k].setStates(zero);
              }
            }

            m.allocateOutputTables();
            double pnoerror = m.peel();
            boolean oblig = (pnoerror <= Double.MIN_VALUE);

            // Find the total likelihood.
            for (int k = 0; k < erro.length; k++) {
              if (erro[k] != null) {
                erro[k].setStates(zeroone);
              }
            }

            m.allocateOutputTables();
            double allprob = m.peel();

            // Hence get probability of clean data.
            pnoerror /= allprob;

            if (pnoerror < overallthresh) {
              System.out.println("Pedigree (#" + (1 + i) + ") " + x.name() + "\tlocus (#" + (1 + j) + ") " + d.locName(j));
              if (oblig) {
                System.out.println("\t Obligatory error");
              }

              System.out.println
                (
                  "\t P(at least one error) = "
                    + StringFormatter.format((1 - pnoerror), 2, 3) + " > "
                    + StringFormatter.format(1 - overallthresh, 2, 3)
                );
              System.out.print("\t Most probable individual(s) in error: ");

              // Find most probable error congiguration.
              m.allocateInvolTables();
              m.max();
              m.drop();

              for (int k = 0; k < erro.length; k++) {
                if (erro[k] != null) {
                  if (erro[k].getState() == 1) {
                    erro[k].setStates(one);
                    System.out.print(" (#" + (1 + k) + ") " + d.id(k) + ",");
                  }
                  else {
                    erro[k].setStates(zero);
                  }
                }
              }

              // And calculate its probability.
              m.allocateOutputTables();
              double best = m.peel() / allprob;

              System.out.println("   with probability " + StringFormatter.format(best, 2, 4));
              System.out.println("\t Individuals with high posterior error probability:");

              // Compute posterior maringals for error states.
              for (int k = 0; k < erro.length; k++) {
                if (erro[k] != null) {
                  erro[k].setStates(zeroone);
                }
              }

              m.allocateOutputTables();
              m.allocateInvolTables();
              m.findMarginals();

              // And output most probable.
              for (int k = 0; k < erro.length; k++) {
                if (erro[k] != null) {
                  Function f = m.getMarginal(erro[k]);
                  erro[k].setState(1);
                  if (f.getValue() > margthresh) {
                    outputErrorUnordered(d, i, j, k, f.getValue(), geno[k], m.getMarginal(geno[k]));
                  }
                }
              }

              System.out.println();
            }
          } catch (OutOfMemoryError e) {
            System.out.println("Pedigree (#" + (1 + i) + ") " + xx[i].name() + "\tlocus (#" + (1 + j) + ") " + d.locName(j));
            System.out.println("Cannot allocate memory for computation");
          }
        }
      }
    } catch (Exception e) {
      System.err.println("Caught in ErrorProbs:main().");
      e.printStackTrace();
    }
  }
}
