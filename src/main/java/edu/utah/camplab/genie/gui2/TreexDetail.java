package edu.utah.camplab.genie.gui2;

import edu.utah.camplab.genie.util.Ut;

import javax.swing.*;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseListener;

//=============================================================================
public class TreexDetail extends JPanel
  implements ActionListener,
  TreeSelectionListener {
  private static final String SPKG_GUI = Ut.pkgOf(DetailPanel.class);
  private static String ADD_COMMAND = "add";
  private static String REMOVE_COMMAND = "remove";
  private static String COMMIT_COMMAND = "commit";
  public GenieGUI gui;
  protected JTree tree;
  protected DefaultTreeModel treeModel;
  protected DefaultMutableTreeNode rootNode;
  protected Detail rootPanel;
  String title;
  JPanel treepanel, buttonPanel;
  JLayeredPane layeredPane;
  DefaultMutableTreeNode newNode = null;
  JButton addButton, removeButton, commitButton;
  TreexDetail treexdetail;
  //---------------------------------------------------------------------------
  MouseListener ml = new MouseAdapter() {
    /*
    public void mousePressed(MouseEvent e) 
    {
      int selRow = tree.getRowForLocation(e.getX(), e.getY());
      TreePath selPath = tree.getPathForLocation(e.getX(), e.getY());
      if(selRow != -1) 
      {
        if(e.getClickCount() == 1) 
          mySingleClick(selRow, selPath);
        else if(e.getClickCount() == 2) 
          myDoubleClick(selRow, selPath);
      }
    }
    */
  };
  private Toolkit toolkit = Toolkit.getDefaultToolkit();

  //----------------------------------------------------------------------------
  public TreexDetail(String inTitle, GenieGUI gui) {
    //super(new GridLayout(1,0));
    this.gui = gui;
    title = inTitle;
    rootPanel = buildRootPanel();
    rootNode = new DefaultMutableTreeNode(rootPanel, true);
    rootPanel.setGUI(gui);
    rootPanel.setMyNode(rootNode);
    rootPanel.setRootNode(rootNode);
    //rootPanel.setTreexDetail(this);
    treeModel = new DefaultTreeModel(rootNode);
    tree = new JTree(treeModel);
    tree.getSelectionModel().setSelectionMode(
      TreeSelectionModel.SINGLE_TREE_SELECTION);
    tree.addTreeSelectionListener(this);
    tree.addMouseListener(ml);
    tree.setShowsRootHandles(true);
    tree.setExpandsSelectedPaths(true);
    JScrollPane scrollPane = new JScrollPane(tree);

    // File List Panel
    treepanel = new JPanel();
    treepanel.setLayout(new BorderLayout());
    TitledEtched bordertitle = new TitledEtched(title + " Listing");
    treepanel.setBorder(bordertitle.title);

    buttonPanel = new JPanel();
    addButton = new JButton(ADD_COMMAND);
    removeButton = new JButton(REMOVE_COMMAND);
    commitButton = new JButton(COMMIT_COMMAND);
    addButton.setActionCommand(ADD_COMMAND);
    removeButton.setActionCommand(REMOVE_COMMAND);
    commitButton.setActionCommand(COMMIT_COMMAND);
    addButton.addActionListener(this);
    removeButton.addActionListener(this);
    commitButton.addActionListener(this);
    buttonPanel.add(addButton);
    buttonPanel.add(removeButton);
    buttonPanel.add(commitButton);

    treepanel.add(scrollPane, BorderLayout.CENTER);
    treepanel.add(buttonPanel, BorderLayout.SOUTH);

    // Detail Pane
    layeredPane = new JLayeredPane();
    layeredPane.setLayout(new BorderLayout());

    // add treepanel and layeredPane to splitpane
    JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
    splitPane.setLeftComponent(treepanel);
    splitPane.setRightComponent(layeredPane);

    add(splitPane);
  }

  //---------------------------------------------------------------------------
  public TreexDetail() {
    super(new GridLayout(1, 0));
  }

  //-------------------------------------------------------------------------
  private static void createAndShowGUI(DetailPanel panel) {
    JPanel newContentPane = panel;
    newContentPane.setOpaque(true); //content panes must be opaque
    String title = newContentPane.toString();
    JFrame frame = new JFrame(title);
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    //Create and set up the content pane.
    frame.setContentPane(newContentPane);

    //Display the window.
    frame.pack();
    frame.setSize(new Dimension(950, 800));
    //frame.setResizable(false);
    frame.setVisible(true);
  }

  //---------------------------------------------------------------------------

  //---------------------------------------------------------------------------
  public Detail buildRootPanel() {
    return null;
  }

  //---------------------------------------------------------------------------
  public Detail getRootPanel() {
    return rootPanel;
  }

  /**
   * add new node to tree
   */
  public DefaultMutableTreeNode addNode(Object obj, boolean allowsChildren) {
    DefaultMutableTreeNode node = new DefaultMutableTreeNode(obj,
      allowsChildren);
    return addNode(node);
  }

  //---------------------------------------------------------------------------
  public DefaultMutableTreeNode addNode(DefaultMutableTreeNode node) {
    DefaultMutableTreeNode parentNode = null;
    TreePath parentPath = tree.getSelectionPath();
    if (parentPath == null) {
      parentNode = rootNode;
    }
    else {
      parentNode = (DefaultMutableTreeNode) parentPath.getLastPathComponent();
    }
    // check whether parentNode allow children
    //Object parentObj = parentNode.getUserObject();

    if (parentNode.getAllowsChildren()) {
      return addNode(parentNode, node);
    }
    else {
      return null;
    }
    //return addNode(rootNode, node);
  }

  //---------------------------------------------------------------------------
  public DefaultMutableTreeNode addNode(DefaultMutableTreeNode parent,
                                        DetailPanel childpanel) {
    boolean allowsChildren = parent.getAllowsChildren();
    DefaultMutableTreeNode child = new DefaultMutableTreeNode
      (childpanel, allowsChildren);
    return addNode(parent, child);
  }

  //---------------------------------------------------------------------------

  //---------------------------------------------------------------------------
  public DefaultMutableTreeNode addNode(DefaultMutableTreeNode parent,
                                        DefaultMutableTreeNode child) {
    //Object parentObj = parent.getUserObject();
    if (parent == null) {
      parent = rootNode;
    }
    try {
      treeModel.insertNodeInto(child, parent, parent.getChildCount());
    } catch (Exception ee) {
      System.err.println("Error : " + ee.getMessage());
    }
    tree.setModel(treeModel);
    tree.scrollPathToVisible(new TreePath(child.getPath()));
    //DefaultMutableTreeNode lastNode = (DefaultMutableTreeNode) 
    //                                  tree.getLastSelectedPathComponent();
    tree.updateUI();
    return child;
  }

  //---------------------------------------------------------------------------
  public DefaultMutableTreeNode getFirstNode() {
    return (DefaultMutableTreeNode) rootNode.getFirstChild();
  }

  /**
   * return current selected node.
   */
  public DefaultMutableTreeNode getCurrentNode() {
    DefaultMutableTreeNode currentNode = rootNode;
    TreePath currentPath = tree.getSelectionPath();
    if (currentPath != null) {
      currentNode = (DefaultMutableTreeNode) tree.getLastSelectedPathComponent();
    }
    return currentNode;
  }

  //----------------------------------------------------------------------------
  public void removeCurrentNode() {
    DefaultMutableTreeNode currentNode = getCurrentNode();
    MutableTreeNode parent = (MutableTreeNode) (currentNode.getParent());
    if (parent != null) {
      treeModel.removeNodeFromParent(currentNode);
      return;
    }
    // Either there was no selection, or the root was selected.
    toolkit.beep();
  }

  //---------------------------------------------------------------------------
  public void displaySelectedScreen() {
    DefaultMutableTreeNode selectedNode = getCurrentNode();
    if (selectedNode == null) {
      selectedNode = (DefaultMutableTreeNode) rootNode.getFirstChild();
    }
    displaySelectedScreen(selectedNode);
  }

  public void displaySelectedScreen(DefaultMutableTreeNode selectedNode) {
    if (selectedNode == rootNode) {
      selectedNode = (DefaultMutableTreeNode) rootNode.getFirstChild();
    }
    DetailPanel panel = (DetailPanel) selectedNode.getUserObject();
    displaySelectedScreen(panel);
  }

  //----------------------------------------------------------------------------
  /*
  public TreeMap<DefaultMutableTreeNode, DetailPanel> 
		getDetailPanel(DefaultMutableTreeNode inNode ) {
    return getDetailPanel();
  }

  public TreeMap<DefaultMutableTreeNode, DetailPanel> 
		getDetailPanel() {
    TreeMap<DefaultMutableTreeNode, DetailPanel> detailnode =
           new TreeMap<DefaultMutableTreeNode, DetailPanel>();
    int i = 0 ; 
    for ( Enumeration e = rootNode.children(); e.hasMoreElements(); ) {
      DefaultMutableTreeNode node = (DefaultMutableTreeNode) e.nextElement();
      detailnode.put(node, (DetailPanel) node.getUserObject());
    }
    return detailnode;
  }
  */

  public void displaySelectedScreen(DetailPanel panel) {
    panel.setOpaque(true);
    //layeredPane.remove(panel);
    //layeredPane.add(panel);
    layeredPane.updateUI();
    layeredPane.validate();
    layeredPane.moveToFront(panel);
    //if ( panel.isShowing() ) 
    //System.out.println("displaySelectedScreen " + panel.toString() + " is showing");
    //else 
    //System.out.println("displaySelectedScreen " + panel.toString() + " is NOT showing");
  }

  public void displaySelectedScreen(DefaultMutableTreeNode selectedNode,
                                    DetailPanel inScreen) {
    inScreen.setVisible(true);
    inScreen.setOpaque(true);
    layeredPane.moveToFront(inScreen);
    layeredPane.updateUI();
    layeredPane.repaint();
    this.updateUI();
    this.repaint();
  }

  //---------------------------------------------------------------------------
  public void commitedAction() {
  }

  //---------------------------------------------------------------------------
  public void actionPerformed(ActionEvent e) {
    String cmd = e.getActionCommand();
    actionPerformed(cmd);
  }

  public void actionPerformed(String cmd) {
    if (ADD_COMMAND.equals(cmd)) {
      Detail screen = addAction();
    }
    else if (REMOVE_COMMAND.equals(cmd)) {
      DefaultMutableTreeNode selectedNode = getCurrentNode();
      if (selectedNode != rootNode) {
        removeCurrentNode();
        DefaultMutableTreeNode nextNode = (DefaultMutableTreeNode)
          rootNode.getFirstChild();
        displaySelectedScreen(nextNode);
      }
      else {
        JOptionPane.showMessageDialog(this, "Cannot remove this last Component!");
      }
    }
    else if (COMMIT_COMMAND.equals(cmd)) {
      commitedAction();
    }
  }

  //---------------------------------------------------------------------------
  public Detail addAction() {
    DetailPanel detail = null;
    DefaultMutableTreeNode selectedNode = getCurrentNode();
    DefaultMutableTreeNode parentNode = null;
    if (selectedNode == rootNode) {
      parentNode = rootNode;
    }
    else {
      parentNode = (DefaultMutableTreeNode) selectedNode.getParent();
    }

    return addAction(parentNode);
  }

  //---------------------------------------------------------------------------
  public Detail addAction(DefaultMutableTreeNode parentNode) {
    Detail parentPanel = (Detail) parentNode.getUserObject();
    DetailPanel detail = null;
    try {
      detail = (DetailPanel) parentPanel.createChild();
      // remove the obj before adding it to the layer
      DefaultMutableTreeNode node = addNode(parentNode, detail);
      detail.setMyNode(node);
      detail.validate();
      detail.setOpaque(true);
      layeredPane.add(detail);
      ((DetailPanel) parentPanel).updateUI();
      displaySelectedScreen(detail);
      ((DetailPanel) parentPanel).validate();
    } catch (Exception ee) {
      System.out.println(
        "Failed to create Detail Panel, exception : " + ee.getMessage());
    }
    return detail;
  }

  //---------------------------------------------------------------------------
  public void valueChanged(TreeSelectionEvent tse) {
    DefaultMutableTreeNode node = getCurrentNode();
    /*
    if ( node != rootNode )
    {
      DetailPanel panel = (DetailPanel) node.getUserObject();
      TreeNode parentNode = node.getParent();
        displaySelectedScreen(node, panel);
    } else  {
        System.out.println(" value Changed in tree ");
        displaySelectedScreen(node, (DetailPanel) node.getUserObject());
    }
    */
    if (node == null) {
      node = (DefaultMutableTreeNode) rootNode.getFirstChild();
    }
    //DetailPanel panel = (DetailPanel) node.getUserObject();
    displaySelectedScreen(node);
    //createAndShowGUI(panel);
  }

  /*
  public void mousePressed(MouseEvent me)
  {}

  public void mouseExited(MouseEvent me)
  {}

  public void mouseEntered(MouseEvent me)
  {}

  public void mouseReleased(MouseEvent me)
  {System.out.println("mouse release");}

  public void mouseClicked(MouseEvent me)
  {}


  class MyTreeModelListener implements TreeModelListener 
  {
    public void treeNodesChanged(TreeModelEvent e) 
    {
      DefaultMutableTreeNode node;
      node = (DefaultMutableTreeNode)
               (e.getTreePath().getLastPathComponent());

       // If the event lists children, then the changed
       // node is the child of the node we've already
       // gotten.  Otherwise, the changed node and the
       // specified node are the same.

      try 
      {
        int index = e.getChildIndices()[0];
        node = (DefaultMutableTreeNode)
                 (node.getChildAt(index));
      } 
      catch (NullPointerException npx) 
      {}
    }

    public void treeNodesInserted(TreeModelEvent e) {}
    public void treeNodesRemoved(TreeModelEvent e) {}
    public void treeStructureChanged(TreeModelEvent e) {}
  }
  */
}
