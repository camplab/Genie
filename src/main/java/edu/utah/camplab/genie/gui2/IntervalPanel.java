package edu.utah.camplab.genie.gui2;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

//=============================================================================
public class IntervalPanel extends BasicPanel
  implements ActionListener {
  JRadioButton yesRB, noRB;
  ButtonGroup intervalBgroup;

  public IntervalPanel() {
    super("Simulation Interval Check");
    yesRB = new JRadioButton("Yes");
    noRB = new JRadioButton("No");
    yesRB.setActionCommand("yes");
    noRB.setActionCommand("no");
    yesRB.setSelected(true);
    intervalBgroup = new ButtonGroup();
    intervalBgroup.add(yesRB);
    intervalBgroup.add(noRB);
    add(yesRB);
    add(noRB);
    yesRB.addActionListener(this);
    noRB.addActionListener(this);
  }

  public void actionPerformed(ActionEvent e) {
  }

  public String getInterval() {
    return intervalBgroup.getSelection().getActionCommand();
  }

  public void populate(Parameters params) {
    String interval = params.get(Parameters.Att.INTERVALCHECK);
    if (interval.equals("Yes")) {
      yesRB.setSelected(true);
    }
  }
}
