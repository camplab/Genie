package edu.utah.camplab.genie.gui2;

import javax.swing.tree.DefaultMutableTreeNode;
import java.util.TreeMap;

public interface DetailPanelInterface {
  public String toString();

  public String getChildName();

  public void buildDetail(int index);

  public void buildChild() throws Exception;

  public void setParentNode(DefaultMutableTreeNode parent);

  public void setMyNode(DefaultMutableTreeNode myNode);

  //public void setTreexDetailPanel ( TreexDetailPanel top );
  //public DetailPanelImp createChildPanel()
  //                      throws Exception; 
  public boolean getAllowsChildren();

  public int getChildCount();

  public int getIndex();

  public String getTitle();

  public TreeMap<DefaultMutableTreeNode, DetailPanelImp> getDetailPanel();

  public TreeMap<DefaultMutableTreeNode, DetailPanelImp>
  getDetailPanel(DefaultMutableTreeNode inNode);

  //public ImageIcon createImageIcon (String path,
  //                                          String description);
  public void incrementChildCount();
}
