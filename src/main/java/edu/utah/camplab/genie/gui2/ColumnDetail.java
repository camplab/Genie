package edu.utah.camplab.genie.gui2;

import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ColumnDetail extends DetailPanel
  implements ActionListener {
  JButton weighthelpB, addGroupB;
  JTextField weightT;
  AnalysisTab.AnalysisPanel topPanel;

  public ColumnDetail() {
    super();
    title = "ColumnDetail";
    childName = "ColumnGroup";
    allowsChildren = true;
  }

  public void build(int inIndex) {
    super.build(inIndex);
    setLayout(new BorderLayout());
    setPreferredSize(new Dimension(600, 400));
    TitledEtched subtitle = new TitledEtched(title + " " + getIndex());
    setBorder(subtitle.title);
    JPanel panel = new JPanel();
    GridBagLayout gridbag = new GridBagLayout();
    panel.setLayout(gridbag);
    GridBagConstraints constraints = new GridBagConstraints();
    int childPaneCount = 0;
    JLabel weightL = new JLabel("Weight");
    weightT = new JTextField(20);
    weightL.setLabelFor(weightT);
    weighthelpB = new JButton(createImageIcon("/toolbarButtonGraphics/general/Help24.gif",
      "column weight help button"));
    weighthelpB.setContentAreaFilled(false);
    weighthelpB.setBorderPainted(false);
    weighthelpB.setHorizontalAlignment(JLabel.LEFT);
    weighthelpB.addActionListener(this);
    weighthelpB.setActionCommand("weighthelp");
    addGroupB = new JButton("Add Pattern Group");
    addGroupB.addActionListener(this);
    constraints.gridx = 10;
    constraints.gridwidth = 3;
    constraints.gridheight = 2;
    constraints.ipadx = 5;
    constraints.weightx = 1.0;
    gridbag.setConstraints(weightL, constraints);
    constraints.gridx = 20;
    constraints.gridwidth = 40;
    gridbag.setConstraints(weightT, constraints);
    constraints.gridx = 80;
    gridbag.setConstraints(weighthelpB, constraints);
    //gridbag.setConstraints(addGroupB, constraints);
    panel.add(weightL);
    panel.add(weightT);
    panel.add(weighthelpB);
    add(panel, BorderLayout.CENTER);
    add(addGroupB, BorderLayout.SOUTH);
    //grouplayer.add(addGroupB);
    //add(wtPanel, BorderLayout.NORTH);
    //add(grouplayer, BorderLayout.CENTER);
  }

  public void actionPerformed(ActionEvent ae) {
    GenieGUI gui = ((AnalysisMain) rootNode.getUserObject()).getGUI();
    Object source = ae.getSource();
    if (source == weighthelpB) {
      JOptionPane.showMessageDialog(this, UIConstants.WEIGHT_MESSAGE);
    }
    else if (source == addGroupB) {
      int wt = 0;
      ColumnGroupDetail screen = null;
      if (weightT.getText().length() > 0) {
        try {
          wt = Integer.parseInt(weightT.getText());
          Detail detail = treexdetail.addAction(myNode);
        } catch (NumberFormatException nfe) {
          JOptionPane.showMessageDialog(this, "Please enter an INTEGER weight value");
          weightT.setText("");
        } catch (Exception ee) {
          System.out.println("Failed to create Column Group Detail " + ee.getMessage());
          System.exit(0);
        }
      }
      else {
        JOptionPane.showMessageDialog(this,
          "Enter weight value for this column");
      }
    }
  }

  public String getWeight() {
    return weightT.getText();
  }

  public void setDisplayOnly() {
    Util.disable(weightT);
  }

  public String toString() {
    return "Column " + myIndex;
  }

  //---------------------------------------------------------------------------
  public void populate(Parameters params, int wt, NodeList colGroup) {
    weightT.setText("" + wt);
    for (int i = 0; i < colGroup.getLength(); i++) {
      ColumnGroupDetail colgroupdetail = (ColumnGroupDetail)
        treexdetail.addAction(myNode);
      Element group = (Element) colGroup.item(i);
      NodeList apattern = params.kidsOf(group, Parameters.Tag.A);
      colgroupdetail.populate(apattern);
    }
  }
}
