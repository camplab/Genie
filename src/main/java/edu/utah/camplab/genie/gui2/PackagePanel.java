package edu.utah.camplab.genie.gui2;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

//==============================================================================
public class PackagePanel extends JPanel
  implements ActionListener {
  JRadioButton hapmcRB, pedGenieRB, hapConstructorRB;
  ButtonGroup pkgBgroup;
  OptionTab parentPanel;
  //VariablesPanel variablesPanel = new VariablesPanel();
  //AlleleFormatPanel alleleformatPanel = new AlleleFormatPanel();
  //MissingAllelePanel missingAllelePanel = new MissingAllelePanel();
  //TopSimPanel  topsimPanel    = new TopSimPanel();
  //SamplingPanel samplingPanel  = new SamplingPanel();
  //ReportPanel    reportPanel    = new ReportPanel();
  //DecodePanel   decodePanel   = new DecodePanel();
  //DumpPanel      dumpPanel      = new DumpPanel();
  //IntervalPanel intervalPanel = new IntervalPanel();
  String packageName = "PedGenie";
  JPanel pkginfo = new JPanel();
  JButton commitB;
  GenieGUI gui;

  //----------------------------------------------------------------------------
  public PackagePanel() {
    super();
    TitledEtched titledEtched = new TitledEtched("Package");
    setBorder(titledEtched.title);
    setPreferredSize(new Dimension(800, 200));
    setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
    pedGenieRB = new JRadioButton("PedGenie");
    hapmcRB = new JRadioButton("hapMC");
    hapConstructorRB = new JRadioButton("hapConstructor");
    pkgBgroup = new ButtonGroup();
    pkgBgroup.add(pedGenieRB);
    pkgBgroup.add(hapmcRB);
    pkgBgroup.add(hapConstructorRB);
    pedGenieRB.setActionCommand("PedGenie");
    pedGenieRB.addActionListener(this);
    hapmcRB.setActionCommand("hapMC");
    hapmcRB.addActionListener(this);
    hapConstructorRB.setActionCommand("hapConstructor");
    hapConstructorRB.addActionListener(this);
    add(pedGenieRB);
    add(hapmcRB);
    add(hapConstructorRB);
  }

  //----------------------------------------------------------------------------
  public void actionPerformed(ActionEvent e) {
    // add new tab and disable buttons
    packageName = e.getActionCommand();
    gui.setPackageName(packageName);
    if (gui.isNew) {
      gui.globalTab.build(gui);
      gui.setSelected(gui.globalTab);
      setDisplayOnly();
    }
    // if (packageName.equalsIgnoreCase("hapmc")) {
    if (packageName.toLowerCase().startsWith("hap")) {
      gui.globalTab.topSimPanel.setHapMC();
    }
    else {
      gui.globalTab.topSimPanel.setPedGenie();
    }
    //gui.addTab("Global");
    //changeDisplay(e.getActionCommand());
    //updateUI();
  }

  //--------------------------------------------------------------------------
  public String toString() {
    return "Package";
  }

  //--------------------------------------------------------------------------
  public void setParentPanel(JPanel parent) {
    parentPanel = (OptionTab) parent;
  }

  //--------------------------------------------------------------------------
  public void setGUI(GenieGUI inGUI) {
    gui = inGUI;
  }

  //--------------------------------------------------------------------------
  public String getPackage() {
    return packageName;
  }

  //--------------------------------------------------------------------------
  public void setDisplayOnly() {
    Util.disable(pkgBgroup);
  }

  //--------------------------------------------------------------------------
  public void populate() {
    Parameters params = gui.getParameters();
    String dropSim = params.get(Parameters.Att.DROP);
    if (dropSim.equals("HapMCDropTogether")) {
      packageName = "hapMC";
      hapmcRB.setSelected(true);
      pedGenieRB.setSelected(false);
    }
    else {
      packageName = "PedGenie";
      hapmcRB.setSelected(false);
      pedGenieRB.setSelected(true);
    }
    gui.setPackageName(packageName);
    //gui.addTab("Global");
  }

  /*
  public void changeDisplay(String pkg)
  {
    parentPanel.removeAll();
    JPanel panel = new JPanel();
    pkgName = pkg;
    panel.setPreferredSize(new Dimension(UIConstants.SCREEN_WIDTH,
                                         UIConstants.SCREEN_HEIGHT));
    panel.setLayout(new BorderLayout());
    panel.setOpaque(true);
    pkginfo.setLayout(new GridLayout(5, 2));
    pkginfo.setOpaque(true);
    commitB = new JButton("Commit and Build Study Tab");
    commitB.addActionListener(this);
    pkginfo.add(variablesPanel);
    if (pkgName.equals("PedGenie")) {
      pkginfo.add(topsimPanel);
    }
    pkginfo.add(alleleformatPanel);
    pkginfo.add(missingAllelePanel);
    pkginfo.add(samplingPanel);
    pkginfo.add(intervalPanel);
    pkginfo.add(reportPanel);
    pkginfo.add(dumpPanel);
    pkginfo.add(decodePanel);
    panel.add(pkginfo, BorderLayout.CENTER);
    panel.add(commitB, BorderLayout.SOUTH);
    parentPanel.add(panel);
  }
  */
}

