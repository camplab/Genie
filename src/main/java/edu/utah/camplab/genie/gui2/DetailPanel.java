package edu.utah.camplab.genie.gui2;

import edu.utah.camplab.genie.util.Ut;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import java.util.Enumeration;

//=============================================================================
public class DetailPanel extends JPanel
  implements Detail {
  private static final String SPKG_GUI = Ut.pkgOf(DetailPanel.class);
  private static final String suffix = "Detail";
  String title = null;
  boolean allowsChildren;
  DefaultMutableTreeNode myNode, parentNode, rootNode;
  TreexDetail treexdetail;
  Detail parentPanel;
  String childName;
  int childIndex = 1;
  int myIndex;
  GenieGUI gui;

  public DetailPanel() {
    super();
  }

  public GenieGUI getGUI() {
    return gui;
  }

  public void setGUI(GenieGUI gui) {
    this.gui = gui;
  }

  public DefaultMutableTreeNode getRootNode() {
    return rootNode;
  }

  //---------------------------------------------------------------------------
  public void setRootNode(DefaultMutableTreeNode node) {
    rootNode = node;
  }

  public DefaultMutableTreeNode getParentNode() {
    return parentNode;
  }

  public void setParentNode(DefaultMutableTreeNode node) {
    parentNode = node;
  }

  public Detail getParentPanel() {
    return parentPanel;
  }

  public void setParentPanel(Detail parentPanel) {
    this.parentPanel = parentPanel;
  }

  public void setMyNode(DefaultMutableTreeNode node) {
    myNode = node;
  }

  public TreexDetail getTreexDetail() {
    return treexdetail;
  }

  public void setTreexDetail(TreexDetail treexdetail) {
    this.treexdetail = treexdetail;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String inTitle) {
    title = inTitle;
  }

  public String toString() {
    return title;
  }

  //public void setOpaque(boolean opaque) {
  //  JPanel thisPanel = (JPanel) this;
  // thisPanel.setOpaque(opaque);
  //}

  //---------------------------------------------------------------------------
  public void incrementChildIndex() {
    childIndex++;
  }

  public int getChildIndex() {
    return childIndex;
  }

  public int getIndex() {
    return myIndex;
  }

  public void setIndex(int index) {
    myIndex = index;
  }

  public boolean getAllowsChildren() {
    return allowsChildren;
  }

  //---------------------------------------------------------------------------
  public void setAllowsChildren(boolean inBoolean) {
    allowsChildren = inBoolean;
  }

  public String getChildName() {
    return childName;
  }

  public void setChildName(String childName) {
    this.childName = childName;
  }

  //---------------------------------------------------------------------------
  public Detail createChild() throws Exception {
    Object newPanel = Ut.newModule(SPKG_GUI, childName, suffix);
    Detail childDetail = (Detail) newPanel;
    //childDetail.setIndex(getChildIndex());
    childDetail.setParentNode(myNode);
    childDetail.setParentPanel(this);
    childDetail.setRootNode(rootNode);
    childDetail.setTreexDetail(treexdetail);
    childDetail.build(getChildIndex());
    DetailPanel child = (DetailPanel) childDetail;
    child.setOpaque(true);
    child.setVisible(true);
    child.updateUI();
    incrementChildIndex();
    return childDetail;
  }

  //---------------------------------------------------------------------------
  public void build(int index) {
    myIndex = index;
  }

  //---------------------------------------------------------------------------
  public DetailPanel[] getChildDetail() {
    DetailPanel[] list = new DetailPanel[myNode.getChildCount()];
    int i = 0;
    for (Enumeration e = myNode.children(); e.hasMoreElements(); ) {
      DefaultMutableTreeNode node = (DefaultMutableTreeNode) e.nextElement();
      list[i] = (DetailPanel) node.getUserObject();
      i++;
    }
    return list;
  }

  //---------------------------------------------------------------------------
  public ImageIcon createImageIcon(String path,
                                   String description) {
    java.net.URL imgURL = getClass().getResource(path);
    if (imgURL != null) {
      return new ImageIcon(imgURL, description);
    }
    else {
      System.err.println("Couldn't find file: " + path);
      return null;
    }
  }
}
