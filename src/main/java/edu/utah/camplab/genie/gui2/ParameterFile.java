package edu.utah.camplab.genie.gui2;

import edu.utah.camplab.genie.gm.LocusImp;

import java.io.*;

//==============================================================================
public class ParameterFile {
  public File rgen;
  public GlobalTab globaltab;
  public StudyDetail[] studydetails;
  public LocusImp[] globalLocus;
  public Statistic[] globalStatistic;
  public Statistic[] globalMeta;
  public String decode = "no";
  private PrintWriter pw;
  private StudyTab studytab;
  private StatisticTab statistictab;
  private LocusTab locustab;
  private AnalysisTab analysistab;

  //----------------------------------------------------------------------------
  public ParameterFile(File infile,
                       GlobalTab globalt,
                       StudyTab studyt,
                       StatisticTab statistict,
                       LocusTab locust,
                       AnalysisTab analysist) throws IOException {
    rgen = infile;
    globaltab = globalt;
    studytab = studyt;
    statistictab = statistict;
    locustab = locust;
    analysistab = analysist;

    StatisticDetail statisticdetail = statistictab.getStatisticDetail();
    MetaDetail metadetail = statistictab.getMetaDetail();
    globalLocus = locustab.getSelectedLocus();
    globalStatistic = statisticdetail.getSelectedStat();
    globalMeta = metadetail.getSelectedStat();
    pw = new PrintWriter(new BufferedWriter(new FileWriter(rgen)), true);
    writeHeader();
    writeLocus();
    writeDataFile();
    writeStatistic();
    writeSampling();
    writeCCtable();
    closepw();
  }

  public String getName() {
    return rgen.toString();
  }

  public void writeHeader() {
    String nsims = globaltab.getNsim();
    String seed = globaltab.getRseed();
    String top = globaltab.getTop();
    String drop = globaltab.getDrop();
    String report = globaltab.getReport();
    String alleleformat = globaltab.getAlleleFormat();
    String interval = globaltab.getInterval();
    decode = globaltab.getDecode();

    pw.println("<?xml version=\"1.0\" encoding=\"iso-8859-1\"?>");
    pw.println("<!DOCTYPE ge:rgen SYSTEM \"ge-rgen.dtd\">");
    pw.println();
    pw.println("<ge:rgen rseed=\"" + seed + "\" nsims=\"" + nsims + "\" top=\"" + top + "\" drop=\"" + drop + "\" report=\"" + report + "\" alleleformat=\"" + alleleformat + "\" decode=\"" + decode + "\" intervalcheck=\"" + interval + "\">");
    pw.println();
  }

  public void writeLocus() {
    for (int i = 0; i < globalLocus.length; i++) {
      int locusID = globalLocus[i].getID();
      String locusName = globalLocus[i].getMarker();
      double dist = globalLocus[i].getTheta();
      //int gene = globalLocus[i].getGeneID();
      pw.print("<ge:locus id=\"" + locusID + "\" marker=\"" + locusName + "\"");
      if (dist != 0.0) {
        pw.print(" dist=\"" + dist + "\"");
      }
      //if ( gene != -1 )
      //  pw.print(" gene=\"" + gene + "\"");
      pw.print("/>");
      pw.println();
    }
  }

  public void writeDataFile() {
    if (decode.startsWith("y") || decode.startsWith("Y")) {
      String decodefile = globaltab.getDecodeFile();
      if (decodefile.length() > 0 &&
        !decodefile.equals("No File Selected")) {
        pw.println("<ge:datafile decodefile=\"" + decodefile + "\"/>");
      }
    }

    Detail studyMain = studytab.studyPanel.getRootPanel();
    Detail[] detailPanel = studyMain.getChildDetail();
    for (int i = 0; i < detailPanel.length; i++) {
      StudyDetail studydetail = (StudyDetail) detailPanel[i];
      String study = studydetail.getStudyName().trim();
      String genofile = studydetail.getGenoFile();
      String hapfreqfile = studydetail.getHapFreq();
      String varfile = studydetail.getVariableFile();
      String parfile = studydetail.getLinkageParFile();

      // no need to add study i, it appears on the screen
      //if (study.length() == 0)
      //  study = "study " + i;

      if (genofile.length() == 0) {
        System.out.println("No genotype data file selected, please reset start the GUI.");
        break;
      }

      pw.print("<ge:datafile studyname=\"" + study + "\" genotypedata=\"" +
        genofile + "\"");
      if (!hapfreqfile.equals("No File Selected")) {
        pw.print(" haplotype=\"" + hapfreqfile + "\"");
      }
      if (!varfile.equals("No File Selected")) {
        pw.print(" quantitative=\"" + varfile + "\"");
      }
      if (!parfile.equals("No File Selected")) {
        pw.print(" linkageparameter=\"" + parfile + "\"");
      }
      pw.print("/>");
      pw.println();
    }
  } // end writedatafile

  public void writeStatistic() {
    for (int i = 0; i < globalStatistic.length; i++) {
      String statID = "ccstat" + i;
      pw.println("<ge:param name=\"" + statID + "\">" + globalStatistic[i].programName + "</ge:param>");
    }
    for (int i = 0; i < globalMeta.length; i++) {
      String metaID = "metastat" + i;
      pw.println("<ge:param name=\"" + metaID + "\">" + globalMeta[i].programName + "</ge:param>");
    }
  } // end writestatistic

  public void writeSampling() {
    String sampling = globaltab.samplingPanel.getMethod();
    pw.println("<ge:param name=\"top-sample\">" + sampling + "</ge:param>");
  }

  public void writeCCtable() {
    Detail analysisMain = analysistab.analysisPanel.getRootPanel();
    Detail[] detailPanel = analysisMain.getChildDetail();
    for (int i = 0; i < detailPanel.length; i++) {
      AnalysisDetail analysisdetail = (AnalysisDetail) detailPanel[i];
      String locus = "";
      String statStr = "";
      String metaStr = "";
      String space = "";
      //JPanel infoTab = analysisdetail[i].getTab("info");
      String model = analysisdetail.getModelName();
      String type = analysisdetail.getAnalysisType();
      String repeat = analysisdetail.getRepeatMethod();
      LocusImp[] locusImp = analysisdetail.subsetLocusDetail.getSelectedLocus();
      if (locusImp.length < globalLocus.length) {
        for (int j = 0; j < locusImp.length; j++) {
          int locusID = locusImp[j].getID();
          locus += space + locusID;
          space = " ";
        }
        space = "";
      }

      space = "";
      Integer[] stat = analysisdetail.subsetStatDetail.getSelectedStatID();
      int nGstat = globalStatistic.length;
      int nStat = stat.length;
      if (nStat < nGstat) {
        for (int j = 0; j < nStat; j++) {
          statStr += space + stat[j];
          space = " ";
        }
      }

      space = "";
      Integer[] meta = analysisdetail.subsetMetaDetail.getSelectedStatID();
      int nGmeta = globalMeta.length;
      int nMeta = meta.length;
      if (nMeta < nGmeta) {
        for (int j = 0; j < nMeta; j++) {
          metaStr += space + meta[j];
          space = " ";
        }
      }

      pw.print("<ge:cctable");
      if (type.length() > 0 && type.equals("Allele")) {
        pw.print(" type=\"allele\"");
      }
      if (model.length() > 0) {
        pw.print(" model=\"" + model + "\"");
      }
      if (repeat.length() > 0) {
        pw.print(" repeat=\"" + repeat + "\"");
      }
      if (locus.length() > 0) {
        pw.print(" loci=\"" + locus + "\"");
      }
      if (statStr.length() > 0) {
        pw.print(" stats=\"" + statStr + "\"");
      }
      if (metaStr.length() > 0) {
        pw.print(" metas=\"" + metaStr + "\"");
      }
      pw.print(">");
      pw.println();

      Detail[] detailcolumn = analysisdetail.getChildDetail();

      for (int col = 0; col < detailcolumn.length; col++) {
        ColumnDetail columndetail = (ColumnDetail) detailcolumn[col];
        pw.println("<ge:col wt=\"" + columndetail.weightT.getText() + "\">");
        Detail[] detailgroup = columndetail.getChildDetail();
        // write group allele pattern 
        for (int g = 0; g < detailgroup.length; g++) {
          ColumnGroupDetail columngroup = (ColumnGroupDetail) detailgroup[g];
          pw.println("<ge:g>");
          for (int p = 0; p < columngroup.patternT.length; p++) {
            String pattern = columngroup.patternT[p].getText();
            pw.println("<ge:a>" + pattern + "</ge:a>");
          }
          pw.println("</ge:g>");
        }
        pw.println("</ge:col>");
      }
      pw.println("</ge:cctable>");
    }
  } // end writeCCtable

  public void closepw() {
    pw.println("</ge:rgen>");
    pw.close();
  }
}
