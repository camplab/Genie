package edu.utah.camplab.genie.gui2;

import edu.utah.camplab.genie.gm.LocusImp;
import edu.utah.camplab.genie.io.XUt;
import edu.utah.camplab.genie.util.GEException;
import edu.utah.camplab.genie.util.Ut;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import javax.swing.*;

//=============================================================================
public class AnalysisTab extends TabImp {
  AnalysisPanel analysisPanel;
  String errMessage = "";
  AnalysisMain analysisMain;
  AnalysisDetail firstAnalysis;

  public AnalysisTab() {
    super();
    title = "Analysis";
  }

  //----------------------------------------------------------------------------
  protected static ImageIcon createImageIcon(String path,
                                             String description) {
    java.net.URL imgURL = AnalysisTab.class.getResource(path);
    if (imgURL != null) {
      return new ImageIcon(imgURL, description);
    }
    else {
      System.err.println("Couldn't find file: " + path);
      return null;
    }
  }

  public void build(GenieGUI inGUI) {
    super.build(inGUI);
    analysisPanel = new AnalysisPanel("Analysis", inGUI);
    add(analysisPanel);
  }

  //----------------------------------------------------------------------------
  private int[] parseRefs(Element e, String attr_name, int nobjs)
    throws GEException {
    int[] ordinals = XUt.intsAtt(e, attr_name);
    if (ordinals != null) // if attribute specifying object subset present
    {
      return Ut.mapAdd(ordinals, -1); // change indexing base from 1 to 0
    }
    return Ut.identityArray(nobjs);   // otherwise reference all objects
  }

  //----------------------------------------------------------------------------
  public void populate() {
    Parameters params = gui.getParameters();
    System.out.println(" Analysis Tab populate");
    NodeList analysisdata = params.getAnalysis();
    Statistic[] gstat = gui.statisticTab.statisticDetail.getSelectedStat();
    Statistic[] gmeta = gui.statisticTab.metaDetail.getSelectedStat();
    LocusImp[] glocusImp = gui.locusTab.getSelectedLocus();
    int nstat = gstat.length;
    int nmeta = gmeta.length;
    int nlocus = glocusImp.length;

    for (int i = 0; i < analysisdata.getLength(); i++) {
      int[] loci = null;
      int[] stat = null;
      int[] meta = null;
      Element etable = (Element) analysisdata.item(i);
      //int[] tabletype = parseRefs(etable, Parameters.Att.TABLES, ntabletype);
      try {
        loci = parseRefs(etable, Parameters.Att.LOCI, nlocus);
        stat = parseRefs(etable, Parameters.Att.STATS, nstat);
        meta = parseRefs(etable, Parameters.Att.METAS, nmeta);
      } catch (Exception e) {
        System.out.println(
          "Failed to read Analysis variables loci, stats, meta "
            + e.getMessage());
        System.exit(0);
      }
      String model = etable.getAttribute(Parameters.Att.MODEL);
      String type = etable.getAttribute(Parameters.Att.TYPE).toLowerCase();
      String repeat = etable.getAttribute(Parameters.Att.REPEAT);
      NodeList cols = params.kidsOf(etable, Parameters.Tag.COL);
      if (type.length() == 0) {
        type = "genotype";
      }

      AnalysisDetail analysisDetail = (AnalysisDetail) analysisPanel.addAction();
      analysisDetail.populateLocus(loci);
      analysisDetail.populateStatistic(stat);
      analysisDetail.populateMetaStat(meta);
      //check # loci in the table
      Element colAtt = (Element) cols.item(0);
      NodeList gAtt = params.kidsOf(colAtt, Parameters.Tag.G);
      NodeList aAtt = params.kidsOf(
        (Element) gAtt.item(0), Parameters.Tag.A);
      int groupSize = aAtt.getLength();

      System.out.println("check A1");
      analysisDetail.populateInfoPanel(model,
        type,
        repeat,
        groupSize);

      analysisDetail.populateColumn(params, cols);
      analysisPanel.validate();
    }
  }

  //----------------------------------------------------------------------------
  public class AnalysisPanel extends TreexDetail {

    public AnalysisPanel(String inTitle, GenieGUI inGUI) {
      super(inTitle, inGUI);
      if (inGUI.isNew) {
        AnalysisDetail analysisdetail = (AnalysisDetail) addAction();
      }
    }

    //--------------------------------------------------------------------------
    public Detail buildRootPanel() {
      analysisMain = new AnalysisMain();
      analysisMain.setGUI(gui);
      analysisMain.setTreexDetail(this);
      return (Detail) analysisMain;
    }

    //--------------------------------------------------------------------------
    public void commitedAction() {
      if (checkDetail()) {
        if (gui.isNew) {
          setDisplayOnly();
        }
        gui.executeTab.build(gui);
        gui.setSelected(gui.executeTab);
      }
      else {
        JOptionPane.showMessageDialog(this, errMessage);
      }
    }

    //--------------------------------------------------------------------------
    public boolean checkDetail() {
      errMessage = "";
      boolean nextDetail = true;
      boolean nextColumn = true;
      boolean nextGroup = true;
      //DetailPanel[] detailpanel = this.getDetailPanel();
      DetailPanel[] detailpanel = analysisMain.getChildDetail();
      for (int i = 0; i < detailpanel.length; i++) {
        AnalysisDetail ad = (AnalysisDetail) detailpanel[i];
        DetailPanel[] columndetail = ad.getChildDetail();
        int nColumnDetail = columndetail.length;
        if (nColumnDetail > 1) {
          for (int j = 0; j < nColumnDetail; j++) {
            ColumnDetail cd = (ColumnDetail) columndetail[j];
            int cdIndex = cd.getIndex();
            if (cd.getWeight().length() == 0) {
              errMessage = "Column " + cdIndex + " missing Weight value";
              nextDetail = false;
              break;
            }

            DetailPanel[] groupdetail = cd.getChildDetail();
            for (int k = 0; k < groupdetail.length; k++) {
              ColumnGroupDetail cgd = (ColumnGroupDetail) groupdetail[k];
              String[] patterns = cgd.getPattern();
              for (int p = 0; p < patterns.length; p++) {
                if (patterns[p].length() == 0) {
                  errMessage = "Column " + cdIndex + " Group " + cgd.getIndex() + " missing pattern";
                  nextGroup = false;
                  nextColumn = false;
                  nextDetail = false;
                  break;
                }
              }
              if (!nextGroup) {
                break;
              }
            }
          }
          if (!nextDetail) {
            break;
          }
        }
        else {
          errMessage = "Analysis requires more than 1 column";
          nextDetail = false;
          break;
        } // if nColumnDetail > 1
      } // end detailpanel loop
      return nextDetail;
    } //end checkDetail

    //--------------------------------------------------------------------------
    public void setDisplayOnly() {
      Util.disable(commitButton);
      Util.disable(addButton);
      Util.disable(removeButton);
      //DetailPanel[] detailpanel = this.getDetailPanel();
      DetailPanel[] detailpanel = analysisMain.getChildDetail();
      for (int i = 0; i < detailpanel.length; i++) {
        AnalysisDetail ad = (AnalysisDetail) detailpanel[i];
        DetailPanel[] columndetail = ad.getChildDetail();
        for (int j = 0; j < columndetail.length; j++) {
          ColumnDetail cd = (ColumnDetail) columndetail[j];
          cd.setDisplayOnly();
          DetailPanel[] groupdetail = cd.getChildDetail();
          for (int k = 0; k < groupdetail.length; k++) {
            ColumnGroupDetail cgd = (ColumnGroupDetail) groupdetail[k];
            cgd.setDisplayOnly();
          }
        }
      }
    }

    //--------------------------------------------------------------------------
    /*
    public LocusDetail buildSubsetLocusDetail()
    {
        LocusTab locustab = (LocusTab) gui.locusTab;
        String messageText2 = new String ("No Locus info");
        if ( locustab != null )
        {
          LocusImp[] locus = locustab.getSelectedLocus();
          int nlocus = locus.length;
          if ( locus.length > 0 )
          {
            String[] locusName = new String[nlocus];
            String[] locusDist = new String[nlocus];
            String[] gene      = new String[nlocus];
            for ( int i = 0; i < nlocus; i++ )
            {
              locusName[i] = locus[i].getMarker();
              locusDist[i] = String.valueOf(locus[i].getTheta());
              gene[i]      = String.valueOf(locus[i].getGeneID());
            }

            //allLocusB.setVisible(false);
            //getLocusB.setVisible(false);
            LocusDetail panel = new LocusDetail
                                    ( "Subset Locus",
                                      nlocus,
                                      locusName,
                                      locusDist,
                                      gene );
            //subsetLocus.add(locusDetailPanel);
            //subsetLocus.updateUI();
            //columnPanel.setLocusName( locusName );
            return panel;
          }
          else
            JOptionPane.showMessageDialog(this, messageText2);
        }
        else
          JOptionPane.showMessageDialog(this, messageText2);
        return null;
      } //end buildSubsetLocus

    //--------------------------------------------------------------------------
    public StatisticDetail buildSubsetStatisticDetail() {
        StatisticDetail statisticdetail = new StatisticDetail();
        StatisticTab stattab = (StatisticTab) gui.statisticTab;
        StatisticDetail statdetail = stattab.getStatisticDetail();
        if ( statdetail != null )
        {
          Statistic[] stats = statdetail.getSelectedStat();
          statisticdetail.setStatistic(stats);
          statisticdetail.build();
        }
        return statisticdetail;
      }  // end getSubsetStatistic
    */

    //--------------------------------------------------------------------------
    //public void increaseOpenPaneCount()
    //{ openPaneCount++; }

    //public int getOpenPaneCount()
    //{ System.out.println("within AnalysisPanel");
    //  return openPaneCount; }

    //--------------------------------------------------------------------------
    public String toString() {
      return title;
    }
  } // end AnalysisPanel
}
