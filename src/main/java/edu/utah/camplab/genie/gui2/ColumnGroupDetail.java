package edu.utah.camplab.genie.gui2;

import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ColumnGroupDetail extends DetailPanel
  implements ActionListener {
  JButton patternhelpB;
  //String detailTitle = null;
  //int openChildCount = 0;
  //DefaultMutableTreeNode parentNode;
  int nLocus = 0;
  JTextField[] patternT;
  String[] locusName = null;

  public ColumnGroupDetail() {
    super();
    title = "ColumnGroupDetail";
    allowsChildren = false;
  }

  public void build(int inIndex) {
    super.build(inIndex);
    setPreferredSize(new Dimension(600, 400));
    //parentNode = inParentNode;
    GridBagLayout gridbag = new GridBagLayout();
    GridBagConstraints constraints = new GridBagConstraints();
    setLayout(gridbag);
    TitledEtched subtitle = new TitledEtched(title + " " + getIndex());
    setBorder(subtitle.title);
    // use the same layeredPane from ColumnDetail 
    DefaultMutableTreeNode grandNode = (DefaultMutableTreeNode) parentNode.getParent();
    AnalysisDetail analysisdetail = null;
    try {
      analysisdetail = (AnalysisDetail) grandNode.getUserObject();
    } catch (Exception ee) {
      System.out.println("error : " + ee.getMessage());
    }
    nLocus = analysisdetail.getnumLocus();
    ;
    JLabel locusL = new JLabel("Locus");
    JLabel patternL = new JLabel("Pattern");
    patternT = new JTextField[nLocus];
    JLabel[] locusNameL = new JLabel[nLocus];
    patternhelpB = new JButton(createImageIcon("/toolbarButtonGraphics/general/Help24.gif",
      "pattern help button"));
    patternhelpB.setContentAreaFilled(false);
    patternhelpB.setBorderPainted(false);
    patternhelpB.setHorizontalAlignment(JLabel.LEFT);
    patternhelpB.setActionCommand("patternthelp");
    patternhelpB.addActionListener(this);

    // constraints for all objects
    constraints.gridheight = 2;
    constraints.anchor = GridBagConstraints.WEST;

    // constraints for Locus label
    constraints.ipadx = 20;
    constraints.gridx = 0;
    constraints.gridy = 0;
    constraints.gridwidth = 20;
    gridbag.setConstraints(locusL, constraints);
    add(locusL);

    // constraints for pattern Label
    constraints.gridx = 60;
    constraints.gridwidth = 30;
    gridbag.setConstraints(patternL, constraints);
    add(patternL);

    // constraints for pattern help button
    constraints.gridx = 100;
    constraints.gridy = 0;
    constraints.gridwidth = 5;
    gridbag.setConstraints(patternhelpB, constraints);
    add(patternhelpB);
    super.repaint();

    for (int i = 0; i < nLocus; i++) {
      locusNameL[i] = new JLabel("Marker " + (i + 1));
      patternT[i] = new JTextField(20);

      // constraints for locusName label
      constraints.gridx = 0;
      constraints.gridy = 4 * i + 4;
      constraints.gridwidth = 20;
      gridbag.setConstraints(locusNameL[i], constraints);

      // constraints for locusPatternT field
      constraints.gridx = 60;
      //constraints.gridwidth = GridBagConstraints.REMAINDER;
      constraints.gridwidth = 30;
      gridbag.setConstraints(patternT[i], constraints);

      add(locusNameL[i]);
      add(patternT[i]);
      repaint();
    }
  }

  public void actionPerformed(ActionEvent ae) {
    Object source = ae.getSource();
    if (source == patternhelpB) {
      JOptionPane.showMessageDialog(this,
        "Enter allelic or haplotype pattern for this column. Need more details");
    }
  }

  public String[] getPattern() {
    String[] pattern = new String[patternT.length];
    for (int i = 0; i < pattern.length; i++) {
      pattern[i] = patternT[i].getText();
    }
    return pattern;
  }

  //----------------------------------------------------------------------------
  public String toString() {
    return "Pattern " + myIndex;
  }

  public void setDisplayOnly() {
    for (int i = 0; i < patternT.length; i++) {
      Util.disable(patternT[i]);
    }
  }

  //----------------------------------------------------------------------------
  public void populate(NodeList aPatternList) {
    for (int i = 0; i < aPatternList.getLength(); i++) {
      Element aPattern = (Element) aPatternList.item(i);
      try {
        String ap = aPattern.getFirstChild().getNodeValue().trim();
        //String ap = XUt.stringAtt(aPattern, Parameters.Tag.A);
        patternT[i].setText(ap);
      } catch (Exception e) {
        System.out.println("Failed to read Analysis Table allele pattern " +
          e.getMessage());
      }
    }
  }
} 

