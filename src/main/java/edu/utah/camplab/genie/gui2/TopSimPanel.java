package edu.utah.camplab.genie.gui2;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class TopSimPanel extends BasicPanel
  implements ActionListener {
  ButtonGroup topBgroup;
  JRadioButton alleleFreqRB, hapMCTopRB, hapFreqRB, xRB;

  public TopSimPanel() {
    super("Top Simulation");
    alleleFreqRB = new JRadioButton("AlleleFreqTopSim", true);
    hapMCTopRB = new JRadioButton("HapMCTopTogether");
    hapFreqRB = new JRadioButton("HapFreqTopSim");
    xRB = new JRadioButton("XTopSim");
    alleleFreqRB.setActionCommand("AlleleFreqTopSim");
    hapMCTopRB.setActionCommand("HapMCTopTogether");
    hapFreqRB.setActionCommand("HapFreqTopSim");
    xRB.setActionCommand("XTopSim");
    topBgroup = new ButtonGroup();
    topBgroup.add(alleleFreqRB);
    topBgroup.add(hapMCTopRB);
    topBgroup.add(hapFreqRB);
    topBgroup.add(xRB);
    alleleFreqRB.addActionListener(this);
    hapMCTopRB.addActionListener(this);
    hapFreqRB.addActionListener(this);
    xRB.addActionListener(this);
    add(alleleFreqRB);
    add(hapMCTopRB);
    add(hapFreqRB);
    add(xRB);
  }

  public void actionPerformed(ActionEvent e) {
  }

  public void setHapMC() {
    hapMCTopRB.setSelected(true);
  }

  public void setPedGenie() {
    alleleFreqRB.setSelected(true);
  }

  public String getTop() {
    return topBgroup.getSelection().getActionCommand();
  }

  public void populate(Parameters params) {
    String topsim = params.get(Parameters.Att.TOP);
    if (topsim.startsWith("HapMC")) {
      hapMCTopRB.setSelected(true);
    }
    else if (topsim.equals("HapFreqTopSim")) {
      hapFreqRB.setSelected(true);
    }
    else if (topsim.equals("XTopSim")) {
      xRB.setSelected(true);
    }
    //parentPanel.setPackage(packageName);
  }
}
