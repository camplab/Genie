package edu.utah.camplab.genie.gui2;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

//=============================================================================
public class ParameterFilePanel extends BasicPanel
  implements ActionListener {
  JButton browsefileB, commitB;
  JLabel parameterfileL;
  JTextField parameterfileT;
  OptionTab parentPanel;
  GenieGUI gui;

  public ParameterFilePanel() {
    super("Parameter File");
    TitledEtched titledEtched = new TitledEtched("Parameter File");
    setBorder(titledEtched.title);
    setPreferredSize(new Dimension(800, 200));

    GridBagLayout gridbag = new GridBagLayout();
    //GridBagConstraints constraintsL = new GridBagConstraints();
    //GridBagConstraints constraintsT = new GridBagConstraints();
    //GridBagConstraints constraintsB = new GridBagConstraints();
    //GridBagConstraints constraintsC = new GridBagConstraints();
    GridBagConstraints constraints = new GridBagConstraints();
    constraints.weightx = 0.5;
    constraints.gridwidth = 3;
    constraints.gridheight = 2;
    constraints.ipadx = 0;

    setLayout(gridbag);
    String nfs = new String("No File Selected");
    parameterfileT = new JTextField(nfs, 30);
    parameterfileT.setPreferredSize(new Dimension(15, 27));
    parameterfileL = new JLabel("Parameter File", JLabel.TRAILING);
    parameterfileL.setLabelFor(parameterfileT);
    parameterfileL.setPreferredSize(new Dimension(100, 27));
    browsefileB = new JButton("Browse");
    browsefileB.addActionListener(this);
    commitB = new JButton("Commit");
    commitB.addActionListener(this);

    gridbag.setConstraints(parameterfileL, constraints);
    gridbag.setConstraints(parameterfileT, constraints);
    gridbag.setConstraints(browsefileB, constraints);

    add(parameterfileL);
    add(parameterfileT);
    add(browsefileB);

    constraints.gridx = 0;
    constraints.gridy = 20;
    gridbag.setConstraints(commitB, constraints);
    add(commitB);
  }

  //---------------------------------------------------------------------------
  public void setParentPanel(JPanel parent) {
    parentPanel = (OptionTab) parent;
  }

  //---------------------------------------------------------------------------
  public void setGUI(GenieGUI inGUI) {
    gui = inGUI;
  }

  //---------------------------------------------------------------------------
  public void actionPerformed(ActionEvent e) {
    File parameterfile = null;
    Object source = e.getSource();
    if (source == browsefileB) {
      JFileChooser filechooser = new JFileChooser();
      filechooser.setDialogTitle("Parameter File");
      int returnVal = filechooser.showOpenDialog(ParameterFilePanel.this);
      if (returnVal == JFileChooser.APPROVE_OPTION) {
        parameterfile = filechooser.getSelectedFile();
        parameterfileT.setText(parameterfile.toString());
      }
    }
    else if (source == commitB) {
      // test whether retrievefile was selected in choicepanel
      Parameters parameters = null;
      if (parameterfile == null) {
        String fileStr = parameterfileT.getText();
        if (fileStr.length() > 0 && !fileStr.equals("No File Selected")) {
          parameterfile = new File(fileStr);
          try {
            parameters = new Parameters(parameterfile);
            commitedAction(parameters);
          } catch (Exception ee) {
            JOptionPane.showMessageDialog(this, "Failed to read parameter file "
              + fileStr + " " + ee.getMessage());
          }
        }
        else {
          JOptionPane.showMessageDialog(this, "No file selected");
        }
      }
      else {
        System.out.println("Parameter file is not null ");
        commitedAction(parameters);
      }
    }
  }

  //---------------------------------------------------------------------------
  public void commitedAction(Parameters parameters) {
    Util.disable(parameterfileT);
    Util.disable(commitB);
    gui.setParameters(parameters);
    PackagePanel packagePanel = new PackagePanel();
    packagePanel.setParentPanel(parentPanel);
    packagePanel.setGUI(gui);
    packagePanel.populate();
    packagePanel.setOpaque(true);
    parentPanel.setPackagePanel(packagePanel);
    parentPanel.add(packagePanel);
    gui.populate();
  }
}


