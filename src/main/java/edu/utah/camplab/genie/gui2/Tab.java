package edu.utah.camplab.genie.gui2;

public interface Tab {
  public void build(GenieGUI gui);
}
