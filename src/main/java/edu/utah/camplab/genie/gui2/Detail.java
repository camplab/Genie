package edu.utah.camplab.genie.gui2;

import javax.swing.tree.DefaultMutableTreeNode;

//=============================================================================
public interface Detail {
  public GenieGUI getGUI();

  public void setGUI(GenieGUI gui);

  public DefaultMutableTreeNode getRootNode();

  public void setRootNode(DefaultMutableTreeNode node);

  public DefaultMutableTreeNode getParentNode();

  public void setParentNode(DefaultMutableTreeNode node);

  public Detail getParentPanel();

  public void setParentPanel(Detail parentPanel);

  public void setMyNode(DefaultMutableTreeNode node);

  public TreexDetail getTreexDetail();

  public void setTreexDetail(TreexDetail treexdetail);

  public String getTitle();

  public void setTitle(String inTitle);

  public String toString();

  public void incrementChildIndex();

  public int getChildIndex();

  public int getIndex();

  public void setIndex(int index);

  public boolean getAllowsChildren();

  public void setAllowsChildren(boolean inBoolean);

  public String getChildName();

  public void setChildName(String childName);

  public Detail createChild() throws Exception;

  public void build(int index);

  public Detail[] getChildDetail();
}
