package edu.utah.camplab.genie.gui2;

import edu.utah.camplab.genie.io.XUt;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import java.awt.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

//import java.util.List;
//import java.util.Enumeration;

public class StudyTab extends TabImp {
  StudyPanel studyPanel;
  int nLocus = 0;
  String[] locusName;
  StudyMain studyMain;
  //StudyDetail firstStudy;
  boolean isNew = true;

  public StudyTab() {
    super();
    title = "Study";
  }

  public void build(GenieGUI inGUI) {
    super.build(inGUI);
    setLayout(new BorderLayout());
    studyPanel = new StudyPanel(title, inGUI);
    add(studyPanel);
  }

  //---------------------------------------------------------------------------
  public void populate() {
    studyPanel.populate();
    /*
      Parameters params = gui.getParameters();
      NodeList studydata = params.getDataFile();
      int nStudyData = studydata.getLength();
      for ( int i = 0; i < nStudyData; i++ ) {
        String stdname, genodata, hap, quant, par = null;
        Element edata = (Element) studydata.item(1);
        try {
          stdname     = XUt.stringAtt(edata, Parameters.Att.STUDYNAME);
          genodata    = XUt.stringAtt(edata, Parameters.Att.GENOTYPEDATA);
          hap         = XUt.stringAtt(edata, Parameters.Att.HAPLOTYPE);
          quant       = XUt.stringAtt(edata, Parameters.Att.QUANTITATIVE);
          par         = XUt.stringAtt(edata, Parameters.Att.LINKAGEPARAMETER);
          if ( genodata != null && genodata.length() > 0 ) {
            StudyDetail studydetail = null;
            if ( i == 0 && firstStudy != null )
              studydetail = firstStudy;
            else 
              StudyDetail studydetail = (StudyDetail) studyPanel.addAction();
            studydetail.genofileT.setText(genodata);
            if ( stdname != null && stdname.length() > 0 )
              studydetail.studynameT.setText(stdname);
            if ( hap != null && hap.length() > 0 )
              studydetail.freqfileT.setText(hap);
            if ( quant != null && quant.length() > 0 )
              studydetail.covariatefileT.setText(quant);
            if ( par != null && par.length() > 0 )
              studydetail.linkageparfileT.setText(par);
            studyPanel.commitedAction();
          }
        } catch ( Exception e ) {
          System.out.println("Failed to read study file info" + e.getMessage());
          System.exit(0);
        }
    */
  }

  //-------------------------------------------------------------------------
  /*
  private static void createAndShowGUI(DetailPanel panel) {
    JPanel newContentPane = panel;
      newContentPane.setOpaque(true); //content panes must be opaque
      String title = newContentPane.toString();
      JFrame frame = new JFrame(title);
      frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

      //Create and set up the content pane.
      frame.setContentPane(newContentPane);

      //Display the window.
      frame.pack();
      frame.setSize(new Dimension(950, 800));
      //frame.setResizable(false);
      frame.setVisible(true);
  }
  */

  //----------------------------------------------------------------------------
  public class StudyPanel extends TreexDetail {
    boolean filehasheader = true;

    public StudyPanel(String inTitle, GenieGUI inGUI) {
      super(inTitle, inGUI);
      if (inGUI.isNew) {
        StudyDetail studydetail = (StudyDetail) addAction();
      }
    }

    //-------------------------------------------------------------------------
    public Detail buildRootPanel() {
      studyMain = new StudyMain();
      studyMain.setGUI(gui);
      studyMain.setTreexDetail(this);
      return (Detail) studyMain;
    }

    //-------------------------------------------------------------------------
    /* duplicate of TreexDetail.addAction()
    public StudyDetail addChild() {
      StudyDetail studydetail = new StudyDetail();
      studydetail.setParentNode(studyMain.myNode);
      studydetail.setParentPanel(studyMain);
      studydetail.setRootNode(rootNode);
      studydetail.setTreexDetail(this);
      studydetail.build(studyMain.getChildIndex());
      studydetail.setOpaque(true);
      studydetail.setVisible(true);
      studydetail.setEnabled(true);
      studyMain.incrementChildIndex();
      DefaultMutableTreeNode node = setNode(studyMain.myNode, studydetail);
      studydetail.setMyNode(node);
      return studydetail;
    }

    //-------------------------------------------------------------------------
    public DefaultMutableTreeNode setNode(DefaultMutableTreeNode parentNode,
					  StudyDetail studydetail ) {
      DefaultMutableTreeNode childnode = new DefaultMutableTreeNode
					(studydetail, false);
      try {
        treeModel.insertNodeInto( childnode, 
				  parentNode, 
				  parentNode.getChildCount());
      } catch ( Exception e ) { 
        System.err.println("Error adding node to tree " + e.getMessage());
      }
      tree.setModel(treeModel);
      tree.scrollPathToVisible(new TreePath(childnode.getPath()));
      tree.updateUI();
      return childnode;
    } 
    */

    //-------------------------------------------------------------------------
    public void setLocusInfo() throws IOException {
      File file = null;
      // retrieve the first genotype file from the first Study Panel 
      //DetailPanelImp[] dpi = getDetailPanel();
      //DetailPanelImp[] dpi = studyMain.getChildDetail();
      DefaultMutableTreeNode node = (DefaultMutableTreeNode)
        rootNode.getFirstChild();
      StudyDetail sd = (StudyDetail) node.getUserObject();
      file = new File(sd.getGenoFile());

      if (file == null) {
        JOptionPane.showMessageDialog
          (this, "No Genotype File selected in " + sd.toString());
      }
      else {
        BufferedReader in = new BufferedReader(new FileReader(file));
        String line = in.readLine();
        if (line != null) {
          String[] tokens = line.trim().split("\\s+");

          if (filehasheader) {
            nLocus = tokens.length - 7;
            locusName = new String[nLocus];
            for (int i = 0; i < nLocus; i++) {
              locusName[i] = tokens[i + 7];
            }
          }
          else {
            nLocus = (int) ((tokens.length - 7) / 2);
            for (int i = 0; i < nLocus; i++) {
              locusName[i] = new String("Maker " + i);
            }
          }
        }
        in.close();
      }
    } // end setLocusInfo

    //-------------------------------------------------------------------------
    public int getNumLocus() {
      return nLocus;
    }

    //-------------------------------------------------------------------------
    public String[] getLocusName() {
      return locusName;
    }

    //-------------------------------------------------------------------------
    public void commitedAction() {
      try {
        setLocusInfo();
        if (nLocus != 0) {
          //gui.addTab("Locus");
          gui.locusTab.build(gui);
          if (gui.isNew) {
            setDisplayOnly();
            gui.setSelected(gui.locusTab);
          }
        }
        else {
          JOptionPane.showMessageDialog
            (this, "Genotype file has no genotype data");
        }
      } catch (Exception e) {
        JOptionPane.showMessageDialog
          (this, "Failed to read Genotype Data File");
      }
    }

    //-------------------------------------------------------------------------
    public String toString() {
      return title;
    }

    //-------------------------------------------------------------------------
    public void setDisplayOnly() {
      Util.disable(addButton);
      Util.disable(removeButton);
      Util.disable(commitButton);
      //DetailPanelImp[] detailpanel = studyPanel.getDetailPanel();
      DetailPanel[] detailpanel = studyMain.getChildDetail();
      for (int i = 0; i < detailpanel.length; i++) {
        StudyDetail sd = (StudyDetail) detailpanel[i];
        sd.setDisplayOnly();
      }
    }

    //-------------------------------------------------------------------------
    public void populate() {
      Parameters params = gui.getParameters();
      NodeList studydata = params.getDataFile();
      int nStudyData = studydata.getLength();
      for (int i = 0; i < nStudyData; i++) {
        String stdname, genodata, hap, quant, par = null;
        Element edata = (Element) studydata.item(i);
        try {
          stdname = XUt.stringAtt(edata, Parameters.Att.STUDYNAME);
          genodata = XUt.stringAtt(edata, Parameters.Att.GENOTYPEDATA);
          hap = XUt.stringAtt(edata, Parameters.Att.HAPLOTYPE);
          quant = XUt.stringAtt(edata, Parameters.Att.QUANTITATIVE);
          par = XUt.stringAtt(edata, Parameters.Att.LINKAGEPARAMETER);
          if (genodata != null && genodata.length() > 0) {
            StudyDetail studydetail = (StudyDetail) addAction();
            studydetail.genofileT.setText(genodata);
            if (stdname != null && stdname.length() > 0) {
              studydetail.studynameT.setText(stdname);
            }
            if (hap != null && hap.length() > 0) {
              studydetail.freqfileT.setText(hap);
            }
            if (quant != null && quant.length() > 0) {
              studydetail.covariatefileT.setText(quant);
            }
            if (par != null && par.length() > 0) {
              studydetail.linkageparfileT.setText(par);
            }
            //studydetail.repaint();
            //studydetail.updateUI();
            //studydetail.validate();
            //layeredPane.add(studydetail);
            //studyPanel.updateUI();
            studyPanel.validate();
            //displaySelectedScreen(studydetail);
          }
        } catch (Exception e) {
          System.out.println("Failed to read file " + e.getMessage());
          System.exit(0);
        }
      }
      commitedAction();
    }
  } // end studyPanel

  /*
  private static void createUI()
  {
    JFrame frame = new JFrame("Study");
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.setContentPane(new StudyPanel());
    frame.pack();
    frame.setVisible(true);
    //frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
  }

  public static void main(String[] args)
  {
    SwingUtilities.invokeLater(
        new Runnable()
        {
          public void run()
          {
            createUI();
          }
        } );
  }
  */
}
