package edu.utah.camplab.genie.gui2;

import javax.swing.*;
import java.awt.*;

//=============================================================================
public class VariablesPanel extends JPanel {
  //implements PropertyChangeListener {
  JTextField rSeedT, numSimT;

  public VariablesPanel() {
    GridBagLayout gridbag = new GridBagLayout();
    GridBagConstraints constraints = new GridBagConstraints();
    constraints.weightx = 1.0;
    constraints.gridwidth = 2;
    constraints.gridheight = 2;
    constraints.ipadx = 0;
    setLayout(gridbag);
    TitledEtched titledEtched = new TitledEtched("Simulation Variables");
    setBorder(titledEtched.title);
    setPreferredSize(new Dimension(300, 90));
    JLabel rSeedL = new JLabel("Random Number Seed ", JLabel.TRAILING);
    JLabel numSimL = new JLabel("Number of Simulation ", JLabel.TRAILING);

    rSeedT = new JTextField("Random", 10);
    rSeedL.setLabelFor(rSeedT);
    //rSeedT.addPropertyChangeListener(this);
    numSimT = new JTextField("100", 10);
    numSimL.setLabelFor(numSimT);
    //numSimT.addPropertyChangeListener(this);
    gridbag.setConstraints(rSeedL, constraints);
    gridbag.setConstraints(numSimL, constraints);
    constraints.gridwidth = GridBagConstraints.REMAINDER;
    gridbag.setConstraints(rSeedT, constraints);
    gridbag.setConstraints(numSimT, constraints);
    add(rSeedL);
    add(rSeedT);
    add(numSimL);
    add(numSimT);
  }

  public String getRseed() {
    return rSeedT.getText();
  }

  public String getNSim() {
    return numSimT.getText();
  }

  public void populate(Parameters params) {
    String rSeedVal = params.get(Parameters.Att.RSEED);
    String numSimVal = params.get(Parameters.Att.NSIMS);
    rSeedT.setText(rSeedVal);
    numSimT.setText(numSimVal);
  }
}
