package edu.utah.camplab.genie.gui2;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

//=============================================================================
public class DecodePanel extends BasicPanel
  implements ActionListener {
  JRadioButton decodeYesRB, decodeNoRB;
  ButtonGroup decodeBgroup;
  DecodeFilePanel decodefilePanel = null;
  JButton helpB;
  GlobalTab.GlobalPanel parentPanel;

  public DecodePanel() {
    super("Decode Allele");
    decodeYesRB = new JRadioButton("Yes");
    decodeNoRB = new JRadioButton("No");
    decodeYesRB.setActionCommand("Yes");
    decodeNoRB.setActionCommand("No");
    decodeNoRB.setSelected(true);
    decodeBgroup = new ButtonGroup();
    decodeBgroup.add(decodeYesRB);
    decodeBgroup.add(decodeNoRB);
    decodeYesRB.addActionListener(this);
    decodeNoRB.addActionListener(this);
    helpB = new JButton(createImageIcon("/toolbarButtonGraphics/general/Help24.gif", "Explanation"));
    helpB.setContentAreaFilled(false);
    helpB.setBorderPainted(false);
    helpB.setHorizontalAlignment(JLabel.LEFT);
    helpB.addActionListener(this);
    helpB.setActionCommand("Help");

    add(decodeYesRB);
    add(decodeNoRB);
    add(helpB);
  }

  public void actionPerformed(ActionEvent e) {
    Object source = e.getSource();
    if (source == decodeYesRB) {
      decodefilePanel = new DecodeFilePanel();
      decodefilePanel.setOpaque(true);
      parentPanel.infoPanel.add(decodefilePanel);
      parentPanel.infoPanel.updateUI();
    }
    else if (source == helpB) {
      String mgs = "converts analysis table pattern allele code 1 and 2, common and rare, to the code in the pre-specified decode frequency file, or to the code same as the input data, based on their allele frequency.";
      JOptionPane.showMessageDialog(this, mgs);
    }
  }

  //----------------------------------------------------------------------------
  public void setParentPanel(JPanel parent) {
    parentPanel = (GlobalTab.GlobalPanel) parent;
  }

  public void populate(Parameters params) {
    String decode = params.get(Parameters.Att.DECODE);
    if (decode.equals("Yes")) {
      decodeYesRB.setSelected(true);
    }
    decodefilePanel = new DecodeFilePanel();
    decodefilePanel.setOpaque(true);
    decodefilePanel.populate(params);
    parentPanel.infoPanel.add(decodefilePanel);
    parentPanel.infoPanel.updateUI();
  }

  //----------------------------------------------------------------------------
  public class DecodeFilePanel extends JPanel
    implements ActionListener {
    JButton decodefileB;
    JLabel decodefileL;
    JTextField decodefileT;
    File decodefile;

    public DecodeFilePanel() {
      TitledEtched titledEtched = new TitledEtched("Decode Frequency File");
      setBorder(titledEtched.title);
      GridBagLayout gridbag = new GridBagLayout();
      GridBagConstraints constraintsL = new GridBagConstraints();
      GridBagConstraints constraintsT = new GridBagConstraints();
      GridBagConstraints constraintsB = new GridBagConstraints();
      setLayout(gridbag);
      setPreferredSize(new Dimension(300, 90));
      String nfs = new String("No File Selected");
      decodefileT = new JTextField("");
      decodefileT.setPreferredSize(new Dimension(280, 20));
      decodefileB = new JButton("Browse");
      decodefileB.addActionListener(this);
      decodefile = null;
      int gridyVal = 0;
      constraintsT.ipadx = 0;
      constraintsT.ipady = 5;
      constraintsT.gridx = 0;
      constraintsT.gridy = 0;
      constraintsB.ipadx = 0;
      constraintsB.ipady = 0;
      constraintsB.gridx = 0;
      constraintsB.gridy = 25;
      constraintsB.gridwidth = 5;
      constraintsB.gridheight = 4;
      gridbag.setConstraints(decodefileT, constraintsT);
      gridbag.setConstraints(decodefileB, constraintsB);
      add(decodefileT);
      add(decodefileB);
    }

    public void actionPerformed(ActionEvent e) {
      Object source = e.getSource();
      if (source == decodefileB) {
        JFileChooser filechooser = new JFileChooser();
        filechooser.setDialogTitle("Decode Frequency File");
        int returnVal = filechooser.showOpenDialog(DecodePanel.this);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
          decodefile = filechooser.getSelectedFile();
          decodefileT.setText(decodefile.toString());
        }
      }
    }

    public void populate(Parameters params) {
      String dfile = params.get(Parameters.Att.DECODEFILE);
      if (dfile != null) {
        decodefileT.setText(dfile.toString());
      }
    }
  }  // end DecodefilePanel
} // end DecodePanel


