package edu.utah.camplab.genie.gui2;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;

public class StatisticImp extends JPanel
  implements ItemListener, ActionListener {
  JCheckBox[] statisticCB;
  Statistic[] statistics;
  int nStats = 0;
  String title;

  public StatisticImp(String title) {
    super();
    this.title = title;
  }

  public void build(String indicator) {
    boolean checkitem = true;
    this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
    TitledEtched titledEtched = new TitledEtched(title);
    this.setBorder(titledEtched.title);
    if (statistics == null || statistics.length == 0) {
      statistics = Statistic.initializeStats(indicator);
      checkitem = false;
    }

    nStats = statistics.length;
    statisticCB = new JCheckBox[nStats];

    for (int i = 0; i < nStats; i++) {
      statisticCB[i] = new JCheckBox(statistics[i].displayName, checkitem);
      statisticCB[i].addItemListener(this);
      this.add(statisticCB[i]);
    }
  }

  //--------------------------------------------------------------------------
  public void setStatistic(Statistic[] inStats) {
    statistics = inStats;
    nStats = inStats.length;
  }

  public void itemStateChanged(ItemEvent e) {
  }

  public void actionPerformed(ActionEvent e) {
  }

  //Quit the application.
  protected void quit() {
    System.exit(0);
  }

  //--------------------------------------------------------------------------
  public Statistic[] getSelectedStat() {
    ArrayList<Statistic> statisticl = new ArrayList<Statistic>();
    for (int i = 0; i < nStats; i++) {
      if (statisticCB[i].isSelected()) {
        statisticl.add(statistics[i]);
      }
    }
    return (Statistic[]) statisticl.toArray(new Statistic[0]);
  }

  //--------------------------------------------------------------------------
  public Integer[] getSelectedStatID() {
    ArrayList<Integer> statisticl = new ArrayList<Integer>();
    for (int i = 0; i < nStats; i++) {
      if (statisticCB[i].isSelected()) {
        statisticl.add(new Integer(i + 1));
      }
    }
    return (Integer[]) statisticl.toArray(new Integer[0]);
  }

  //--------------------------------------------------------------------------
  public void setDisplayOnly() {
    Util.disable(statisticCB);
  }

  //--------------------------------------------------------------------------
  public void populate(String[] statStr) {
    deSelectedAll();
    for (int i = 0; i < statStr.length; i++) {
      for (int j = 0; j < nStats; j++) {
        if (statStr[i].equals(statistics[j].getProgramName())) {
          statisticCB[j].setSelected(true);
          break;
        }
      }
    }
  }

  //--------------------------------------------------------------------------
  public void populate(int[] statInt) {
    deSelectedAll();
    for (int i = 0; i < statInt.length; i++) {
      for (int j = 0; j < nStats; j++) {
        if ((statInt[i] - 1) == j) {
          statisticCB[j].setSelected(true);
          break;
        }
      }
    }
  }

  //--------------------------------------------------------------------------
  public void deSelectedAll() {
    for (int i = 0; i < nStats; i++) {
      statisticCB[i].setSelected(false);
    }
  }
}

