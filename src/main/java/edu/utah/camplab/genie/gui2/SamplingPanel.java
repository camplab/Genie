package edu.utah.camplab.genie.gui2;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

//==============================================================================
public class SamplingPanel extends BasicPanel
  implements ActionListener {
  JRadioButton allRB, founderRB;
  ButtonGroup samplingBgroup;

  public SamplingPanel() {
    super("Sampling Method");
    allRB = new JRadioButton("all", true);
    founderRB = new JRadioButton("founder");
    allRB.setActionCommand("all");
    founderRB.setActionCommand("founder");
    allRB.setSelected(true);
    samplingBgroup = new ButtonGroup();
    samplingBgroup.add(allRB);
    samplingBgroup.add(founderRB);
    add(allRB);
    add(founderRB);
    allRB.addActionListener(this);
    founderRB.addActionListener(this);
  }

  public void actionPerformed(ActionEvent e) {
  }

  public String getMethod() {
    return samplingBgroup.getSelection().getActionCommand();
  }

  public void populate(Parameters params) {
    String sampling = params.get("top-sample");
    if (sampling.equals("founder")) {
      founderRB.setSelected(true);
      allRB.setSelected(false);
    }
    else {
      founderRB.setSelected(false);
      allRB.setSelected(true);
    }
  }
}
