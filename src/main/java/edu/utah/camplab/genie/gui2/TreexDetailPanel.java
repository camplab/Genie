package edu.utah.camplab.genie.gui2;

import edu.utah.camplab.genie.util.Ut;

import javax.swing.*;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseListener;
import java.util.Enumeration;
import java.util.TreeMap;

public class TreexDetailPanel extends JPanel
  implements ActionListener,
  TreeSelectionListener {
  private static final String SPKG_GUI = Ut.pkgOf(DetailPanel.class);
  private static String ADD_COMMAND = "add";
  private static String REMOVE_COMMAND = "remove";
  private static String COMMIT_COMMAND = "commit";
  protected JTree tree;
  protected DefaultTreeModel treeModel;
  protected DefaultMutableTreeNode myNode, rootNode;
  protected DetailPanelImp rootPanel;
  String title;
  JPanel treepanel, buttonPanel;
  JLayeredPane layeredPane;
  DefaultMutableTreeNode newNode = null;
  JButton addButton, removeButton, commitButton;
  TreexDetailPanel treexdetailpanel;
  MouseListener ml = new MouseAdapter() {
    /*
    public void mousePressed(MouseEvent e) 
    {
      int selRow = tree.getRowForLocation(e.getX(), e.getY());
      TreePath selPath = tree.getPathForLocation(e.getX(), e.getY());
      if(selRow != -1) 
      {
        if(e.getClickCount() == 1) 
          mySingleClick(selRow, selPath);
        else if(e.getClickCount() == 2) 
          myDoubleClick(selRow, selPath);
      }
    }
    */
  };
  private Toolkit toolkit = Toolkit.getDefaultToolkit();

  public TreexDetailPanel(String inTitle) {
    super(new GridLayout(1, 0));
    title = inTitle;
    rootPanel = mainPanel();
    rootNode = new DefaultMutableTreeNode(rootPanel, true);
    myNode = rootNode;
    //parentNode = rootNode;
    treeModel = new DefaultTreeModel(rootNode);
    //treeModel.addTreeModelListener(new MyTreeModelListener());
    tree = new JTree(treeModel);
    //tree.setUI(new BasicTreeUI());
    tree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
    tree.addTreeSelectionListener(this);
    tree.addMouseListener(ml);
    tree.setShowsRootHandles(true);
    tree.setExpandsSelectedPaths(true);
    JScrollPane scrollPane = new JScrollPane(tree);

    // File List Panel
    treepanel = new JPanel();
    treepanel.setLayout(new BorderLayout());
    TitledEtched bordertitle = new TitledEtched(title + " Listing");
    treepanel.setBorder(bordertitle.title);

    buttonPanel = new JPanel();
    addButton = new JButton(ADD_COMMAND);
    removeButton = new JButton(REMOVE_COMMAND);
    commitButton = new JButton(COMMIT_COMMAND);
    addButton.setActionCommand(ADD_COMMAND);
    removeButton.setActionCommand(REMOVE_COMMAND);
    commitButton.setActionCommand(COMMIT_COMMAND);
    addButton.addActionListener(this);
    removeButton.addActionListener(this);
    commitButton.addActionListener(this);
    buttonPanel.add(addButton);
    buttonPanel.add(removeButton);
    buttonPanel.add(commitButton);

    treepanel.add(scrollPane, BorderLayout.CENTER);
    treepanel.add(buttonPanel, BorderLayout.SOUTH);

    // Detail Pane
    layeredPane = new JLayeredPane();
    layeredPane.setLayout(new BorderLayout());

    // add treepanel and layeredPane to splitpane
    JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
    splitPane.setLeftComponent(treepanel);
    splitPane.setRightComponent(layeredPane);

    add(splitPane);
  }

  //---------------------------------------------------------------------------
  public void createDetail(String panelname) throws Exception {
    createDetail(rootNode, panelname);
  }

  public void createDetail(DefaultMutableTreeNode parentNode,
                           String panelname)
    throws Exception {
    System.out.println("ok " + panelname);
    Object newPanel = Ut.newModule(SPKG_GUI, panelname);
    DetailPanelImp detail = (DetailPanelImp) newPanel;
    //DefaultMutableTreeNode currentNode = getCurrentNode();
    //TreePath parentPath = tree.getSelectionPath();
    //DefaultMutableTreeNode parentNode = rootNode;
    //if ( currentNode != null && currentNode != rootNode ) 
    //  	parentNode = (DefaultMutableTreeNode) currentNode.getParent();
    detail.setParentNode(parentNode);
    detail.setRootNode(rootNode);
    detail.setTreexDetailPanel(this);
    DetailPanelImp parentPanel = (DetailPanelImp) parentNode.getUserObject();
    System.out.println("parent Panel : " + parentPanel.toString());
    int index = parentPanel.getChildCount();
    detail.buildDetail(index);
    parentPanel.incrementChildCount();
    DefaultMutableTreeNode detailNode = new DefaultMutableTreeNode
      (detail,
        detail.getAllowsChildren());

    detail.setMyNode(detailNode);
    System.out.println(" create detail panel parent " + parentPanel.toString() +
      " child " + detail.toString());
    DefaultMutableTreeNode node = addNode(parentNode, detailNode);
    layeredPane.add(detail, detail.getIndex());
    displaySelectedScreen(detail);
    detail.setOpaque(true);
    //return detail;
  }

  //---------------------------------------------------------------------------
  public DetailPanelImp mainPanel() {
    return null;
  }

  /*
  //---------------------------------------------------------------------------
  public String getTitle() {
    return title;
  }

  //---------------------------------------------------------------------------
  public String toString() {
    return title;
  }

  //---------------------------------------------------------------------------
  public int getIndex() {
    return 0;
  }

  //---------------------------------------------------------------------------
  public boolean getAllowsChildren() {
    return allowsChildren;
  }

  //---------------------------------------------------------------------------
  public void buildChild(String childName ) {
  }

  //---------------------------------------------------------------------------
  public int getChildCount() {
    return childCount;
  }

  //---------------------------------------------------------------------------
  public void setParentNode(DefaultMutableTreeNode parent) {
    parentNode = parent;
  }

  //---------------------------------------------------------------------------
  public void setMyNode(DefaultMutableTreeNode node) {
    myNode = node; 
  }
  
  //---------------------------------------------------------------------------
  public void setRootNode(DefaultMutableTreeNode node) {
    rootNode = node;
  }

  //---------------------------------------------------------------------------
  /** add new node to tree */
  public DefaultMutableTreeNode addNode(Object obj, boolean allowsChildren) {
    DefaultMutableTreeNode node = new DefaultMutableTreeNode(obj,
      allowsChildren);
    return addNode(node);
  }

  public DefaultMutableTreeNode addNode(DefaultMutableTreeNode node) {
    DefaultMutableTreeNode parentNode = null;
    TreePath parentPath = tree.getSelectionPath();
    if (parentPath == null) {
      parentNode = rootNode;
    }
    else {
      parentNode = (DefaultMutableTreeNode) parentPath.getLastPathComponent();
    }
    // check whether parentNode allow children

    Object parentObj = parentNode.getUserObject();

    System.out.println("ParentObj " + parentObj.toString());
    if (parentNode.getAllowsChildren()) {
      return addNode(parentNode, node);
    }
    else {
      return null;
    }
    //return addNode(rootNode, node);
  }

  public DefaultMutableTreeNode addNode(DefaultMutableTreeNode parent,
                                        DefaultMutableTreeNode child) {
    Object parentObj = parent.getUserObject();
    System.out.println("ok2 ParentObj " + parentObj.toString());
    if (parent == null) {
      parent = rootNode;
      System.out.println("parent is root ");
    }
    try {
      treeModel.insertNodeInto(child, parent, parent.getChildCount());
    } catch (Exception ee) {
      System.err.println("Error : " + ee.getMessage());
    }
    tree.setModel(treeModel);
    tree.scrollPathToVisible(new TreePath(child.getPath()));
    DefaultMutableTreeNode lastNode = (DefaultMutableTreeNode)
      tree.getLastSelectedPathComponent();
    //if ( lastNode != null )
    tree.updateUI();
    return child;
  }

  //---------------------------------------------------------------------------
  public DefaultMutableTreeNode getFirstNode() {
    return (DefaultMutableTreeNode) rootNode.getFirstChild();
  }

  /**
   * return current selected node.
   */
  public DefaultMutableTreeNode getCurrentNode() {
    DefaultMutableTreeNode currentNode = null;
    TreePath currentPath = tree.getSelectionPath();
    if (currentPath != null) {
      currentNode = (DefaultMutableTreeNode) tree.getLastSelectedPathComponent();
      String currenttitle = currentNode.getUserObject().toString();
      System.out.println("current path is not null, title " + currenttitle);
    }
    else {
      currentNode = rootNode;
    }
    return currentNode;
  }

  //----------------------------------------------------------------------------
  public void removeCurrentNode() {
    DefaultMutableTreeNode currentNode = getCurrentNode();
    MutableTreeNode parent = (MutableTreeNode) (currentNode.getParent());
    if (parent != null) {
      treeModel.removeNodeFromParent(currentNode);
      return;
    }
    // Either there was no selection, or the root was selected.
    toolkit.beep();
  }

  //---------------------------------------------------------------------------
  public void displaySelectedScreen() {
    DefaultMutableTreeNode selectedNode = getCurrentNode();
    displaySelectedScreen(selectedNode);
  }

  public void displaySelectedScreen(DefaultMutableTreeNode selectedNode) {
    if (selectedNode != rootNode) {
      displaySelectedScreen(selectedNode,
        (DetailPanelImp) selectedNode.getUserObject());
    }
  }

  public void displaySelectedScreen(DetailPanelImp screen) {
    displaySelectedScreen(null, screen);
  }

  public void displaySelectedScreen(DefaultMutableTreeNode selectedNode,
                                    DetailPanelImp inScreen) {
    JPanel screen = (JPanel) inScreen;
    screen.setVisible(true);
    screen.setOpaque(true);
    TreeNode parentNode = rootNode;
    if (selectedNode != null && selectedNode != rootNode) {
      parentNode = selectedNode.getParent();
    }
    //if ( parentNode == rootNode )
    //{
    layeredPane.moveToFront(screen);
    layeredPane.updateUI();
    layeredPane.repaint();
    //}
    //else
    //{
    //  inScreen.displayPanel();
    //}
    //tree.updateUI();
    this.updateUI();
    this.repaint();
  }

  //----------------------------------------------------------------------------
  public TreeMap<DefaultMutableTreeNode, DetailPanelImp>
  getDetailPanel(DefaultMutableTreeNode inNode) {
    return getDetailPanel();
  }

  public TreeMap<DefaultMutableTreeNode, DetailPanelImp> getDetailPanel() {
    TreeMap<DefaultMutableTreeNode, DetailPanelImp> detailnode =
      new TreeMap<DefaultMutableTreeNode, DetailPanelImp>();
    int i = 0;
    for (Enumeration e = rootNode.children(); e.hasMoreElements(); ) {
      DefaultMutableTreeNode node = (DefaultMutableTreeNode) e.nextElement();
      detailnode.put(node, (DetailPanelImp) node.getUserObject());
    }
    return detailnode;
  }

  //---------------------------------------------------------------------------
  public void commitedAction() {
  }

  public void actionPerformed(ActionEvent e) {
    String cmd = e.getActionCommand();
    if (ADD_COMMAND.equals(cmd)) {
      DetailPanelImp screen = null;
      DefaultMutableTreeNode selectedNode = getCurrentNode();
      DefaultMutableTreeNode parentNode = null;
      if (selectedNode == rootNode) {
        parentNode = selectedNode;
        selectedNode = (DefaultMutableTreeNode) parentNode.getFirstChild();
      }
      else {
        parentNode = (DefaultMutableTreeNode) selectedNode.getParent();
      }
      DetailPanelImp selectedpanel = (DetailPanelImp)
        selectedNode.getUserObject();
      DetailPanelImp parentPanel = (DetailPanelImp)
        parentNode.getUserObject();
      if (!selectedpanel.getFlag()) {
        try {
          System.out.println("add action");
          String panelname = selectedpanel.getTitle();
          System.out.println("panel name " + panelname);
          //screen = createDetail(parentNode, panelname);
          createDetail(parentNode, panelname);
          //screen.setOpaque(true);
        } catch (Exception ee) {
          System.out.println(
            "Failed to create Detail Panel, exception : " + ee.getMessage());
        }
        if (screen != null) {
          //DefaultMutableTreeNode node = addNode(screen, false );
          //layeredPane.add(screen, childPaneCount);
          //displaySelectedScreen(rootNode, screen);
          int paneCount = parentPanel.getChildCount();
          layeredPane.add(screen, paneCount);
          displaySelectedScreen(parentNode, screen);
        }
      }
      //else if ( selectedNode.getAllowsChildren() ) {
      //if ( !panel.getFlag() ) {
      //each detailpanel has childname setup as a public global variable
      //String name = panel.getChildName();
      //try {
      //screen = panel.createDetailPanel();
      //screen = panel.createChildPanel();
      //} catch ( Exception ex ) {
      //System.out.println("Error creating Child Detail Panel : " + e);
      //}
      //layeredPane.add(screen, screen.childPaneCount);
      //displaySelectedScreen(screen);
      //screen.displayPanel();
      //}
      //}
    }
    else if (REMOVE_COMMAND.equals(cmd)) {
      DefaultMutableTreeNode selectedNode = getCurrentNode();
      if (selectedNode != rootNode) {
        removeCurrentNode();
        DefaultMutableTreeNode nextNode = (DefaultMutableTreeNode)
          rootNode.getFirstChild();
        displaySelectedScreen(nextNode);
      }
      else {
        JOptionPane.showMessageDialog(this, "Can not remove this last Component!");
      }
    }
    else if (COMMIT_COMMAND.equals(cmd)) {
      commitedAction();
    }
  }

  public void valueChanged(TreeSelectionEvent tse) {
    DefaultMutableTreeNode node = getCurrentNode();
    if (node != rootNode) {
      DetailPanelImp panel = (DetailPanelImp) node.getUserObject();
      TreeNode parentNode = node.getParent();
      //if ( parentNode != rootNode )
      //  panel.displayPanel();
      //else
      displaySelectedScreen(node, panel);
      /*
      System.out.println("in valueChanged");
      if ( node.getAllowsChildren() )
      {
        System.out.println("after panel.displayPanel  and allows children : " + panel);
      }
      else 
      {
        System.out.println("in else after displaySelectedScreen");
      }
      */
    }
  }

  /*
  public void mousePressed(MouseEvent me)
  {}

  public void mouseExited(MouseEvent me)
  {}

  public void mouseEntered(MouseEvent me)
  {}

  public void mouseReleased(MouseEvent me)
  {System.out.println("mouse release");}

  public void mouseClicked(MouseEvent me)
  {}


  class MyTreeModelListener implements TreeModelListener 
  {
    public void treeNodesChanged(TreeModelEvent e) 
    {
      DefaultMutableTreeNode node;
      node = (DefaultMutableTreeNode)
               (e.getTreePath().getLastPathComponent());

       // If the event lists children, then the changed
       // node is the child of the node we've already
       // gotten.  Otherwise, the changed node and the
       // specified node are the same.

      try 
      {
        int index = e.getChildIndices()[0];
        node = (DefaultMutableTreeNode)
                 (node.getChildAt(index));
      } 
      catch (NullPointerException npx) 
      {}
    }

    public void treeNodesInserted(TreeModelEvent e) {}
    public void treeNodesRemoved(TreeModelEvent e) {}
    public void treeStructureChanged(TreeModelEvent e) {}
  }
  */
}
