package edu.utah.camplab.genie.gui2;

import javax.swing.*;

//=============================================================================
public class OptionTab extends TabImp {
  static final int INDEX = 0;
  ChoicePanel choicePanel;
  ParameterFilePanel parameterFilePanel;
  PackagePanel packagePanel;
  GenieGUI gui;

  public OptionTab() {
    super();
    title = "Option";
  }

  //----------------------------------------------------------------------------
  protected static ImageIcon createImageIcon(String path,
                                             String description) {
    java.net.URL imgURL = OptionTab.class.getResource(path);
    if (imgURL != null) {
      return new ImageIcon(imgURL, description);
    }
    else {
      System.err.println("Couldn't find file: " + path);
      return null;
    }
  }

  //----------------------------------------------------------------------------
  /*
  public void actionPerformed ( ActionEvent ae ) {
    Object source = ae.getSource();
    packagePanel = choicePanel.getPackagePanel();
    System.out.println( " ++ in PackageTab action " + packagePanel.getTest());
    //if ( source == commitB ) {
    //  setDisplayOnly();
      //gui.addTab("Study");
    //}
  }
  */

  //----------------------------------------------------------------------------
  public void build(GenieGUI inGUI) {
    super.build(inGUI);
    gui = inGUI;
    setLayout(new VerticalLayout());
    choicePanel = new ChoicePanel();
    choicePanel.setGUI(gui);
    add(choicePanel);
    choicePanel.setParentPanel(this);
  }

  //----------------------------------------------------------------------------
  public ParameterFilePanel getParameterFilePanel() {
    return parameterFilePanel;
  }

  //----------------------------------------------------------------------------
  public void setParameterFilePanel(ParameterFilePanel inPanel) {
    parameterFilePanel = inPanel;
  }

  //----------------------------------------------------------------------------
  public PackagePanel getPackagePanel() {
    return packagePanel;
  }
/*
  //----------------------------------------------------------------------------
  public class BasicPanel extends JPanel {
    public BasicPanel ( String titleName ) {
      super();
      TitledEtched titledEtched = new TitledEtched(titleName);
      setBorder(titledEtched.title);
      setPreferredSize(new Dimension(300, 90));
      setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
    }
  }

  //----------------------------------------------------------------------------
  public class PackagePanel extends JPanel
                        implements ActionListener {
    JRadioButton hapmcRB, pedGenieRB, hapConstructorRB;
    ButtonGroup pkgBgroup;
    //JOptionPane optionPane = new JOptionPane();
    public PackagePanel() {
      super();
      TitledEtched titledEtched = new TitledEtched("Package");
      setBorder(titledEtched.title);
      setPreferredSize(new Dimension(800, 200));
      setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
      pedGenieRB = new JRadioButton("PedGenie");
      hapmcRB = new JRadioButton("hapMC");
      hapConstructorRB = new JRadioButton("hapConstructor");
      pkgBgroup = new ButtonGroup();
      pkgBgroup.add(pedGenieRB);
      pkgBgroup.add(hapmcRB);
      pkgBgroup.add(hapConstructorRB);
      pedGenieRB.setActionCommand("PedGenie");
      pedGenieRB.addActionListener(this);
      hapmcRB.setActionCommand("hapMC");
      hapmcRB.addActionListener(this);
      //hapConstructorRB.setActionCommand("hapConstructor");
      //hapConstructorRB.addActionListener(this);
      add(pedGenieRB);
      add(hapmcRB);
      //add(hapConstructorRB);
    }
  
    public void actionPerformed(ActionEvent e) {
      changeDisplay(e.getActionCommand());
      //updateUI();
    }
  }

  //----------------------------------------------------------------------------
  public class VariablesPanel extends JPanel {
                              //implements PropertyChangeListener {
    JTextField rSeedT, numSimT;
 
    public VariablesPanel() {
      GridBagLayout gridbag = new GridBagLayout();
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.weightx = 1.0;
        constraints.gridwidth = 2;
        constraints.gridheight = 2;
        constraints.ipadx = 0;
        setLayout(gridbag); 
        TitledEtched titledEtched = new TitledEtched("Simulation Variables");
        setBorder(titledEtched.title);
        setPreferredSize(new Dimension(300, 90));
        JLabel rSeedL   = new JLabel("Random Number Seed ", JLabel.TRAILING);
        JLabel numSimL  = new JLabel("Number of Simulation ", JLabel.TRAILING);

        rSeedT  = new JTextField("Random", 10);
        rSeedL.setLabelFor(rSeedT);
        //rSeedT.addPropertyChangeListener(this);
        numSimT = new JTextField("100", 10);
        numSimL.setLabelFor(numSimT);
        //numSimT.addPropertyChangeListener(this);
        gridbag.setConstraints(rSeedL, constraints);
        gridbag.setConstraints(numSimL, constraints);
        constraints.gridwidth = GridBagConstraints.REMAINDER;
        gridbag.setConstraints(rSeedT, constraints);
        gridbag.setConstraints(numSimT, constraints);
        add(rSeedL);
        add(rSeedT);
        add(numSimL);
        add(numSimT);
    }

    //public void propertyChange(PropertyChangeEvent pce) {
    //    Object source = pce.getSource();
    //    if ( source == rSeedT )
    //      rSeed = rSeedT.getText();
    //    else if ( source == numSimT )
    //      numSim = numSimT.getText();
    //}
  }

  //----------------------------------------------------------------------------
  public class MissingAllelePanel extends JPanel  {
				//implements PropertyChangeListener {
    JTextField mAlleleT;
 
    public MissingAllelePanel() { 
      GridBagLayout gridbag = new GridBagLayout();
      GridBagConstraints constraints = new GridBagConstraints();
      constraints.weightx = 1.0;
      constraints.gridwidth = 2;
      constraints.gridheight = 2;
      constraints.ipadx = 0;
      setLayout(gridbag);
      TitledEtched titledEtched = new TitledEtched("Missing Allele");
      setBorder(titledEtched.title);
      setPreferredSize(new Dimension(300, 90));
      JLabel mAlleleL   = new JLabel("Missing Allele Code ", JLabel.TRAILING);
      mAlleleT  = new JTextField("0", 10);
      mAlleleL.setLabelFor(mAlleleT);
      //mAlleleT.addPropertyChangeListener(this);
      gridbag.setConstraints(mAlleleL, constraints);
      constraints.gridwidth = GridBagConstraints.REMAINDER;
      gridbag.setConstraints(mAlleleT, constraints);
      add(mAlleleL);
      add(mAlleleT);
    }

    //public void propertyChange(PropertyChangeEvent pce) {
    //    Object source = pce.getSource();
    //    if ( source == mAlleleT )
    //      mAllele = mAlleleT.getText();
    //}
  }
		
  //----------------------------------------------------------------------------
  public class AlleleFormatPanel extends BasicPanel
				implements ActionListener
  {
    ButtonGroup formatBgroup;
    JRadioButton numericRB, characterRB;

    public AlleleFormatPanel() {
	super("Allele Format");
        numericRB = new JRadioButton("Numeric", true);
        characterRB    = new JRadioButton("Character");
        numericRB.setActionCommand("numeric");
        characterRB.setActionCommand("character");
        formatBgroup  = new ButtonGroup();
        formatBgroup.add(numericRB);
        formatBgroup.add(characterRB);
        numericRB.addActionListener(this);
        characterRB.addActionListener(this);
        add(numericRB);
        add(characterRB);
    }

    public void actionPerformed (ActionEvent e) 
    {}
  }

  //----------------------------------------------------------------------------
  public class TopSimulation extends BasicPanel
                               implements ActionListener
  {
      ButtonGroup topBgroup;
      JRadioButton alleleFreqRB, hapFreqRB, xRB;

    public TopSimulation()
    {
        super("Top Simulation");
        alleleFreqRB = new JRadioButton("AlleleFreqTopSim", true);
        hapFreqRB    = new JRadioButton("HapFreqTopSim");
        xRB    = new JRadioButton("XTopSim");
        alleleFreqRB.setActionCommand("AlleleFreqTopSim");
        hapFreqRB.setActionCommand("HapFreqTopSim");
        xRB.setActionCommand("XTopSim");
        topBgroup  = new ButtonGroup();
        topBgroup.add(alleleFreqRB);
        topBgroup.add(hapFreqRB);
        topBgroup.add(xRB);
        alleleFreqRB.addActionListener(this);
        hapFreqRB.addActionListener(this);
        xRB.addActionListener(this);
        add(alleleFreqRB);
        add(hapFreqRB);
        add(xRB);
    }

    public void actionPerformed(ActionEvent e)
    {}

  }

  //----------------------------------------------------------------------------
  public class SamplingMethod extends BasicPanel
                                implements ActionListener
  {
    JRadioButton allRB, founderRB;
    ButtonGroup samplingBgroup;

    public SamplingMethod()
    {
 	super("Sampling Method");
        allRB              = new JRadioButton("all");
        founderRB          = new JRadioButton("founder");
        allRB.setActionCommand("all");
        founderRB.setActionCommand("founder");
        allRB.setSelected(true);
        samplingBgroup = new ButtonGroup();
        samplingBgroup.add(allRB);
        samplingBgroup.add(founderRB);
        add(allRB);
        add(founderRB);
        allRB.addActionListener(this);
        founderRB.addActionListener(this);
    }

    public void actionPerformed(ActionEvent e)
    {}
  }

  //----------------------------------------------------------------------------
  public class IntervalPanel extends BasicPanel
				implements ActionListener
  {
    JRadioButton yesRB, noRB;
    ButtonGroup intervalBgroup;
  
    public IntervalPanel()
    {
      super("Simulation Interval Check");
      yesRB = new JRadioButton("Yes");
      noRB  = new JRadioButton("No");
      yesRB.setActionCommand("yes");
      noRB.setActionCommand("no");
      yesRB.setSelected(true);
      intervalBgroup = new ButtonGroup();
      intervalBgroup.add(yesRB);
      intervalBgroup.add(noRB);
      add(yesRB);
      add(noRB);
      yesRB.addActionListener(this);
      noRB.addActionListener(this);
    }

    public void actionPerformed(ActionEvent e) {
    }
  }

  //----------------------------------------------------------------------------
  public class ReportPanel extends BasicPanel
                             implements ActionListener
  {
      JRadioButton detailRB, summaryRB, bothRB;
      ButtonGroup reportBgroup;

    public ReportPanel()
    {
        super("Report");
        detailRB           = new JRadioButton("Detail Report");
        summaryRB          = new JRadioButton("Summary Report");
        bothRB             = new JRadioButton("Summary and Detail Reports");
        detailRB.setActionCommand("report");
        summaryRB.setActionCommand("summary");
        bothRB.setActionCommand("both");
        detailRB.setSelected(true);
        reportBgroup = new ButtonGroup();
        reportBgroup.add(detailRB);
        reportBgroup.add(summaryRB);
        reportBgroup.add(bothRB);
        add(detailRB);
        add(summaryRB);
        add(bothRB);
        detailRB.addActionListener(this);
        summaryRB.addActionListener(this);
        bothRB.addActionListener(this);
    }

    public void actionPerformed(ActionEvent e)
    {}
  }

  //----------------------------------------------------------------------------
  public class DecodePanel extends BasicPanel
			      implements ActionListener
  {
    JRadioButton decodeYesRB, decodeNoRB;
    ButtonGroup decodeBgroup;
    DecodeFilePanel decodefilePanel = null;
    JButton helpB;

    public DecodePanel()
    {
	super("Decode Allele");
        decodeYesRB           = new JRadioButton("Yes");
        decodeNoRB           = new JRadioButton("No");
	decodeYesRB.setActionCommand("Yes");
	decodeNoRB.setActionCommand("No");
	decodeNoRB.setSelected(true);
	decodeBgroup = new ButtonGroup();
	decodeBgroup.add(decodeYesRB);
	decodeBgroup.add(decodeNoRB);
	decodeYesRB.addActionListener(this);
	decodeNoRB.addActionListener(this);
        helpB = new JButton(createImageIcon("/toolbarButtonGraphics/general/Help24.gif", "Explanation"));
        helpB.setContentAreaFilled(false);
        helpB.setBorderPainted(false);
        helpB.setHorizontalAlignment(JLabel.LEFT);
        helpB.addActionListener(this);
        helpB.setActionCommand("Help");

	add(decodeYesRB);
	add(decodeNoRB);
        add(helpB);
    }

    public void actionPerformed (ActionEvent e )
    {
	Object source = e.getSource();
	if ( source == decodeYesRB ) {
	  decodefilePanel = new DecodeFilePanel();
	  decodefilePanel.setOpaque(true);
	  pkginfo.add(decodefilePanel);
	  pkginfo.updateUI();
  	} else if ( source == helpB ) {
	  String mgs = "converts analysis table pattern allele code 1 and 2, common and rare, to the code in the pre-specified decode frequency file, or to the code same as the input data, based on their allele frequency.";
	  JOptionPane.showMessageDialog(this, mgs);
	}
    }

    public class DecodeFilePanel extends JPanel
  				  implements ActionListener {
	JButton decodefileB;
	JLabel decodefileL;
	JTextField decodefileT;
 	File decodefile;
	
	public DecodeFilePanel() {
	  TitledEtched titledEtched = new TitledEtched("Decode Frequency File");
          setBorder(titledEtched.title);
	  GridBagLayout gridbag = new GridBagLayout();
	  GridBagConstraints constraintsL = new GridBagConstraints();
          GridBagConstraints constraintsT = new GridBagConstraints();
          GridBagConstraints constraintsB = new GridBagConstraints();
          setLayout(gridbag);
          setPreferredSize(new Dimension(300, 90));
	  String nfs = new String("No File Selected");
	  decodefileT = new JTextField("");
	  decodefileT.setPreferredSize(new Dimension(280, 20));
	  decodefileB = new JButton("Browse");
	  decodefileB.addActionListener(this);
	  decodefile = null;
          int gridyVal = 0;
          constraintsT.ipadx = 0;
          constraintsT.ipady = 5;
          constraintsT.gridx = 0;
          constraintsT.gridy = 0;
          constraintsB.ipadx = 0;
          constraintsB.ipady = 0;
          constraintsB.gridx = 0;
          constraintsB.gridy = 25;
          constraintsB.gridwidth = 5;
          constraintsB.gridheight = 4;
          gridbag.setConstraints(decodefileT, constraintsT);
          gridbag.setConstraints(decodefileB, constraintsB);
          add(decodefileT);
	  add(decodefileB);
	}
	
	public void actionPerformed (ActionEvent e) {
	  Object source = e.getSource();
	  if ( source == decodefileB ) {
	    JFileChooser filechooser = new JFileChooser();
	    filechooser.setDialogTitle("Decode Frequency File");
	    int returnVal = filechooser.showOpenDialog(DecodePanel.this);
	    if ( returnVal == JFileChooser.APPROVE_OPTION ) {
	      decodefile = filechooser.getSelectedFile();
	      decodefileT.setText(decodefile.toString());
	    }
	  }
	}
    }  // end DecodefilePanel
  } // end DecodePanel
  
  //---------------------------------------------------------------------------
    public class DumpPanel extends JPanel
                           implements ActionListener
    {
      JRadioButton nodumpB, genodumpB, haplodumpB, tdtdumpB, descentdumpB;
      ButtonGroup dumperBG;

      public DumpPanel ()
      {
        TitledEtched titledEtched = new TitledEtched ("Dump Data");
        setBorder(titledEtched.title);
        setLayout(new GridLayout(5,1));
        nodumpB      = new JRadioButton("no dumping", true);
        genodumpB    = new JRadioButton("genotype data pre-makeped file");
        haplodumpB   = new JRadioButton("haplotype with frequency");
        tdtdumpB     = new JRadioButton("genotype with variable for TDT input");
        descentdumpB = new JRadioButton("genotype data in html format");
        dumperBG     = new ButtonGroup();
        dumperBG.add(nodumpB);
        dumperBG.add(genodumpB);
        dumperBG.add(haplodumpB);
        dumperBG.add(tdtdumpB);
        dumperBG.add(descentdumpB);
        nodumpB.addActionListener(this);
        genodumpB.addActionListener(this);
        haplodumpB.addActionListener(this);
        tdtdumpB.addActionListener(this);
        descentdumpB.addActionListener(this);
        add(nodumpB);
        add(genodumpB);
        add(tdtdumpB);
        add(descentdumpB);
        add(haplodumpB);
        haplodumpB.setVisible(false);
      }
 
      public void updateDisplay ( boolean isHapMC )
      {
        haplodumpB.setVisible(isHapMC);
        updateUI();
      }

      public void actionPerformed ( ActionEvent ae )
      {}
    }
        
    //--------------------------------------------------------------------------
    public void changeDisplay(String pkgName)
    {
      removeAll();
      JPanel panel = new JPanel();
      panel.setPreferredSize(new Dimension(UIConstants.SCREEN_WIDTH, 
					   UIConstants.SCREEN_HEIGHT));
      panel.setLayout(new BorderLayout());
      panel.setOpaque(true);
      pkginfo.setLayout(new GridLayout(5, 2));
      //pkginfo.setLayout(new VerticalLayout());
      pkginfo.setOpaque(true);
      commitB = new JButton("Commit and Build Study Tab");
      commitB.addActionListener(this);
      pkginfo.add(variablesPanel);
      //add(dumpPanel);
      if (pkgName.equals("PedGenie"))
      {
        pkginfo.add(topsimPanel);
      }
      pkginfo.add(alleleformatPanel);
      pkginfo.add(missingAllelePanel);
      pkginfo.add(samplingPanel);
      pkginfo.add(intervalPanel);
      pkginfo.add(reportPanel);
      pkginfo.add(dumpPanel);
      pkginfo.add(decodePanel);
      panel.add(pkginfo, BorderLayout.CENTER);
      panel.add(commitB, BorderLayout.SOUTH);
      add(panel);
    }
  //}

  //----------------------------------------------------------------------------
  public String getPkg()
  { return packagePanel.pkgBgroup.getSelection().getActionCommand(); }
  
  public String getRseed()
  { return variablesPanel.rSeedT.getText(); }

  public String getNsim()
  { return variablesPanel.numSimT.getText(); }

  public String getAlleleFormat()
  { return alleleformatPanel.formatBgroup.getSelection().getActionCommand(); }

  public String getMissingAllele() {
    return missingAllelePanel.mAlleleT.getText(); }

  public String getTop()
  { 
    if (getPkg().equals("hapMC"))
      return new String("HapMCTopTogether");
    else
      return topsimPanel.topBgroup.getSelection().getActionCommand(); 
  }

  public String getDrop()
  {
    if (getPkg().equals("hapMC"))
      return new String("HapMCDropTogether");
    else if ( getTop().equals("XTopSim") )
      return new String("XDropSim");
    else
      return new String("DropSim");
  }

  public String getSampling()
  { return samplingPanel.samplingBgroup.getSelection().getActionCommand();}

  public String getReport()
  { return reportPanel.reportBgroup.getSelection().getActionCommand(); }

  public String getDecode()
  { 
    return decodePanel.decodeBgroup.getSelection().getActionCommand(); 
  }

  public String getDecodeFile()
  {
    return decodePanel.decodefilePanel.decodefileT.getText(); 
  }

  public String getInterval()
  {
    return intervalPanel.intervalBgroup.getSelection().getActionCommand();
  }

  //---------------------------------------------------------------------------
  public VariablesPanel getVariablesPanel()
  { System.out.println("getVariablesPanel");
    return variablesPanel; }

  /*
  public String getDump()
  { return dumpPanel.dumpBgroup.getSelection().getActionCommand(); }
  */

  //----------------------------------------------------------------------------
  /*
  public void setDisplayOnly() 
  {
    Util.disable(packagePanel.commitB);
    Util.disable(packagePanel.variablesPanel.rSeedT);
    Util.disable(packagePanel.variablesPanel.numSimT);
    Util.disable(packagePanel.samplingPanel.samplingBgroup);
    Util.disable(packagePanel.reportPanel.reportBgroup);
    Util.disable(packagePanel.topsimPanel.topBgroup);
  }
  */

  //----------------------------------------------------------------------------
  public void setPackagePanel(PackagePanel inPanel) {
    packagePanel = inPanel;
  }
}
