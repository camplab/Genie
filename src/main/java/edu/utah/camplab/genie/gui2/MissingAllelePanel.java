package edu.utah.camplab.genie.gui2;

import javax.swing.*;
import java.awt.*;

public class MissingAllelePanel extends JPanel {
  JTextField mAlleleT;

  public MissingAllelePanel() {
    GridBagLayout gridbag = new GridBagLayout();
    GridBagConstraints constraints = new GridBagConstraints();
    constraints.weightx = 1.0;
    constraints.gridwidth = 2;
    constraints.gridheight = 2;
    constraints.ipadx = 0;
    setLayout(gridbag);
    TitledEtched titledEtched = new TitledEtched("Missing Allele");
    setBorder(titledEtched.title);
    setPreferredSize(new Dimension(300, 90));
    JLabel mAlleleL = new JLabel("Missing Allele Code ", JLabel.TRAILING);
    mAlleleT = new JTextField("0", 10);
    mAlleleL.setLabelFor(mAlleleT);
    //mAlleleT.addPropertyChangeListener(this);
    gridbag.setConstraints(mAlleleL, constraints);
    constraints.gridwidth = GridBagConstraints.REMAINDER;
    gridbag.setConstraints(mAlleleT, constraints);
    add(mAlleleL);
    add(mAlleleT);
  }

  public String getMissingAllele() {
    return mAlleleT.getText();
  }

  public void populate(Parameters params) {
    String missingCode = params.get(Parameters.Att.MISSINGDATA);
    if (!missingCode.equals("0")) {
      mAlleleT.setText(missingCode);
    }
  }
}
