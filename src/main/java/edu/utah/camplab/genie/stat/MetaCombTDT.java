//*****************************************************************************
// MetaCombTDT.java
//*****************************************************************************
package edu.utah.camplab.genie.stat;

//=============================================================================
public class MetaCombTDT extends CombTDT {

  //---------------------------------------------------------------------------
  public MetaCombTDT() {
    super();
    title = "Combined TDT - Two Tailed Test (normal distribution)";
    statType = "metaTDT";
  }
}
