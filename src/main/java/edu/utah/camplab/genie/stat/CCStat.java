//******************************************************************************
// CCStat.java
//******************************************************************************
package edu.utah.camplab.genie.stat;

import java.util.Vector;

//==============================================================================
public interface CCStat {

  public String getName();

  public String getStatType();

  public String getCompStatTitle();

  public ComparisonTracker newComparisonTracker();

  public ComparisonTracker newComparisonTracker(int df);

  public CCAnalysis.Table getTable(TableMaker tm);

  public CCAnalysis.Table getTable(TableMaker tm, Thread p);

  public CCAnalysis.Table[] getNonZeroTables(CCAnalysis a,
                                             CCAnalysis.Table[] t,
                                             ComparisonTracker compTracker);

  public Result computeAt0(CCAnalysis a, CCAnalysis.Table[] t);

  public Result computeAt0(CCAnalysis a, CCAnalysis.Table t);

  public Result computeAtX(CCAnalysis a, CCAnalysis.Table[] t);

  public Result computeAtX(CCAnalysis a, CCAnalysis.Table t);

  public Result computation(CCAnalysis a, CCAnalysis.Table t,
                            boolean allowMessages);

  public Result computation(CCAnalysis a, CCAnalysis.Table[] t,
                            boolean allowMessages);

  public Result getObservedResult(Result result, int refCol);

  public Result getInferentialResult(ComparisonTracker compTracker,
                                     int refCol);

  public String getObsExtraStatTitle();

  public Result getObsExtraStat(Result result);

  public String getInfExtraStatTitle();

  public Result getInfExtraStat(ComparisonTracker compTracker);

  public Vector<Vector<Double>> getInfSimStat(ComparisonTracker compTracker);

  public int getDegreeOfFreedom(int ncol);

  public String getMessage();

  public interface ComparisonTracker {
    public void setStartingResult(Result r0);

    public void compareResultAtX(Result rx);

    public Result getComparisonsResult();

    //public int[] gethitCount();
    //public int[] getComparisonCount();
    //public int[] getnotval();
    public Result gethitCount();

    public Result getComparisonCount();

    public Result getnotval();

    public String getMessages();

    public void setMessages(String inString);

    public void setDegreeOfFreedom(int df);
  }

  public interface Result {
    public int elementCount();

    public double[] doubleValues();

    public String toString();

    public String toScientificFormat();

    //public void setMaxFractionDigits(int maxFractionDigits);
    //public void setScientificNotation(String scientificNotation);
    public void setSeparator(String separator);

    public String getMessages();

    public void setMessages(String inStr);
  }
}
 
