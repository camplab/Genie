//*****************************************************************************
// CMHChiSqTrend.java
//*****************************************************************************
package edu.utah.camplab.genie.stat;

import edu.utah.camplab.genie.gm.Ptype;

//=============================================================================
public class CMHChiSqTrend extends CCStatImp {
  //---------------------------------------------------------------------------
  public CMHChiSqTrend() {
    title = "Cochran Mantel Haenszel Chi-Squared Trend";
  }

  //---------------------------------------------------------------------------
  public ComparisonTracker newComparisonTracker() {
    compTracker = new ScalarCompTracker();
    return compTracker;
  }

  public ComparisonTracker newComparisonTracker(int df) {
    return this.newComparisonTracker();
  }

  //---------------------------------------------------------------------------
  public int[] getColumnWeights(CCAnalysis a) {
    int[] colWt = a.getColumnWeights();
    return colWt;
  }

  //---------------------------------------------------------------------------
  public boolean checkColumnReq(CCAnalysis.Table t) {
    if (t.getColumnCount() < 3) {
      return false;
    }
    else {
      return true;
    }
  }

  //---------------------------------------------------------------------------
  public Result computation(CCAnalysis a,
                            CCAnalysis.Table[] t,
                            boolean allowMessages) {
    CCAnalysis.Table[] metaTable = getNonZeroTables(a, t, compTracker);
    if (metaTable == null || metaTable.length == 0) {
      res = resultManager.new Real(Double.NaN);
      if (allowMessages) {
        compTracker.setMessages
          ("WARNING: no valid tables for this analysis");
      }
    }
    else {
      int mt = metaTable.length;
      Number[][] rowTotals = new Number[mt][];
      Number[][] columnTotals = new Number[mt][];
      Number[] tableTotal = new Number[mt];
      Number[][] casecells = new Number[mt][];
      Number[][] controlcells = new Number[mt][];
      int[] columnwt = getColumnWeights(a);

      for (int i = 0; i < mt; i++) {
        ContingencyTable table = (ContingencyTable) metaTable[i];
        CCAnalysis.Table.Totals totals = table.getTotals();
        rowTotals[i] = totals.forRows();
        columnTotals[i] = totals.forColumns();
        tableTotal[i] = totals.forTable();
        casecells[i] = table.getRowFor(Ptype.CASE).getCells();
        controlcells[i] = table.getRowFor(Ptype.CONTROL).getCells();
      }
      try {
        if (checkColumnReq(t[0]))
        //resultAt0 = new ResultImp.Real(
        {
          res = StatUt.cmhChiSqTrend(casecells,
            controlcells,
            rowTotals,
            columnTotals,
            tableTotal,
            constantJ,
            columnwt);
        }

        else {
          //resultAt0 = new ResultImp.StringResult
          res = resultManager.new Real(Double.NaN);
          if (allowMessages) {
            compTracker.setMessages
              ("WARNING: table has less than 3 columns, **test** has not been performed.");
          }
        }
      } catch (Exception e) {
        //resultAt0 = new ResultImp.StringResult
        res = resultManager.new Real(Double.NaN);
        System.out.println("e.getMessage() " + e.getMessage());
        if (allowMessages) {
          compTracker.setMessages
            ("WARNING : " + e.getMessage());
        }
        //  ("WARNING: can not calculate var[n|H0], **test** has not been performed.");
      }
    }
    return res;
  }

  //---------------------------------------------------------------------------
  public CCAnalysis.Table getTable(TableMaker tm) {
    return tm.getContingencyTable();
  }

  //---------------------------------------------------------------------------
  public CCAnalysis.Table getTable(TableMaker tm, Thread p) {
    return tm.getContingencyTable(p);
  }
}
