//*****************************************************************************
// ORCompTrackerCI.java
// ---- this object is obsolete, replaced by RatiosComparisonTracker ----------
//*****************************************************************************
package edu.utah.camplab.genie.stat;

import java.util.Iterator;
import java.util.TreeMap;
import java.util.TreeSet;

//import java.util.Vector;
//=============================================================================
public class ORCompTrackerCI extends ORComparisonTracker {
  //public Vector<Vector<Double>> vRatio;
  public TreeMap<String, TreeSet<Double>> ratioMap =
    new TreeMap<String, TreeSet<Double>>();
  public TreeMap<String, ResultManager.Real> ciMap =
    new TreeMap<String, ResultManager.Real>();
  //---------------------------------------------------------------------------
  /*
  public void setStartingResult(CCStat.Result r0)
  {
    super.setStartingResult(r0);
    vRatio = new Vector<Vector<Double>>();
    for ( int i = 0; i < r0Vals.length; i++ )
    {
      vRatio.add(new Vector<Double>());
    }
  }
  */

  //-------------------------------------------------------------------------
  public void compareResultAtX(CCStat.Result rx) {
    System.out.println("CI wrong tracker");
    if (r0Map != null && !(rx instanceof ResultManager.Real)) {
      TreeMap<String, ResultManager.Real> rxMap = ((ResultManager.Series) rx).resultMap();
      for (Iterator<String> it = rxMap.keySet().iterator(); it.hasNext(); ) {
        String key = it.next();
        double rxValue = rxMap.get(key).doubleValues()[0];
        double r0Value = r0Map.get(key).doubleValues()[0];
        double obs1 = 1 / r0Value;
        if (Double.isNaN(rxValue) || Double.isInfinite(rxValue)) {
          incrementMap(notMap, key);
        }
        else {
          if ((r0Value > 1.0 && rxValue >= r0Value) ||
            (r0Value < 1.0 && rxValue <= r0Value)) {
            incrementMap(hitMap, key);
          }
          incrementMap(comparisonMap, key);

          TreeSet<Double> ratio;
          if (ratioMap.get(key) != null) {
            ratio = ratioMap.get(key);
            ratioMap.remove(key);
          }
          else {
            ratio = new TreeSet<Double>();
          }
          ratio.add(new Double(rxValue));
          ratioMap.put(key, ratio);
        }
      }
    }
  }

  //-------------------------------------------------------------------------
  public CCStat.Result getConfidenceIntervals() {
    String warning = "Warning : 95% CI is uninformative, likely due to sparse data or insufficient simulations";
    /*
    int nRatio = 1;
    if ( vRatio == null )
    {
      String[][]  ciPair = new String[1][2];
      ciPair[0][0] = "*";
      ciPair[0][1] = "*";
      return new ResultImp.PairSeries(ciPair);
    }
    if ( vRatio.size() > 0 )
    {
      nRatio = vRatio.size();
    }

    String[][]  ciPair = new String[nRatio][2];
    for ( int i = 0; i < nRatio; i++ )
    {
      ciPair[i][0] = "-";
      ciPair[i][1] = "-";
    }

    if ( nRatio != 0 )
    {
      for ( int i = 0; i < nRatio; i++ )
      {
        double firstCI, secondCI;
        if ( Double.isNaN(r0Vals[i]) )
        {
          messages = warning;
        }
        else 
        {
          firstCI = secondCI = 0.0;
          Vector<Double> vCI = (Vector<Double>) vRatio.get(i);
          if ( vCI.size() > 1 )
          {
            int ci1st = (new Double(comparisonCount[i] * 0.025)).intValue();
            if ( ci1st > 0 )
              ci1st -= 1;
            int ci2ndNext = (new Double(comparisonCount[i] * 0.975)).intValue();
            int ci2nd = ci2ndNext - 1;

            double valFloat = (comparisonCount[i] * 0.025) - ci1st;
            double val2nd    = ((Double) vCI.get(ci1st)).doubleValue();
            double val3rd    = ((Double) vCI.get(ci1st + 1)).doubleValue();
            double val97th   = ((Double) vCI.get(ci2nd)).doubleValue();
            double val98th   = ((Double) vCI.get(ci2ndNext)).doubleValue();
            double first     = val2nd + valFloat * (val3rd - val2nd) ;
            double second    = val97th + valFloat * (val98th - val97th);
            
            if ( r0Vals[i] < 1.0 )
            {
              firstCI = r0Vals[i] / first;
              secondCI = r0Vals[i] * first;
            }
            else if ( r0Vals[i] > 1.0 )
            {
              firstCI = r0Vals[i] / second;
              secondCI = r0Vals[i] * second;
            }
            else if ( r0Vals[i] == 1.0 )
            {
              firstCI = first;
              secondCI = second;
            }
            if ( Double.isNaN(firstCI) )
            {  
              messages = warning;
              if ( Double.isNaN(secondCI) )
              {
                ciPair[i][0] = "0";
                ciPair[i][1] = "infinity";
              }
              else
              {
                ciPair[i][0] = (new ResultImp.Real(secondCI)).toString();
                ciPair[i][1] = "infinity";
              }
            }
            else if ( Double.isNaN(secondCI) )
            {
              messages = warning;
              ciPair[i][0] = (new ResultImp.Real(firstCI)).toString();
              ciPair[i][1] = "infinity";
            }  
            else if ( firstCI > secondCI )
            {
              if ( Double.isInfinite(firstCI) )
                ciPair[i][1] =  "infinity";
              else 
                ciPair[i][1] = (new ResultImp.Real(firstCI)).toString(); 
              if ( Double.isInfinite(secondCI))
                ciPair[i][0] = "infinity";
              else
                ciPair[i][0] = (new ResultImp.Real(secondCI)).toString();
            }
            else 
            {
              if ( Double.isInfinite(firstCI) )
                ciPair[i][0] = "infinity";
              else
                ciPair[i][0] = (new ResultImp.Real(firstCI)).toString();
              if ( Double.isInfinite(secondCI))
                ciPair[i][1] = "infinity";
              else
                ciPair[i][1] = (new ResultImp.Real(secondCI)).toString(); 
            }
          }
        }
      }
    }
    return new ResultImp.PairSeries(ciPair);
    */
    TreeMap<String, ResultManager.Real> resultMap =
      new TreeMap<String, ResultManager.Real>();
    if (comparisonMap != null) {
      for (Iterator<String> it = ratioMap.keySet().iterator(); it.hasNext(); ) {
        String key = it.next();
        TreeSet<Double> value = ratioMap.get(key);
        Double[] ci = (Double[]) value.toArray(new Double[0]);
        double interval1 = 0.0;
        double interval2 = 0.0;
        double infinity = Double.POSITIVE_INFINITY;
        double r0Value = r0Map.get(key).doubleValues()[0];
        double firstCI, secondCI;
        if (!Double.isNaN(r0Value)) {
          firstCI = secondCI = 0.0;
          if (value.size() > 1) {
            //int comparison = comparisonMap.get(key).intValue();
            //int ci1st = (new Double(comparison * 0.025)).intValue();
            int nValue = value.size();
            int ci1st = (new Double(nValue * 0.025)).intValue();
            if (ci1st > 0) {
              ci1st -= 1;
            }
            int ci2ndNext = (new Double(nValue * 0.975)).intValue();
            int ci2nd = ci2ndNext - 1;
            double valFloat = (nValue * 0.025) - ci1st;
            double val2nd = ((Double) ci[ci1st]).doubleValue();
            double val3rd = ((Double) ci[ci1st + 1]).doubleValue();
            double val97th = ((Double) ci[ci2nd]).doubleValue();
            double val98th = ((Double) ci[ci2ndNext]).doubleValue();
            double first = val2nd + valFloat * (val3rd - val2nd);
            double second = val97th + valFloat * (val98th - val97th);
            if (r0Value < 1.0) {
              firstCI = r0Value / first;
              secondCI = r0Value * first;
            }
            else if (r0Value > 1.0) {
              firstCI = r0Value / second;
              secondCI = r0Value * second;
            }
            else if (r0Value == 1.0) {
              firstCI = first;
              secondCI = second;
            }
            if (Double.isNaN(firstCI)) {
              messages = warning;
              if (!Double.isNaN(secondCI)) {
                interval1 = secondCI;
              }
              interval2 = infinity;
            }
            else if (Double.isNaN(secondCI)) {
              messages = warning;
              interval1 = firstCI;
              interval2 = infinity;
            }
            else if (firstCI < secondCI) {
              interval1 = firstCI;
              interval2 = secondCI;
            }
            else {
              interval1 = secondCI;
              interval2 = firstCI;
            }
          }
        }
        double[] interval = new double[2];
        interval[0] = interval1;
        interval[1] = interval2;
        resultMap.put(key, resultManager.new Real(interval));
      }
    }
    return resultManager.new Series(resultMap);
  }

  //----------------------------------------------------------------------------
  //public Vector<Vector<Double>> getSimulatedResults()
  //return vRatio;
  public TreeMap<String, TreeSet<Double>> getSimulatedResults() {
    return ratioMap;
  }

  public int getDF() {
    return 4;
  }
}
