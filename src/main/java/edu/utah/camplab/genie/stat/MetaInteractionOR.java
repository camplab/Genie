//*****************************************************************************
// MetaOddsRatios.java
//*****************************************************************************
package edu.utah.camplab.genie.stat;

//=============================================================================
public class MetaInteractionOR extends MetaOddsRatios {
  //---------------------------------------------------------------------------
  public MetaInteractionOR() {
    title = "Meta Interaction OR, 2-Tailed Test";
    //infExtraStatTitle = "Empirical Confidence Intervals : ";
  }

  //---------------------------------------------------------------------------
  public Result computation(CCAnalysis a,
                            CCAnalysis.Table[] t,
                            boolean allowMessages) {
    Result res = null;
    String msg = "";
    boolean hasError = false;
    if (a.checkDistinctRefWt()) {
      if (getdata(a, t))
      //return new  ResultImp.Real (
      {
        res = StatUt.metaInteractionOR(caseCell,
          controlCell,
          ctotal,
          refColIndex);
      }
      else {
        msg = "WARNING: no valid tables for this analysis";
        hasError = true;
      }
    }
    else {
      msg = "WARNING: Analysis Table has more than one reference column, **test** has not been performed.";
      hasError = true;
    }
    if (hasError) {
      res = resultManager.new Real(Double.NaN);
      if (allowMessages) {
        res.setMessages(msg);
      }
    }
    return res;
  }
}
