//*****************************************************************************
// ORComparisonTracker.java
// ---- this object is obsolete, replaced by RatiosComparisonTracker ------
//*****************************************************************************
package edu.utah.camplab.genie.stat;

import java.util.Iterator;
import java.util.TreeMap;

//=============================================================================
public class ORComparisonTracker implements CCStat.ComparisonTracker {
  public int[] notval;
  public int[] comparisonCount;
  public double[] r0Vals;
  public TreeMap<String, ResultManager.Real> r0Map;
  public TreeMap<String, ResultManager.Real> hitMap;
  public TreeMap<String, ResultManager.Real> notMap;
  public TreeMap<String, ResultManager.Real> comparisonMap;
  public String messages = null;
  public ResultManager resultManager = new ResultManager();
  protected int[] hitCount;
  protected int degreeF;

  //-----------------------------------------------------------------------
  public void setStartingResult(CCStat.Result r0) {
    /*
    if ( !(r0 instanceof ResultImp.StringSeries) )
    {
      assert r0 instanceof ResultImp.RealSeries;
      //int      ntriplets = r0.elementCount();
      r0Vals = r0.doubleValues();

      //r0statistics = new double[ntriplets];
      //for (int i = 0; i < r0.length; ++i)
      //  r0statistics[i] = r0Vals[i];
      comparisonCount = new int[r0Vals.length];
      notval = new int[r0Vals.length];
      hitCount = new int[r0Vals.length];
      for ( int i = 0; i < r0Vals.length; i++ )
        comparisonCount[i] = notval[i] = hitCount[i]= 0;
    }
    */
    if (r0 instanceof ResultManager.Series) {
      r0Map = ((ResultManager.Series) r0).resultMap();
      hitMap = new TreeMap<String, ResultManager.Real>();
      notMap = new TreeMap<String, ResultManager.Real>();
      comparisonMap = new TreeMap<String, ResultManager.Real>();
    }
  }

  //-----------------------------------------------------------------------
  public void compareResultAtX(CCStat.Result rx) {
    System.out.println("using wrong comparison tracker");
    //double[] rxVals = rx.doubleValues();
    //for (int i = 0, n = rxVals.length; i < n; ++i)
    if (r0Map != null) {
      if (rx == null || (rx instanceof ResultManager.Real)) {
        for (Iterator<String> it = r0Map.keySet().iterator(); it.hasNext(); ) {
          String key = it.next();
          incrementMap(notMap, key);
        }
      }
      else {
        TreeMap<String, ResultManager.Real> rxMap =
          ((ResultManager.Series) rx).resultMap();
        for (Iterator<String> it = rxMap.keySet().iterator(); it.hasNext(); ) {
          String key = it.next();
          double rxValue = rxMap.get(key).doubleValues()[0];
          double r0Value = r0Map.get(key).doubleValues()[0];
          if (!Double.isNaN(r0Value) || !Double.isInfinite(r0Value)) {
            if (Double.isNaN(rxValue)) {
              incrementMap(notMap, key);
            }
            else {
              if (r0Value > 1.0 && rxValue >= r0Value) {
                incrementMap(hitMap, key);
              }
              else if (r0Value < 1.0 && rxValue <= r0Value) {
                incrementMap(hitMap, key);
              }
              incrementMap(comparisonMap, key);
            }
          }
        }
      }
    }
  }

  /*
  double obs1 = 1 / obs;
  if ( Double.isNaN(sim) )
    notval[i]++;
  else 
  {
      if ( obs > 1.0 && sim >= obs ) 
        hitCount[i]++;
      else if ( obs < 1.0 && sim <= obs )
        hitCount[i]++;
      comparisonCount[i]++;
  }
  i++;

    //System.out.println("Ratio : " + i + " Sim result : " + sim + " hitcount : " + hitCount[i]);
  
      //if ( vCI[i].size() != 0)
      if ( vCI.size() != 0 )
      {
        int a = vCI.size();
        for (int j = 0; j < a; j++ )
        {
          if ( rxVals[i] > ((Double) vCI.get(j)).doubleValue() )
          {
            if ( j == a - 1 )
            {
              vCI.add(new Double(rxVals[i]));
              break;
            }
          }
          else
          {
            vCI.add( j, new Double(rxVals[i]) );
            break;
          }
        }
      }
      else 
      {
        vCI.add(new Double(rxVals[i]));
      }
  }
  //vRatio.setElementAt(vCI, i);
  */
  //--------------------------------------------------------------------------
  public CCStat.Result getComparisonsResult() {
    //double[] pvals = new double[hitCount.length];
    TreeMap<String, ResultManager.Real> resultMap =
      new TreeMap<String, ResultManager.Real>();
    if (r0Map != null) {
      for (Iterator<String> it = r0Map.keySet().iterator(); it.hasNext(); ) {
        double pval = 0.0;
        String key = it.next();
        ResultManager.Real value = r0Map.get(key);
        double r0Value = r0Map.get(key).doubleValues()[0];
        int hit = 0;
        int comparison = 0;
        if (hitMap.get(key) != null) {
          hit = hitMap.get(key).intValue();
        }
        if (comparisonMap.get(key) != null) {
          comparison = comparisonMap.get(key).intValue();
        }
        if (Double.isNaN(r0Value))
        //pval = r0Vals[i];
        {
          pval = r0Value;
        }
        //else if ( r0Vals[i] == 1.0 )
        else if (r0Value == 1.0) {
          pval = 1.0;
        }
        //else if ( hit != 0 && comparison != 0 ) {
        else {
          if (hit * 2 < comparison) {
            hit = hit * 2;
          }
          else {
            hit = comparison;
          }
          pval = hit / (double) comparison;
          //hitMap.remove(key);
          //hitMap.put(key, resultManager.new Real(hit));
        }
        resultMap.put(key,
          resultManager.new Real(pval, new String(hit + "/" + comparison)));
      }
    }
    else {
      resultMap.put("-", resultManager.new Real(Double.NaN));
    }

    //return ResultImp.convertTriplet( new ResultImp.RealSeries(pvals), 
    //                                 numCol,
    //                                 referenceColIndex );
    //return new ResultImp.RealSeries(pvals);
    return resultManager.new Series(resultMap);
  }

  //------------------------------------------------------------------------
  public CCStat.Result getnotval() {
    return resultManager.new Series(notMap);
  }

  //------------------------------------------------------------------------
  public CCStat.Result gethitCount() {
    return resultManager.new Series(hitMap);
  }

  //------------------------------------------------------------------------
  public CCStat.Result getComparisonCount() {
    return resultManager.new Series(comparisonMap);
  }

  //------------------------------------------------------------------------
  public String getMessages() {
    return messages;
  }

  //------------------------------------------------------------------------
  public void setMessages(String inMessages) {
    messages = inMessages;
  }

  //------------------------------------------------------------------------
  public void setDegreeOfFreedom(int df) {
    degreeF = df;
  }

  //------------------------------------------------------------------------
  public void incrementMap(TreeMap<String, ResultManager.Real> inMap,
                           String key) {
    int count = 0;
    ;
    if (inMap.get(key) != null) {
      count = inMap.get(key).intValue();
    }
    ;
    count++;
    inMap.remove(key);
    inMap.put(key, resultManager.new Real(count));
  }
}
