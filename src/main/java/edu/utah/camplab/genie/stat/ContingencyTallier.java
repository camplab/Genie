//******************************************************************************
// ContingencyTallier.java
//******************************************************************************
package edu.utah.camplab.genie.stat;

import edu.utah.camplab.genie.gm.Gtype;
import edu.utah.camplab.genie.gm.Ptype;

//==============================================================================
class ContingencyTallier extends TallierImp {
  protected static final int NPTYPES = Ptype.instanceCount();

  protected final Ptype[] thePtypes;
  protected final CCAnalysis.Table.Column[] theCols;
  protected final String theAtype;
  protected final int[] pid2RowIx = new int[NPTYPES];
  protected final int repeatIndex;
  //protected final Map				mGt2ColIx = new HashMap();

  //----------------------------------------------------------------------------
  ContingencyTallier(Ptype[] ptypes,
                     CCAnalysis.Table.Column[] cols,
                     String atype,
                     int inRepeatIndex) {
    super(ptypes.length, cols.length);
    thePtypes = ptypes;
    theCols = cols;
    theAtype = atype;
    //nRows = thePtypes.length;
    //nCols = theCols.length;
    repeatIndex = inRepeatIndex;
    theCounters = createCounters();

    // establish a mapping from row phenotype ids back to row indices
    for (int irow = 0; irow < nRows; ++irow) {
      int r = Integer.parseInt(thePtypes[irow].getID());
      //pid2RowIx[thePtypes[irow].getID()] = irow;
      pid2RowIx[r] = irow;
    }
  }

  //----------------------------------------------------------------------------
  void countExpressionEvent(Gtype gt, Ptype pt) {
    countExpressionEvent(gt, pt, null);
  }

  //----------------------------------------------------------------------------
  void countExpressionEvent(Gtype gt, Ptype pt, Thread p) {
    /*
    for (int icol = 0; icol < nCols; ++icol)
    {
      //if (theCols[icol].subsumesGtype(gt))
      //{
      //  theCounters[pid2RowIx[pt.getID()]][icol].increment();
      //  return;
      //}
      //
      int value = theCols[icol].subsumesGtype(gt);
      if ( value > 0 )
      {
        int c = Integer.parseInt(pt.getID());
        //theCounters[pid2RowIx[pt.getID()]][icol].add(value);
        theCounters[pid2RowIx[c]][icol].add(value);
      }
      if ( !theAtype.toLowerCase().matches("allele") && value > 0 )
        return;
    }
    */

    //System.out.println("---- new Gt -- ");
    int c = Integer.parseInt(pt.getID());
    if (theAtype.equals("allele")) {
      for (int i = 0; i < 2; i++) {
        boolean first = ((i == 0) ? true : false);
        for (int icol = 0; icol < nCols; icol++) {
          //System.out.println("matching column : " + icol );
          if ((theCols[icol].subsumesAtype(gt, first, repeatIndex)) == 1) {
            theCounters[pid2RowIx[c]][icol].add(1);
            //System.out.println("insert row : "+pid2RowIx[c]+" col : " +icol);
            break;
          }
        }
      }
    }
    else {
      for (int icol = 0; icol < nCols; icol++) {
        if ((theCols[icol].subsumesGtype(gt, repeatIndex)) == 1) {
          theCounters[pid2RowIx[c]][icol].add(1);
          //System.out.println("insert row : " +pid2RowIx[c]+ " col : " + icol);
          break;
        }
      }
    }
    //else 
    //System.out.println(" not insert - row : " +  pid2RowIx[c] + " col : " + icol + " subsume value : " + value);
  }

  //----------------------------------------------------------------------------
  public CCAnalysis.Table extractTable() {
    return new ContingencyTable(thePtypes, theCols, theCounters, pid2RowIx);
  }
}
