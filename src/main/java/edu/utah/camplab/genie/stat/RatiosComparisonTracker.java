//*****************************************************************************
// RatiosComparisonTracker.java
//*****************************************************************************
package edu.utah.camplab.genie.stat;

import java.util.Iterator;
import java.util.TreeMap;
import java.util.TreeSet;

//=============================================================================
public class RatiosComparisonTracker implements CCStat.ComparisonTracker {
  public TreeMap<String, ResultManager.Real> r0Map = null;
  public TreeMap<String, ResultManager.Real> hitMap = null;
  public TreeMap<String, ResultManager.Real> notMap = null;
  public TreeMap<String, ResultManager.Real> comparisonMap = null;
  public TreeMap<String, ResultManager.Real> ciMap = null;
  public TreeMap<String, TreeSet<Double>> ratioMap;
  //public TreeMap<String, ArrayList<Double>> ratioMap = null;
  public String messages = null;
  public ResultManager resultManager = new ResultManager();
  public boolean twoSided = true;
  public boolean confidenceIntervals = false;
  public double threshold = 0.0;
  protected int degreeF;

  //-----------------------------------------------------------------------
  public RatiosComparisonTracker(boolean twoSided,
                                 boolean confidenceIntervals,
                                 double threshold) {
    this.twoSided = twoSided;
    this.confidenceIntervals = confidenceIntervals;
    this.threshold = threshold;
    if (confidenceIntervals) {
      ciMap = new TreeMap<String, ResultManager.Real>();
      ratioMap = new TreeMap<String, TreeSet<Double>>();
      //ratioMap = new TreeMap<String, ArrayList<Double>>();
    }
  }

  //-----------------------------------------------------------------------
  public void setStartingResult(CCStat.Result r0) {
    if (r0 instanceof ResultManager.Series) {
      r0Map = ((ResultManager.Series) r0).resultMap();
      hitMap = new TreeMap<String, ResultManager.Real>();
      notMap = new TreeMap<String, ResultManager.Real>();
      comparisonMap = new TreeMap<String, ResultManager.Real>();
    }
  }

  //-----------------------------------------------------------------------
  public void compareResultAtX(CCStat.Result rx) {
    int eq = 0;
    if (r0Map != null) {
      if (rx == null || (rx instanceof ResultManager.Real)) {
        for (Iterator<String> it = r0Map.keySet().iterator(); it.hasNext(); ) {
          String key = it.next();
          incrementMap(notMap, key);
        }
      }
      else {
        TreeMap<String, ResultManager.Real> rxMap =
          ((ResultManager.Series) rx).resultMap();
        for (Iterator<String> it = rxMap.keySet().iterator(); it.hasNext(); ) {
          String key = it.next();
          double rxValue = rxMap.get(key).doubleValues()[0];
          double r0Value = r0Map.get(key).doubleValues()[0];
          if (!Double.isNaN(r0Value) && !Double.isInfinite(r0Value)) {
            if (Double.isNaN(rxValue)) {
              incrementMap(notMap, key);
            }
            else {
              if (key.equals("overall")) {
                if (rxValue >= r0Value) {
                  incrementMap(hitMap, key);
                }
              }
              else {
                if (r0Value == rxValue) {
                  incrementMap(hitMap, key);
                }
                else if (twoSided) {
                  if ((r0Value > threshold && rxValue >= r0Value) ||
                    (r0Value < threshold && rxValue <= r0Value)) {
                    incrementMap(hitMap, key);
                  }
                }
                else {
                  if (Math.abs(rxValue) >= Math.abs(r0Value)) {
                    incrementMap(hitMap, key);
                  }
                }
              }
              incrementMap(comparisonMap, key);
              if (confidenceIntervals) {
                TreeSet<Double> ratio;
                if (ratioMap.get(key) != null) {
                  ratio = ratioMap.get(key);
                  ratioMap.remove(key);
                }
                else {
                  ratio = new TreeSet<Double>();
                }
                ratio.add(new Double(rxValue));
                ratioMap.put(key, ratio);
              }
            }
          }
        }
      }
    }
  }

  //--------------------------------------------------------------------------
  public CCStat.Result getComparisonsResult() {
    TreeMap<String, ResultManager.Real> resultMap =
      new TreeMap<String, ResultManager.Real>();
    if (r0Map != null) {
      for (Iterator<String> it = r0Map.keySet().iterator(); it.hasNext(); ) {
        double pval = 0.0;
        String key = it.next();
        ResultManager.Real value = r0Map.get(key);
        double r0Value = r0Map.get(key).doubleValues()[0];
        int hit = 0;
        int comparison = 0;
        if (hitMap.get(key) != null) {
          hit = hitMap.get(key).intValue();
        }
        if (comparisonMap.get(key) != null) {
          comparison = comparisonMap.get(key).intValue();
        }
        if (Double.isNaN(r0Value)) {
          pval = r0Value;
        }
        else if (r0Value == threshold) {
          pval = 1.0;
        }
        else {
          if (!key.equals("overall")) {
            if (twoSided) {
              if (hit * 2 < comparison) {
                hit = hit * 2;
              }
              else {
                hit = comparison;
              }
            }
          }
          pval = hit / (double) comparison;
        }
        resultMap.put(key,
          resultManager.new Real(pval, new String(hit + "/" + comparison)));
      }
    }
    else {
      resultMap.put("-", resultManager.new Real(Double.NaN));
    }

    return resultManager.new Series(resultMap);
  }

  //------------------------------------------------------------------------
  public CCStat.Result getConfidenceIntervals() {
    String warning = "Warning : 95% CI is uninformative, likely due to sparse data or insufficient simulations";
    TreeMap<String, ResultManager.Real> resultMap =
      new TreeMap<String, ResultManager.Real>();
    if (comparisonMap != null) {
      for (Iterator<String> it = ratioMap.keySet().iterator();
           it.hasNext(); ) {
        String key = it.next();
        TreeSet<Double> value = ratioMap.get(key);
        //ArrayList<Double> value = ratioMap.get(key);
        //Collections.sort(value); //It's a tree afterall
        Double[] ci = (Double[]) value.toArray(new Double[0]);
        double interval1 = 0.0;
        double interval2 = 0.0;
        double infinity = Double.POSITIVE_INFINITY;
        double r0Value = r0Map.get(key).doubleValues()[0];
        double firstCI, secondCI;
        if (!Double.isNaN(r0Value)) {
          firstCI = secondCI = 0.0;
          if (value.size() > 1) {
            //int comparison = comparisonMap.get(key).intValue();
            //int ci1st = (new Double(comparison * 0.025)).intValue();
            int nValue = value.size();
            int ci1st = (new Double(nValue * 0.025)).intValue();
            if (ci1st > 0) {
              ci1st -= 1;
            }
            int ci2ndNext = (new Double(nValue * 0.975)).intValue();
            int ci2nd = ci2ndNext - 1;
            double valFloat = (nValue * 0.025) - ci1st;
            double val2nd = ((Double) ci[ci1st]).doubleValue();
            double val3rd = ((Double) ci[ci1st + 1]).doubleValue();
            double val97th = ((Double) ci[ci2nd]).doubleValue();
            double val98th = ((Double) ci[ci2ndNext]).doubleValue();
            double first = val2nd + valFloat * (val3rd - val2nd);
            double second = val97th + valFloat * (val98th - val97th);
            if (r0Value < 1.0) {
              firstCI = r0Value / first;
              secondCI = r0Value * first;
            }
            else if (r0Value > 1.0) {
              firstCI = r0Value / second;
              secondCI = r0Value * second;
            }
            else if (r0Value == 1.0) {
              firstCI = first;
              secondCI = second;
            }
            if (Double.isNaN(firstCI)) {
              messages = warning;
              if (!Double.isNaN(secondCI)) {
                interval1 = secondCI;
              }
              interval2 = infinity;
            }
            else if (Double.isNaN(secondCI)) {
              messages = warning;
              interval1 = firstCI;
              interval2 = infinity;
            }
            else if (firstCI < secondCI) {
              interval1 = firstCI;
              interval2 = secondCI;
            }
            else {
              interval1 = secondCI;
              interval2 = firstCI;
            }
          }
        }
        double[] interval = new double[2];
        interval[0] = interval1;
        interval[1] = interval2;
        resultMap.put(key, resultManager.new Real(interval));
      }
    }
    return resultManager.new Series(resultMap);
  }

  //------------------------------------------------------------------------
  public CCStat.Result getnotval() {
    return resultManager.new Series(notMap);
  }

  //------------------------------------------------------------------------
  public CCStat.Result gethitCount() {
    if (hitMap == null) {
      //System.out.println("RatiosComparisonTracker hitMap is null");
      return null;
    }
    return resultManager.new Series(hitMap);
  }

  //------------------------------------------------------------------------
  public CCStat.Result getComparisonCount() {
    return resultManager.new Series(comparisonMap);
  }

  //------------------------------------------------------------------------
  public String getMessages() {
    return messages;
  }

  //------------------------------------------------------------------------
  public void setMessages(String inMessages) {
    messages = inMessages;
  }

  //------------------------------------------------------------------------
  public void setDegreeOfFreedom(int df) {
    degreeF = df;
  }

  //------------------------------------------------------------------------
  public TreeMap<String, TreeSet<Double>> getSimulatedResults()
  //public TreeMap<String, ArrayList<Double>> getSimulatedResults()
  {
    return ratioMap;
  }

  //------------------------------------------------------------------------
  public void incrementMap(TreeMap<String, ResultManager.Real> inMap,
                           String key) {
    int count = 0;
    ;
    if (inMap.get(key) != null) {
      count = inMap.get(key).intValue();
      ;
      inMap.remove(key);
    }
    count++;
    inMap.put(key, resultManager.new Real(count));
  }
}
