//******************************************************************************
// ResultManager.java
//******************************************************************************
package edu.utah.camplab.genie.stat;

import edu.utah.camplab.genie.app.rgen.Specification;

import java.text.NumberFormat;
import java.util.Iterator;
import java.util.TreeMap;

//==============================================================================
public class ResultManager {
  //static int maxFractionDigits;
  //protected String separator = " ";
  //protected double[] theVal;
  //protected int nElement;
  //protected String[] infoString;
  protected NumberFormat fmt = Specification.getResultFormat();
  protected NumberFormat scientificfmt = Specification.getScientificFormat();

  //----------------------------------------------------------------------------
  public class ResultImp implements CCStat.Result {
    public String warning = null;
    protected String separator = " ";
    protected double[] theVal;
    protected int nElement;
    protected String[] infoString;

    //--------------------------------------------------------------------------
    public double[] doubleValues() {
      //double[] returnVal = theVal;
      return theVal;
    }

    //--------------------------------------------------------------------------
    public int elementCount() {
      return nElement;
    }

    //--------------------------------------------------------------------------
    public void setSeparator(String inSeparator) {
      separator = inSeparator;
    }

    //--------------------------------------------------------------------------
    public String toString() {
      return "";
    }

    public String toScientificFormat() {
      return "";
    }

    //--------------------------------------------------------------------------
    public String[] getFractionValue() {
      return infoString;
    }

    //--------------------------------------------------------------------------
    public String getMessages() {
      return warning;
    }

    //--------------------------------------------------------------------------
    public void setMessages(String inStr) {
      warning = inStr;
    }
  } // end of ResultImp

  //----------------------------------------------------------------------------
  public class Real extends ResultManager.ResultImp {
    public int intVal;

    //--------------------------------------------------------------------------
    public Real(int val) {
      nElement = 1;
      theVal = new double[1];
      theVal[0] = (new Integer(val)).doubleValue();
      intVal = val;
    }

    public Real(double val) {
      nElement = 1;
      theVal = new double[1];
      theVal[0] = val;
    }

    public Real(double[] val) {
      nElement = val.length;
      theVal = val;
    }

    public Real(double val, String infoString) {
      nElement = 1;
      theVal = new double[1];
      theVal[0] = val;
      this.infoString = new String[1];
      this.infoString[0] = infoString;
    }

    public Real(double[] val, String[] infoString) {
      nElement = val.length;
      theVal = val;
      this.infoString = infoString;
    }

    //--------------------------------------------------------------------------
    public String toString() {
      return formatResult(false);
    }

    public String toScientificFormat() {
      return formatResult(true);
    }

    public String formatResult(boolean scientific) {
      boolean hasInfo = false;
      if (infoString != null) {
        hasInfo = true;
      }
      int nVal = theVal.length;
      String returnStr = "";
      for (int i = 0; i < nVal; i++) {
        if (nVal >= 2) {
          if (i == 0) {
            returnStr += "(";
          }
        }
        if (Double.isNaN(theVal[i])) {
          returnStr += "-";
        }
        else if (Double.isInfinite(theVal[i])) {
          returnStr += "infinity";
        }
        else if (theVal[i] == 0.0) {
          returnStr += 0;
        }
        else {
          if (scientific) {
            returnStr += scientificfmt.format(theVal[i]);
          }
          else {
            returnStr += fmt.format(theVal[i]);
          }
        }
        if (hasInfo) {
          returnStr += " (" + infoString[i] + ")";
        }
        if (i + 1 < nVal) {
          returnStr += separator;
        }
        else if (nVal >= 2) {
          returnStr += ")";
        }
      }
      return returnStr;
    }

    //--------------------------------------------------------------------------
    public int intValue() {
      return intVal;
    }
  } // end Real

  //----------------------------------------------------------------------------
  public class Series extends ResultManager.ResultImp {
    protected TreeMap<String, Real> resultMap =
      new TreeMap<String, Real>();
    ;
    protected String separator = ":";

    //--------------------------------------------------------------------------
    public Series(TreeMap<String, Real> inMap) {
      //if ( inMap.size() == 0 ) 
//	System.out.println("ResultManager Series encounter inMap == null ");
      nElement = inMap.size();
      resultMap = inMap;
    }

    //--------------------------------------------------------------------------
    public String toString() {
      return formatResult(false);
    }

    public String toScientificFormat() {
      return formatResult(true);
    }

    public String formatResult(boolean scientific) {
      String returnStr = "";
      for (Iterator<String> it = resultMap.keySet().iterator();
           it.hasNext(); ) {
        String key = it.next();
        Real value = resultMap.get(key);
        //value.setMaxFractionDigits(this.maxFractionDigits);
        //value.setScientificNotation(this.scientificNotation);
        value.setSeparator(", ");
        String valueStr;
        if (scientific) {
          valueStr = value.toScientificFormat();
        }
        else {
          valueStr = value.toString();
        }
        returnStr += key + ": " + valueStr + "  ";
      }
      return returnStr;
    }

    //--------------------------------------------------------------------------
    public double[] doubleValues() {
      int nValue = 0;
      for (Iterator<String> iter = resultMap.keySet().iterator();
           iter.hasNext(); ) {
        String key = iter.next();
        Real value = resultMap.get(key);
        nValue += (value.doubleValues()).length;
      }
      theVal = new double[nValue];
      int i = 0;
      for (Iterator<String> iter = resultMap.keySet().iterator();
           iter.hasNext(); ) {
        String key = iter.next();
        Real value = resultMap.get(key);
        for (double v : value.doubleValues()) {
          theVal[i] = v;
          i++;
        }
      }
      return theVal;
    }

    //--------------------------------------------------------------------------
    public CCStat.Result doubleValuesAtKey(String key) {
      return resultMap.get(key);
    }

    //--------------------------------------------------------------------------
    public TreeMap<String, Real> resultMap() {
      return resultMap;
    }
  } // end RealSeries

  //----------------------------------------------------------------------------
  public class StringResult extends ResultManager.ResultImp {
    String resultStr;

    public StringResult(String inStr) {
      nElement = 1;
      theVal = new double[]{0.0};
      resultStr = inStr;
    }

    public String toString() {
      return resultStr;
    }
  }  // end StringResult
}
