//*****************************************************************************
// MetaOddsRatiosWithCI.java
//*****************************************************************************
package edu.utah.camplab.genie.stat;

//=============================================================================
public class MetaInteractionORWithCI extends MetaInteractionOR {
  //---------------------------------------------------------------------------
  public MetaInteractionORWithCI() {
    super();
    infExtraStatTitle = "Empirical Confidence Intervals : ";
  }

  //---------------------------------------------------------------------------
  public ComparisonTracker newComparisonTracker() {
    //compTracker = new ORCompTrackerExt();
    //return new ORCompTrackerCI();
    return new RatiosComparisonTracker(true, true, 1.0);
  }

  public ComparisonTracker newComparisonTracker(int df) {
    return this.newComparisonTracker();
  }

  //----------------------------------------------------------------------------
  public Result getInfExtraStat(ComparisonTracker compTracker) {
    //ORCompTrackerCI ORct = (ORCompTrackerCI) compTracker;
    RatiosComparisonTracker rct = (RatiosComparisonTracker) compTracker;
    return rct.getConfidenceIntervals();
  }
}
