//******************************************************************************
// ScalarCompTracker.java
//******************************************************************************
package edu.utah.camplab.genie.stat;

//==============================================================================
class ScalarCompTracker implements CCStat.ComparisonTracker {

  public int notval;
  //public  CCStat.Result confIntervals = null;
  public String messages = null;
  public ResultManager resultManager = new ResultManager();
  protected double r0Value;
  protected int comparisonCount;
  // change gteCount to hitCount, same as all comptracker
  //protected int    gteCount;
  protected int hitCount;

  //----------------------------------------------------------------------------
  ScalarCompTracker() {
  }

  //----------------------------------------------------------------------------
  public void setStartingResult(CCStat.Result r0) {
    assert r0.elementCount() == 1;
    r0Value = r0.doubleValues()[0];
    comparisonCount = 0;
    hitCount = 0;
    notval = 0;
    //comparisonCount[0] = gteCount = notval[0] = 0;
  }

  //----------------------------------------------------------------------------
  public void compareResultAtX(CCStat.Result rx) {
    assert rx.elementCount() == 1;
    //if ( rx instanceof ResultImp.StringResult )
    double rxValue = (rx.doubleValues())[0];
    if (!Double.isNaN(r0Value) && !Double.isInfinite(r0Value)) {
      if (Double.isNaN(rxValue) ||
        (rx instanceof ResultManager.StringResult)) {
        notval++;
      }
      else {
        comparisonCount++;
        //System.out.println("rx :" + rxValue + " r0 : " + r0Value);
        if (Math.abs(rxValue) >= Math.abs(r0Value))
        //++gteCount;
        {
          ++hitCount;
        }
      }
    }
  }

  //----------------------------------------------------------------------------
  public CCStat.Result getComparisonsResult() {
    /*
    if ( Double.isNaN(r0Val) )
      return new ResultImp.StringResult("-");
    else
      return new ResultImp.Real(gteCount / (double) comparisonCount[0] );
    */
    ResultManager resultManager = new ResultManager();
    if (Double.isNaN(r0Value)) {
      return resultManager.new StringResult("-");
    }
    else {
      return resultManager.new Real(hitCount / (double) comparisonCount,
        (hitCount + "/" + comparisonCount));
    }
  }

  //----------------------------------------------------------------------------
  public CCStat.Result getnotval() {
    return resultManager.new Real(notval);
  }

  //----------------------------------------------------------------------------
  public String getMessages() {
    return messages;
  }

  //----------------------------------------------------------------------------
  public void setMessages(String inMessages) {
    messages = inMessages;
  }

  //----------------------------------------------------------------------------
  public void setDegreeOfFreedom(int df) {
  }

  //----------------------------------------------------------------------------
  public CCStat.Result getComparisonCount() {
    return resultManager.new Real(comparisonCount);
  }

  //----------------------------------------------------------------------------
  public CCStat.Result gethitCount() {
    return resultManager.new Real(hitCount);
  }
}
