//*****************************************************************************
// MetaOddsRatios.java
//*****************************************************************************
package edu.utah.camplab.genie.stat;

import edu.utah.camplab.genie.gm.Ptype;

//=============================================================================
public class MetaOddsRatios extends CCStatImp {
  Number[][] caseCell;
  Number[][] controlCell;
  Number[][] ctotal;
  int refColIndex;

  //---------------------------------------------------------------------------
  public MetaOddsRatios() {
    title = "Meta Odds Ratios, 2-Tailed Test";
    //infExtraStatTitle = "Empirical Confidence Intervals : ";
  }

  //---------------------------------------------------------------------------
  public ComparisonTracker newComparisonTracker() {
    //return new ORComparisonTracker();
    compTracker = new RatiosComparisonTracker(true, false, 1.0);
    return compTracker;
  }

  public ComparisonTracker newComparisonTracker(int df) {
    return this.newComparisonTracker();
  }

  //---------------------------------------------------------------------------
  public boolean getdata(CCAnalysis a,
                         CCAnalysis.Table[] t) {
    boolean returnValue = true;
    CCAnalysis.Table[] metaTable;
    metaTable = getNonZeroTables(a, t, compTracker);
    if (metaTable == null || metaTable.length == 0) {
      return false;
    }
    //resultAt0 = new ResultImp.StringResult
    //("WARNING: no valid tables for this analysis");

    int mt = metaTable.length;
    refColIndex = a.getReferenceColumnIndex();
    caseCell = new Number[mt][];
    controlCell = new Number[mt][];
    ctotal = new Number[mt][];
    for (int i = 0; i < mt; i++) {
      ContingencyTable table = (ContingencyTable) metaTable[i];
      CCAnalysis.Table.Totals totals = table.getTotals();
      caseCell[i] = table.getRowFor(Ptype.CASE).getCells();
      controlCell[i] = table.getRowFor(Ptype.CONTROL).getCells();
      ctotal[i] = totals.forColumns();
      for (int j = 0; j < ctotal[i].length; j++) {
        if (ctotal[i][j].intValue() == 0) {
          break;
        }
      }
    }
    return returnValue;
  }

  //---------------------------------------------------------------------------
  public Result computation(CCAnalysis a,
                            CCAnalysis.Table[] t,
                            boolean allowMessages) {
    Result res = null;
    String msg = "";
    boolean hasError = false;
    if (a.checkDistinctRefWt()) {
      if (getdata(a, t)) {
        res = StatUt.metaOddsRatios(caseCell,
          controlCell,
          ctotal,
          refColIndex);
      }
      else {
        msg = getMessage();
        hasError = true;
      }
    }
    else {
      msg = "WARNING: Analysis Table has more than one reference column, **test** has not been performed.";
      hasError = true;
    }
    if (hasError) {
      res = resultManager.new Real(Double.NaN);
      if (allowMessages) {
        res.setMessages(msg);
      }
    }
    return res;
  }

  //----------------------------------------------------------------------------
  public Result getObservedResult(Result obsResult,
                                  int refColIndex) {
    return obsResult;
    /*
    if ( obsResult instanceof ResultImp.StringResult )
      return obsResult;
    else
    {
      double[] r0Result = obsResult.doubleValues();
      int numCol = r0Result.length + 1;
      return ResultImp.convertTriplet( obsResult, numCol, refColIndex );
    }
    */
  }

  //----------------------------------------------------------------------------
  public Result getInferentialResult(ComparisonTracker compTracker,
                                     int refColIndex) {
    //ORComparisonTracker ORct = (ORComparisonTracker) compTracker;
    RatiosComparisonTracker rct = (RatiosComparisonTracker) compTracker;
    return rct.getComparisonsResult();
    //double[] rr = (ORct.getComparisonsResult()).doubleValues();
    //int numCol = rr.length + 1;
    //return ResultImp.convertTriplet(ORct.getComparisonsResult(),
    //                                numCol,
    //                                refColIndex );
  }

  //----------------------------------------------------------------------------
  //public Result getInfExtraStat(ComparisonTracker compTracker)
  //{
  //  ORCompTrackerExt ORct = (ORCompTrackerExt) compTracker;
  //  return ORct.getConfidenceIntervals();
  //}

  //----------------------------------------------------------------------------
  public CCAnalysis.Table getTable(TableMaker tm) {
    return tm.getContingencyTable();
  }

  //---------------------------------------------------------------------------
  public CCAnalysis.Table getTable(TableMaker tm, Thread p) {
    return tm.getContingencyTable(p);
  }
}
