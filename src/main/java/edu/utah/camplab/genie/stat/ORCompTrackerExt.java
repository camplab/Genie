//******************************************************************************
// ORCompTrackerExt.java
// ---- this object is obsolete, replaced by RatiosComparisonTracker ----------
//******************************************************************************
package edu.utah.camplab.genie.stat;

import java.util.Iterator;
import java.util.TreeMap;

//==============================================================================
class ORCompTrackerExt extends ORComparisonTracker {

  //----------------------------------------------------------------------------
  public void compareResultAtX(CCStat.Result rx) {
    System.out.println("using  wrong comp tracker ext ");
    if (r0Map != null) {
      if (rx == null || (rx instanceof ResultManager.Real)) {
        for (Iterator<String> it = r0Map.keySet().iterator(); it.hasNext(); ) {
          String key = it.next();
          incrementMap(notMap, key);
        }
      }
      else {
        TreeMap<String, ResultManager.Real> rxMap =
          ((ResultManager.Series) rx).resultMap();
        for (Iterator<String> it = rxMap.keySet().iterator(); it.hasNext(); ) {
          String key = it.next();
          double rxValue = rxMap.get(key).doubleValues()[0];
          double r0Value = r0Map.get(key).doubleValues()[0];
          if (Double.isNaN(rxValue)) {
            incrementMap(notMap, key);
          }
          else {
            if (Math.abs(rxValue) >= Math.abs(r0Value)) {
              incrementMap(hitMap, key);
            }
            incrementMap(comparisonMap, key);
          }
        }
      }
    }
  }

  //----------------------------------------------------------------------------
  public CCStat.Result getComparisonsResult() {
    TreeMap<String, ResultManager.Real> resultMap =
      new TreeMap<String, ResultManager.Real>();
    if (r0Map != null) {
      for (Iterator<String> it = r0Map.keySet().iterator(); it.hasNext(); ) {
        String key = it.next();
        ResultManager.Real value = r0Map.get(key);
        double r0Value = r0Map.get(key).doubleValues()[0];
        double pval = 0.0;
        int hit = 0;
        int comparison = 0;
        if (hitMap.get(key) != null) {
          hit = hitMap.get(key).intValue();
        }
        if (comparisonMap.get(key) != null) {
          comparison = comparisonMap.get(key).intValue();
        }
        if (Double.isNaN(r0Value)) {
          pval = r0Value;
        }
        else {
          if (hit * 2 < comparison) {
            hit = hit * 2;
          }
          else {
            hit = comparison;
          }
          pval = hit / (double) comparison;
        }
        resultMap.put(key,
          resultManager.new Real(pval,
            new String(hit + "/" + comparison)));
      }
    }
    else {
      return resultMap.put("-", resultManager.new Real(Double.NaN));
    }
    return resultManager.new Series(resultMap);
  }
}
