//*****************************************************************************
// TrioTDT.java
//*****************************************************************************
package edu.utah.camplab.genie.stat;

//=============================================================================
public class TrioTDT extends CCStatImp {

  //---------------------------------------------------------------------------
  public TrioTDT() {
    title = "Case Parents Trio TDT (Chi-Squared Distribution)";
  }

  //---------------------------------------------------------------------------
  public ComparisonTracker newComparisonTracker() {
    return new ScalarCompTracker();
  }

  public ComparisonTracker newComparisonTracker(int df) {
    return this.newComparisonTracker();
  }

  //---------------------------------------------------------------------------
  public Result computation(CCAnalysis a,
                            CCAnalysis.Table t,
                            boolean allowMessages) {
    if (!a.getType().matches("allele")) {
      if (t != null && t.getColumnCount() == 3) {
        TrioTDTTable tt = (TrioTDTTable) t;
        //resultAt0 = new ResultImp.Real(
        res = StatUt.trioTDT(tt.getCellb(), tt.getCellc());
      }
      else {
        res = resultManager.new Real(Double.NaN);
        if (allowMessages) {
          res.setMessages
            ("WARNING : Table does not contain 3 columns; ***test*** has not been performed");
        }
      }
    }
    else {
      res = resultManager.new Real(Double.NaN);
      if (allowMessages) {
        res.setMessages
          ("WARNING : Unable to apply test to table type = " + a.getType());
      }
    }
    return res;
  }

  //---------------------------------------------------------------------------
  public CCAnalysis.Table getTable(TableMaker tm) {
    return tm.getTrioTDTTable();
  }

  //---------------------------------------------------------------------------
  public CCAnalysis.Table getTable(TableMaker tm, Thread p) {
    return tm.getTrioTDTTable(p);
  }
}
