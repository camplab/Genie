//*****************************************************************************
// OddsRatios.java
//*****************************************************************************
package edu.utah.camplab.genie.stat;

import edu.utah.camplab.genie.gm.Ptype;

import java.util.Vector;

//=============================================================================
public class OddsRatios extends CCStatImp {

  //---------------------------------------------------------------------------
  public OddsRatios() {
    title = "Odds Ratios, 2-Tailed Test";
    //infExtraStatTitle = "Empirical Confidence Intervals : ";
  }

  //---------------------------------------------------------------------------
  public ComparisonTracker newComparisonTracker() {
    //return new ORComparisonTracker();
    return new RatiosComparisonTracker(true, false, 1.0);
  }

  public ComparisonTracker newComparisonTracker(int df) {
    return this.newComparisonTracker();
  }

  //---------------------------------------------------------------------------
  public Result computation(CCAnalysis a,
                            CCAnalysis.Table t,
                            boolean allowMessages) {
    Result res = null;
    if (a.checkDistinctRefWt()) {
      if (StatUt.checkRefCell(t.getRowFor(Ptype.CASE).getCells(),
        t.getRowFor(Ptype.CONTROL).getCells(),
        a.getReferenceColumnIndex())) {
        res = StatUt.oddsRatios(
          t.getRowFor(Ptype.CASE).getCells(),
          t.getRowFor(Ptype.CONTROL).getCells(),
          a.getReferenceColumnIndex());
      }
      else {
        res = resultManager.new Real(Double.NaN);
        if (allowMessages) {
          res.setMessages
            ("WARNING: reference cell has 0 value, **test** has not been performed.");
        }
      }
    }
    else {
      res = resultManager.new Real(Double.NaN);
      if (allowMessages) {
        res.setMessages
          ("WARNING: Analysis Table has more than one reference column, **test** has not been performed.");
      }
    }
    return res;
  }

  //----------------------------------------------------------------------------
  //public Result getObservedResult(Result obsResult,
  public synchronized Result getObservedResult(Result obsResult,
                                               int refColIndex) {
    /*String[][] resultSeries = new String[numCol - 1][3];
    double[] r0Vals = observedResult.doubleValues();
    for ( int j = 0, r = 0; j < numCol; j++ )
    {
      if ( j != referenceColIndex && r < r0Vals.length )
      {
        resultSeries[r][0] = Integer.toString(j+1);
        resultSeries[r][1] = Integer.toString(referenceColIndex + 1);
        resultSeries[r][2] = Double.toString(r0Vals[r]);
        r++;
      }
    }
    return new ResultImp.TripletSeries ( resultSeries );
    */
    /* just need to obsResult.toString()
    if ( obsResult instanceof ResultManager.StringResult )
      return obsResult;
    else
    {
      double[] r0Result = obsResult.doubleValues();
      int numCol = r0Result.length + 1;
      return ResultImp.convertTriplet( obsResult, numCol, refColIndex );
    }
    */
    return obsResult;
  }

  //----------------------------------------------------------------------------
  public Result getInferentialResult(ComparisonTracker compTracker,
                                     int refColIndex) {
    //ORComparisonTracker ORct = (ORComparisonTracker) compTracker;
    RatiosComparisonTracker rct = (RatiosComparisonTracker) compTracker;
    //double[] rr = (ORct.getComparisonsResult()).doubleValues();
    //int numCol = rr.length + 1;
    //return ResultImp.convertTriplet(ORct.getComparisonsResult(),
    //                                numCol,
    //                                refColIndex );
    return rct.getComparisonsResult();
  }

  //----------------------------------------------------------------------------
  //public Result getInfExtraStat(ComparisonTracker compTracker)
  //{
  //ORCompTrackerExt ORct = (ORCompTrackerExt) compTracker;
  //return ORct.getConfidenceIntervals();
  //}

  //----------------------------------------------------------------------------
  public Vector<Vector<Double>> getInfSimStat(ComparisonTracker compTracker) {
    return null;
    //ORCompTrackerExt ORct = (ORCompTrackerExt) compTracker;
    //return ORct.getSimulatedResults();
  }

  //----------------------------------------------------------------------------
  public CCAnalysis.Table getTable(TableMaker tm) {
    return tm.getContingencyTable();
  }

  //----------------------------------------------------------------------------
  //Ryan 06-24-07 overloaded to pass thread
  public CCAnalysis.Table getTable(TableMaker tm, Thread p) {
    return tm.getContingencyTable(p);
  }
}
