//******************************************************************************
// WeightedCompTracker.java
//******************************************************************************
package edu.utah.camplab.genie.stat;

import umontreal.iro.lecuyer.probdist.ChiSquareDist;

//==============================================================================
class WeightedCompTracker extends ScalarCompTracker {
  private ChiSquareDist csd;
  private int degreeF;

  //----------------------------------------------------------------------------
  WeightedCompTracker(int df) {
    super();
    degreeF = df;
    //System.out.println("created weightedCompTracker with : " + df + " freedom");
  }

  //----------------------------------------------------------------------------
  public CCStat.Result getComparisonsResult() {
    CCStat.Result res = null;
    if (Double.isNaN(r0Value))
    //return new ResultImp.StringResult("-");
    {
      res = resultManager.new Real(Double.NaN);
    }
    else {
      csd = new ChiSquareDist(degreeF);
      double csValue = csd.barF(r0Value);
      //System.out.println("df : "+  degreeF + " obs " +  r0Val + " result : " + csValue);
      //return new ResultImp.Real(csValue);
      res = resultManager.new Real(csValue);
    }
    return res;
  }

  //----------------------------------------------------------------------------
  //public void setDegreeOfFreedom(int df)
  //{ 
  //  degreeF = df;
  //}

  //----------------------------------------------------------------------------
  public int getDegreeOfFreedom() {
    return degreeF;
  }
}
