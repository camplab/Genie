//*****************************************************************************
// QuantComparisonTracker.java
// ------- this object is obsolete, replaced by RatiosComparisonTracker -------
//*****************************************************************************
package edu.utah.camplab.genie.stat;

import java.util.Iterator;
import java.util.TreeMap;

//=============================================================================
public class QuantComparisonTracker implements CCStat.ComparisonTracker {
  //public int[]    notval;
  //private int[]   hitCounts;
  //public int[]    comparisonCount;
  //public double[] r0Vals;
  public TreeMap<String, ResultManager.Real> r0Map;
  public TreeMap<String, ResultManager.Real> hitMap;
  public TreeMap<String, ResultManager.Real> notMap;
  public TreeMap<String, ResultManager.Real> comparisonMap;
  public String messages = null;
  public int degreeF;
  public ResultManager resultManager = new ResultManager();

  //-----------------------------------------------------------------------
  public void setStartingResult(CCStat.Result r0) {
    /*
    if ( !(r0 instanceof ResultImp.StringResult))
        {
          assert r0 instanceof CCStat.Result;
          int nResult = r0.elementCount();
          r0Vals = r0.doubleValues();
          hitCounts = new int[nResult];
          comparisonCount = new int[nResult];
          notval = new int[nResult];
          for ( int i =0; i < r0Vals.length; i++ )
            comparisonCount[i] = notval[i] = 0;
        }
    */
    if (r0 instanceof ResultManager.Series) {
      r0Map = ((ResultManager.Series) r0).resultMap();
      hitMap = new TreeMap<String, ResultManager.Real>();
      notMap = new TreeMap<String, ResultManager.Real>();
      comparisonMap = new TreeMap<String, ResultManager.Real>();
    }
  }

  //-----------------------------------------------------------------------
  public void compareResultAtX(CCStat.Result rx) {
    /*
    double[] rxVals = rx.doubleValues();
    for ( int i = 0; i < rxVals.length; i++ ) {
      if ( Double.isNaN(rxVals[i]) || Double.isInfinite(rxVals[i]) )
        notval[i]++;
      else {
        if ( r0Vals.length > 2 && i == ( rxVals.length - 1) ) {
          if ( rxVals[i] > r0Vals[i] )
            hitCounts[i]++;
        } else {
          if ( r0Vals[i] > 0 ) {
            if ( rxVals[i] > r0Vals[i] ||
               rxVals[i] < ( r0Vals[i] * -1 ) )
               hitCounts[i]++;
          } else if ( r0Vals[i] < 0 ) {
            if ( rxVals[i] < r0Vals[i] ||
                 rxVals[i] > ( r0Vals[i] * -1 ) )
              hitCounts[i]++;
          }
        }
        comparisonCount[i]++;
      }
    }
    */
    if (r0Map != null) {
      if (rx == null || (rx instanceof ResultManager.Real)) {
        for (Iterator<String> it = r0Map.keySet().iterator(); it.hasNext(); ) {
          String key = it.next();
          incrementMap(notMap, key);
        }
      }
      else {
        TreeMap<String, ResultManager.Real> rxMap =
          ((ResultManager.Series) rx).resultMap();
        for (Iterator<String> it = rxMap.keySet().iterator(); it.hasNext(); ) {
          String key = it.next();
          double rxValue = rxMap.get(key).doubleValues()[0];
          double r0Value = r0Map.get(key).doubleValues()[0];
          if (Double.isNaN(rxValue)) {
            incrementMap(notMap, key);
          }
          else {
            if (key.equals("overall")) {
              if (rxValue > r0Value) {
                incrementMap(hitMap, key);
              }
            }
            else {
              //if ( r0Value > 0 ) {
              // mod 11-9-09 follow the same rule as odds ratios and 2 * hit
              //if ( rxValue > r0Value || rxValue < ( r0Value * -1 ) )
              //incrementMap(hitMap, key);
              //} else if ( r0Value < 0 ) {
              //if ( rxValue < r0Value || rxValue > ( r0Value * -1 ) ) 
              //incrementMap(hitMap, key);
              if (Math.abs(rxValue) > Math.abs(r0Value)) {
                incrementMap(hitMap, key);
              }
            }
            incrementMap(comparisonMap, key);
          }
        }
      }
    }
  }

  //--------------------------------------------------------------------------
  public CCStat.Result getComparisonsResult() {
    //int resultsize = 1;
    //if ( hitCounts.length > 2 )
    //  resultsize = hitCounts.length - 1;
    /*
    double[] pvals = new double[hitCounts.length];
    for ( int i = 0; i < hitCounts.length; i++ ) {
      if ( Double.isNaN(r0Vals[i]))
        pvals[i] = r0Vals[i];
      else if ( hitCounts.length > 2 && i == hitCounts.length - 1)
        pvals[i] = hitCounts[hitCounts.length - 1] /
                          (double) comparisonCount[i] ;
      else
        pvals[i] = hitCounts[i] / (double) comparisonCount[i];
      }
      return new ResultImp.RealSeries(pvals);
    */
    TreeMap<String, ResultManager.Real> resultMap =
      new TreeMap<String, ResultManager.Real>();
    if (r0Map != null) {
      for (Iterator<String> it = r0Map.keySet().iterator(); it.hasNext(); ) {
        double pval = 0.0;
        String key = it.next();
        ResultManager.Real value = r0Map.get(key);
        double r0Value = value.doubleValues()[0];
        int hit = 0;
        int comparison = 0;
        if (hitMap.get(key) != null) {
          hit = hitMap.get(key).intValue();
        }
        if (comparisonMap.get(key) != null) {
          comparison = comparisonMap.get(key).intValue();
        }
        if (Double.isNaN(r0Value)) {
          pval = r0Value;
        }
        else {
          if (!key.equals("overall")) {
            if (hit * 2 < comparison) {
              hit = hit * 2;
            }
            else {
              hit = comparison;
            }
          }
          pval = hit / (double) comparison;
        }
        resultMap.put(key,
          resultManager.new Real(pval,
            new String(hit + "/" + comparison)));
      }
    }
    else {
      resultMap.put("-", resultManager.new Real(Double.NaN));
    }
    return resultManager.new Series(resultMap);
  }

  //------------------------------------------------------------------------
  //public int[] getnotval() {
  //{ return notval; }
  public CCStat.Result getnotval() {
    return resultManager.new Series(notMap);
  }

  //------------------------------------------------------------------------
  /*
  public int[] getComparisonCount()
  {
    if ( comparisonCount.length > 2 )
    {
      int[] trueComparisonCount = new int[comparisonCount.length - 1];
      for ( int i = 0; i < comparisonCount.length - 1; i++ )
        trueComparisonCount[i] = comparisonCount[i];
      return trueComparisonCount; 
    }
    else 
      return comparisonCount;
  } 
  */
  public CCStat.Result getComparisonCount() {
    return resultManager.new Series(comparisonMap);
  }

  //------------------------------------------------------------------------
  public CCStat.Result gethitCount() {
    return resultManager.new Series(hitMap);
  }

  //------------------------------------------------------------------------
  public String getMessages() {
    return messages;
  }

  //------------------------------------------------------------------------
  public void setMessages(String inMessages) {
    messages = inMessages;
  }

  //------------------------------------------------------------------------
  public void setDegreeOfFreedom(int df) {
    degreeF = df;
  }

  //------------------------------------------------------------------------
  public void incrementMap(TreeMap<String, ResultManager.Real> inMap,
                           String key) {
    int count = 0;
    if (inMap.get(key) != null) {
      count = inMap.get(key).intValue();
    }
    count++;
    inMap.remove(key);
    inMap.put(key, resultManager.new Real(count));
  }
}
