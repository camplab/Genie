//******************************************************************************
// LinkageDataSetExt.java
//******************************************************************************
package edu.utah.camplab.genie.gchapext;

import alun.genio.LinkageParameterData;
import alun.genio.LinkagePedigreeData;
import edu.utah.camplab.genie.ped.Indiv;

import java.io.IOException;

//==============================================================================

/**
 * This class holds the information from a standard Linkage parameter data file
 * and a standard Linkage pedigree file.
 */
public class LinkageDataSetExt extends alun.genio.LinkageDataSet {
  public LinkageDataSetExt(LinkageParameterData lpd,
                           Indiv[] indiv,
                           Indiv.GtSelector selector)
    throws IOException {
    super();
    LinkagePedigreeData ldd = new LinkagePedDataExt(lpd, indiv, selector);
    set(lpd, ldd);
  }
}
