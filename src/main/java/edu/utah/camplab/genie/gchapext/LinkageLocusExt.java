package edu.utah.camplab.genie.gchapext;

import alun.genio.LinkagePhenotype;
import alun.genio.NumberedAlleleLocus;
import alun.genio.NumberedAllelePhenotype;

public class LinkageLocusExt extends NumberedAlleleLocus {
  public LinkageLocusExt() {
    super();
  }

  public LinkagePhenotype readPhenotype(int a, int b) {
    return new NumberedAllelePhenotype(this, a, b);
  }
}
