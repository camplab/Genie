//******************************************************************************
// MainManager.java
//******************************************************************************
package edu.utah.camplab.genie.app.rgen;

import edu.utah.camplab.genie.gui2.GenieGUI;
import edu.utah.camplab.genie.util.GEException;
import edu.utah.camplab.genie.util.Ut;

import java.util.Calendar;

//==============================================================================
public class MainManager {

  public static final String application_version = "3.0.0.1";
  private static final boolean DBUG = true;
  ;
  private static String application_id;
  public CommandLine cl;
  private Thread thread = null;
  private IOManager iom = null;
  private PedGenie pd = null;

  //----------------------------------------------------------------------------
  public MainManager(String[] args) {
    final String[] str = args;
    thread = new Thread() {
      public void run() {
        while (true) {
          if (str.length == 0) {
            GenieGUI.main(str);
          }
          else {
            executeGenie(str);
          }
          try {
            Thread.currentThread().sleep(1);
          } catch (InterruptedException ie) {
            break;
          }
          System.out.println("Analyses Completed Successfully");
          break;
        }
      }
    };
    thread.start();
  }

  //----------------------------------------------------------------------------
  public void executeGenie(String[] args) {
    application_id = args[0];
    System.out.println(Ut.N + "***** " + application_id + " " +
      application_version + " *****" + Ut.N);

    Calendar creationDate = Calendar.getInstance();
    CommandLine cl = new CommandLine();
    if (!cl.parse(args)) {
      cl.exitWithUsage();
    }
    try {
      iom = new IOManager(application_id, application_version, cl.getSpecFile(), cl.getProperties(), creationDate);
      //new PedGenie(iom.getSpecification(), iom.getPedData()).run(iom);
      pd = new PedGenie(iom, application_id);
      int[] params = {-1, 1};
      params = pd.run(params);

      while (params[0] > -2) {
        params = pd.run(params);
        //System.out.println("Run params : " + params[0] + " " + params[1]);
      }
    } catch (Exception e) {
      if (DBUG || !(e instanceof GEException)) {
        System.err.println(Ut.N + "<< " + application_id +
          application_version + " aborted. >>");
        e.printStackTrace();
      }
      else {
        System.err.println("Error: " + e.getMessage());
      }
    }
  }

  //---------------------------------------------------------------------------
  public void stopThread() {
    if (iom != null && !iom.reportCompleted()) {
      try {
        iom.writeReport(pd.getCycleIndex());
      } catch (Exception ee) {
        if (!(ee instanceof GEException)) {
          ee.printStackTrace();
        }
        else {
          System.err.println("Error: " + ee.getMessage());
        }
      }
    }
    thread.interrupt();
  }
}

