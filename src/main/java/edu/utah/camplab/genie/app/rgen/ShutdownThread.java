//******************************************************************************
// ShutdownThread is the thread we pass to the addShutdownHook method
//******************************************************************************
package edu.utah.camplab.genie.app.rgen;

class ShutdownThread extends Thread {
  private MainManager mainManager = null;

  public ShutdownThread(MainManager mainmanager) {
    super();
    mainManager = mainmanager;
  }

  public void run() {
    mainManager.stopThread();
  }
}
