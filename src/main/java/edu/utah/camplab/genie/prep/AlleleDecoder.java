// AlleleDecoder.java
//*****************************************************************************
package edu.utah.camplab.genie.prep;

import edu.utah.camplab.genie.gm.*;
import edu.utah.camplab.genie.ped.Indiv;
import edu.utah.camplab.genie.ped.PedData;
import edu.utah.camplab.genie.ped.PedQuery;
import edu.utah.camplab.genie.ped.Study;
import edu.utah.camplab.genie.util.GEException;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.TreeMap;
import java.util.TreeSet;

//=============================================================================
//decoderMap - key is the code of 1 or 2 , value is the actual allele value  
public class AlleleDecoder {

  //public static TreeMap<Byte, Byte> decoderMap = new TreeMap<Byte, Byte>();
  public static AlleleFormat alleleformat;

  //----------------------------------------------------------------------------
  public static void parse(GDef gdef, File inFile)
    throws IOException, GEException {
    int nLocus = gdef.getLocusCount();
    alleleformat = gdef.getAlleleFormat();
    String format = alleleformat.getFormat();
    int column;
    if (format.startsWith("num")) {
      column = 3;
    }
    else {
      column = 7;
    }
    // read in the file 
    TreeMap<String, TreeSet<FreqDataSet>> tempMap =
      new TreeMap<String, TreeSet<FreqDataSet>>();
    BufferedReader in = new BufferedReader(new FileReader(inFile));
    String line;
    TreeSet<FreqDataSet> valueSet = null;
    while ((line = in.readLine()) != null) {
      String[] tokens = line.trim().split("\\s+");
      double freq = Double.valueOf(tokens[6]).doubleValue();
      byte allele = alleleformat.convertAllele(tokens[column]);
      FreqDataSet fds = new FreqDataSet(freq, allele, "alleledecoder");
      valueSet = tempMap.get(tokens[1]);
      if (valueSet != null) {
        if (!valueSet.contains(fds)) {
          valueSet.add(fds);
        }
      }
      else {
        valueSet = new TreeSet<FreqDataSet>();
        valueSet.add(fds);
      }
      tempMap.remove(tokens[1]);
      tempMap.put(tokens[1], valueSet);
    }

    for (int i = 0; i < nLocus; i++) {
      GDef.Locus locus = gdef.getLocus(i);
      String marker = locus.getMarker();
      for (Iterator it = tempMap.keySet().iterator(); it.hasNext(); ) {
        String key = (String) it.next();
        if (key.equals(marker)) {
          //System.out.println("Matched marker : " + marker + " with tempMap : " + key);
          TreeSet<FreqDataSet> value = tempMap.get(key);
          buildDecoderMap(value, locus);
          break;
        }
      }
    }
  }

  //----------------------------------------------------------------------------
  public static void buildDecoderMap(TreeSet<FreqDataSet> fds, GDef.Locus locus) {
    //System.out.println("build DecoderMap for Locus : " + locus.getMarker());
    TreeMap<Byte, FreqDataSet> decoderMap = new TreeMap<Byte, FreqDataSet>();
    int n = 1;
    for (Iterator it = fds.iterator(); it.hasNext(); ) {
      FreqDataSet key = (FreqDataSet) it.next();
      //byte[] allelecode = key.getCode();
      //Byte bb = Byte.valueOf(allelecode[0]);
      decoderMap.put(Byte.valueOf(String.valueOf(n)),
        key);
      //Byte.valueOf(allelecode[0]));
      //System.out.println("locus : "+ locus.getMarker() + " pattern " + n + " shoudld convert to " + new String(allelecode) + " in Byte : " + bb.toString());
      n++;
    }
    locus.addDecoderMap(decoderMap);
    // for testing
    /*
    TreeMap<Byte, Byte> dmap = locus.getDecoderMap();
    System.out.println("   Print Locus Decoder Map " );
    for ( Iterator it = dmap.keySet().iterator(); it.hasNext(); ) {
      Byte key = (Byte) it.next();
      Byte value = dmap.get(key);
      System.out.println(" allele : " + key.toString() + " map to " + value.toString());
    }
    */
  }

  //----------------------------------------------------------------------------
  public static void build(GDef gdef, Study[] inStudy) {
    alleleformat = gdef.getAlleleFormat();
    byte missingData = alleleformat.getMissingData();
    //System.out.println("Missing Data " + new Byte(missingData));
    int nstudy = inStudy.length;
    int nLocus = gdef.getLocusCount();
    int[] locusTotal = new int[nLocus];
    GDef.Locus[] locus;
    TreeMap<Byte, Integer>[] locusMap = (TreeMap<Byte, Integer>[])
      new TreeMap[nLocus];
    for (int i = 0; i < nLocus; i++) {
      locusTotal[i] = 0;
    }

    for (Study study : inStudy) {
      PedData pd = study.getPedData();
      // sample should be from control only 
      Indiv[] sampleInds = pd.getIndividuals(PedQuery.IS_CONTROL);
      int nSampleInds = sampleInds.length;
      for (int iind = 0; iind < nSampleInds; ++iind) {
        //System.out.println("ind : " + sampleInds[iind].getID() );
        Gtype gt = sampleInds[iind].getGtype(Indiv.GtSelector.OBS);
        if (gt != null) {
          for (int i = 0; i < nLocus; i++) {
            AllelePair pair = gt.getAllelePairAt(i);
            if (pair != null) {
              byte allele1 = pair.getAlleleCode(true);
              byte allele2 = pair.getAlleleCode(false);
              if (allele1 != missingData || allele2 != missingData) {
                Byte a1 = new Byte(allele1);
                Byte a2 = new Byte(allele2);
                //System.out.println(" allele 1 " + allele1 + " allele2 " + allele2);
                if (locusMap[i] == null) {
                  locusMap[i] = new TreeMap<Byte, Integer>();
                  locusMap[i].put(a1, 1);
                  locusMap[i].put(a2, 1);
                }
                else {
                  if (locusMap[i].get(a1) == null) {
                    locusMap[i].put(a1, 1);
                  }
                  else {
                    Integer value = locusMap[i].get(a1);
                    locusMap[i].remove(a1);
                    locusMap[i].put(a1, ++value);
                  }
                  if (locusMap[i].get(a2) == null) {
                    locusMap[i].put(a2, 1);
                  }
                  else {
                    Integer value = locusMap[i].get(a2);
                    locusMap[i].remove(a2);
                    locusMap[i].put(a2, ++value);
                  }
                }
                locusTotal[i] += 2;
              }
            }
          }
        }
      }
    }
    //ArrayList<String[]> tempArray = new ArrayList<String[]>();
    for (int i = 0; i < locusMap.length; i++) {
      TreeSet<FreqDataSet> allelefreqset = new TreeSet<FreqDataSet>();
      for (Iterator it = locusMap[i].keySet().iterator(); it.hasNext(); ) {
        Byte key = (Byte) it.next();
        int value = locusMap[i].get(key).intValue();
        FreqDataSet fds = new FreqDataSet(
          (double) value / locusTotal[i],
          key.byteValue(), "AlleleDecoder");
        allelefreqset.add(fds);
      }
      buildDecoderMap(allelefreqset, gdef.getLocus(i));
    }
  }
}
