//******************************************************************************
// LpedParser.java
//******************************************************************************
package edu.utah.camplab.genie.io;

import edu.utah.camplab.genie.gm.AlleleFormat;
import edu.utah.camplab.genie.gm.GDef;
import edu.utah.camplab.genie.gm.GtypeBuilder;
import edu.utah.camplab.genie.gm.Qdata;
import edu.utah.camplab.genie.ped.PedData;
import edu.utah.camplab.genie.ped.PedDocument;
import edu.utah.camplab.genie.ped.QuantIndiv;
import edu.utah.camplab.genie.util.GEException;

import java.io.*;

//==============================================================================
public class LpedParser implements PedDocument.FileParser {

  int nmarkers;
  int[] marcols;
  int nSim;

  //----------------------------------------------------------------------------
  public void parse
  (File fsource, QuantIndiv[] qInd, int[] quant_ids,
   GDef gdef, int numSimData, PedData.Loader loader,
   boolean hasHeader)
    throws GEException {
    BufferedReader in = null;
    try {
      in = new BufferedReader(new FileReader(fsource));
    }
    catch (FileNotFoundException fnf) {
      throw new GEException(fnf.getMessage());
    }
    int nmarkers = gdef.getLocusCount();
    int[] marcols = new int[nmarkers];
    int nSim = numSimData;
    Qdata quants_val = null;
    String line, ped_id, ind_id;
    GtypeBuilder gt_builder = gdef.getGtypeBuilder();
    AlleleFormat af = gdef.getAlleleFormat();
    GDef.Locus[] locus = new GDef.Locus[nmarkers];

    for (int i = 0; i < nmarkers; ++i) {
      locus[i] = gdef.getLocus(i);
      marcols[i] = Col.MARKER1 + (locus[i].getID() - 1) * 2;
      //marcols[i] = Col.MARKER1 + (gdef.getLocus(i).getID() - 1) * 2;
    }
    // skip possible header line in non-standard files (e.g., GE labouts)
    //if (in.readLine().matches("^\\s*\\d+\\s+\\d+.+$"))
    //if (in.readLine().trim().startsWith("\\d"))
    int linecount = 1;
    try {
      if (hasHeader) {
        line = in.readLine();
        String[] s = line.trim().split("\\s+");
        for (int i = 0; i < nmarkers; i++) {
          //GDef.Locus locus = gdef.getLocus(i);
          if (locus[i].getMarker() == null) {
            locus[i].setMarker(s[Col.MARKER1 + locus[i].getID() - 1]);
          }
        }
        //System.out.println("has header");
      }
      else {
        System.out.println("no header");
      }


      while ((line = in.readLine()) != null) {
        String[] tokens = line.trim().split("\\s+");
        //System.out.println("line count : " + linecount  + " token.length : " + tokens.length);
        for (int i = 0; i < nmarkers; ++i) {
          byte allele1, allele2;
          int col1 = marcols[i];
          //GDef.Locus locus = gdef.getLocus(i);
          //System.out.println("token at : " + col1);

          allele1 = af.convertAllele(tokens[col1]);
          allele2 = af.convertAllele(tokens[col1 + 1]);
          gt_builder.addAllelePair(allele1, allele2);
        }
        ped_id = tokens[Col.PED_ID];
        ind_id = tokens[Col.IND_ID];

        if (qInd != null) {
          double[] qt = new double[quant_ids.length];
          for (int i = 0; i < quant_ids.length; i++) {
            qt[i] = 0.0;
          }
          quants_val = new Qdata(qt);

          for (int i = 0; i < qInd.length; i++) {
            if (qInd[i].ped_id.equals(ped_id) && qInd[i].ind.equals(ind_id)) {
              quants_val = qInd[i].quants;
            }
          }
        }
        loader.loadIndividual(
          ped_id,
          ind_id,
          tokens[Col.DAD_ID],
          tokens[Col.MOM_ID],
          tokens[Col.SEX_ID].charAt(0),
          tokens[Col.PHEN],
          tokens[Col.LIAB],
          gt_builder.buildNext(),
          quants_val,
          nSim
        );
        linecount++;
      }
    }
    catch (IOException ioe) {
      throw new GEException(ioe.getMessage() + ": line " + linecount, ioe );
    }
  }

  interface Col {
    int PED_ID = 0;
    int IND_ID = 1;
    int DAD_ID = 2;
    int MOM_ID = 3;
    int SEX_ID = 4;
    int PHEN = 5;
    int LIAB = 6;
    int MARKER1 = 7;
  }
}

