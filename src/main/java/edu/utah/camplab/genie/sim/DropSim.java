//******************************************************************************
// DropSim.java
//******************************************************************************
package edu.utah.camplab.genie.sim;

import edu.utah.camplab.genie.app.rgen.Specification;
import edu.utah.camplab.genie.gm.*;
import edu.utah.camplab.genie.hc.compressGtype;
import edu.utah.camplab.genie.ped.*;
import edu.utah.camplab.genie.util.GEException;

import java.util.Iterator;
import java.util.List;
//import edu.utah.camplab.genie.util.Ut;

//==============================================================================
public class DropSim implements GSimulator.Drop {

  public int nStudy, nLoci;
  public int nSim;
  public byte missingData;
  protected Study[] study;
  protected GDef gdef;
  protected Indiv.GtSelector gtOBS, gtSIM;
  protected Pedigree[][] thePeds;
  protected Indiv[][] descendInds, anyInds;
  protected int[] nDescendants, nPeds;
  protected GtypeBuilder gtBuilder;

  //----------------------------------------------------------------------------
  public void preProcessor() throws GEException {
  }

  //----------------------------------------------------------------------------
  public void setUserParameters(Specification spec, Study[] std) {
    study = std;
    nStudy = std.length;
  }

  //----------------------------------------------------------------------------
  public void setPedData() {
    descendInds = new Indiv[nStudy][];
    anyInds = new Indiv[nStudy][];
    thePeds = new Pedigree[nStudy][];
    nPeds = new int[nStudy];
    nDescendants = new int[nStudy];

    for (int i = 0; i < nStudy; i++) {
      PedData pd = study[i].getPedData();
      thePeds[i] = pd.getPedigrees();
      nPeds[i] = thePeds[i].length;
      descendInds[i] = pd.getIndividuals(PedQuery.IS_DESCENDANT);
      anyInds[i] = pd.getIndividuals(PedQuery.IS_ANY);
      nDescendants[i] = descendInds[i].length;
    }
  }

  //----------------------------------------------------------------------------
  public void setGDef(GDef gd) {
    gdef = gd;
    nLoci = gdef.getLocusCount();
    gtBuilder = gdef.getGtypeBuilder();
    missingData = gdef.getAlleleFormat().getMissingData();
  }

  //----------------------------------------------------------------------------
  //public void setNumSimulation( int num )
  //{
  //  nSim = num;
  //}

  //----------------------------------------------------------------------------
  public void simulateDescendantGenotypes(int index)
    throws GEException {
    //gtORG = Indiv.GtSelector.ORG;
    gtOBS = Indiv.GtSelector.OBS;
    gtSIM = Indiv.GtSelector.SIM;

    for (int i = 0; i < nStudy; i++) {
      // reset descendant simulated genotypes
      for (int j = 0; j < nDescendants[i]; j++) {
        descendInds[i][j].setSimulatedGtype(null, index);
      }

      for (int iped = 0; iped < nPeds[i]; ++iped) {
        Marriage[] marrs = thePeds[i][iped].getFoundingMarriages();
        for (int imarr = 0, n = marrs.length; imarr < n; ++imarr) {
          drop(marrs[imarr], index); // start recursive descend
        }
      }

      // reset simulated data to match real original data - th respect to missing value
      for (int ind = 0; ind < anyInds[i].length; ind++) {
        Gtype obs = anyInds[i][ind].getGtype(gtOBS);
        Gtype sim = anyInds[i][ind].getSimulatedGtype(index);
        boolean indicator = false;

        for (int l = 0; l < nLoci; l++) {
          AllelePair obspair = null;

          if (obs != null) {
            obspair = obs.getAllelePairAt(l);
          }

          if (obs != null && obspair != null) {
            AllelePair simpair = sim.getAllelePairAt(l);
            gtBuilder.addAllelePair(
              simpair.getAlleleCode(true), simpair.getAlleleCode(false));
          }
          else {
            gtBuilder.addAllelePair(missingData, missingData);
            indicator = true;
          }
        }

        if (indicator) {
          anyInds[i][ind].setSimulatedGtype(gtBuilder.buildNext(), index);
          //System.out.println("reset : ind " + anyInds[i][ind].getID() + "  gt : " + anyInds[i][ind].getSimulatedGtype(index));
        }
        else {
          gtBuilder.buildclean();
        }
      }
    }
  }

  // overloaded for hapBuilder
  public void simulateDescendantGenotypes(int index, compressGtype[] cGtype,
                                          int step)
    throws GEException {
    System.out.println("WARNING : simulated descendant without using compressGtype");
  }

  //----------------------------------------------------------------------------
  private void drop(Marriage m, int index) throws GEException {
    Gtype his_gt = m.getHim().getSimulatedGtype(index);
    if (his_gt == null)  // husband is somebody's kid not yet processed
    {
      return;
    }

    Gtype her_gt = m.getHer().getSimulatedGtype(index);
    if (her_gt == null)  // wife is somebody's kid not yet processed
    {
      return;
    }

    // for each kid...
    for (Iterator kidit = m.getKids().iterator(); kidit.hasNext(); ) {
      Indiv kid = (Indiv) kidit.next();

      // if we haven't yet reached this kid descending via spouse
      if (kid.getSimulatedGtype(index) == null) {
        //System.out.println("kid " + kid.getPedigree().getID() + " - " + kid.getID());
        // the facts of life, geek-style:
        Gamete sperm = his_gt.doMeiosis();
        Gamete egg = her_gt.doMeiosis();
        for (int j = 0; j < nLoci; ++j) {
          //System.out.println(" Locus: " + j + "  father allele is " + sperm.getAllele(j) + " mother allele is " + egg.getAllele(j));
          gtBuilder.addAllelePair(sperm.getAllele(j), egg.getAllele(j));
        }

        kid.setSimulatedGtype(gtBuilder.buildNext(), index);
        gtBuilder.buildclean();
        // added buildclean to make sure the next simulation is clean
        // for each of this kid's marriages, if any...
        List marrs = kid.getMarriages();
        if (marrs != null) {
          for (Iterator marrit = marrs.iterator(); marrit.hasNext(); ) {
            drop((Marriage) marrit.next(), index);   // recurse
          }
        }
      }
    }
  }
}
