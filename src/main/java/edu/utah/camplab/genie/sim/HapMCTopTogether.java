//******************************************************************************
// HapMCTopTogether.java
//******************************************************************************
package edu.utah.camplab.genie.sim;

import edu.utah.camplab.genie.ped.Indiv;
import edu.utah.camplab.genie.ped.PedQuery;
import edu.utah.camplab.genie.prep.PhaseData;
import edu.utah.camplab.genie.util.GEException;

//==============================================================================
//public class HapMCTopTogether extends HapMCTopSim 
public class HapMCTopTogether extends HapFreqTopSim {

  //----------------------------------------------------------------------------
  public HapMCTopTogether() {
  }

  //----------------------------------------------------------------------------
  public void preProcessor() throws GEException {
    PedQuery.Predicate[] querySample = new PedQuery.Predicate[]{
      PedQuery.IS_TOGETHER};
    //setDataSource(querySample);
    PhaseData phaseData = new PhaseData();
    phaseData.setDataSource(study,
      Indiv.GtSelector.OBS,
      gdef,
      -1,
      pInSample,
      querySample);
  }
}

