//******************************************************************************
// GDef.java
//******************************************************************************
package edu.utah.camplab.genie.gm;

import java.util.TreeMap;

//==============================================================================
public interface GDef {
  public int getLocusCount();

  public Locus getLocus(int index);

  public GtypeBuilder getGtypeBuilder();

  public AlleleFormat getAlleleFormat();

  public void setAlleleFormat(AlleleFormat af);

  public boolean getDecoderFlag();

  public void setDecoderFlag(boolean decoder);

  //public boolean getCharCode();
  public interface Locus {
    public int getID();

    public String getMarker();

    public void setMarker(String inMarker);

    public double getTheta();

    public boolean alleleOrderIsSignificant();

    public void addDecoderMap(TreeMap<Byte, FreqDataSet> inMap);

    public TreeMap<Byte, FreqDataSet> getDecoderMap();
    //public void addFrequency(Frequency[] freq);
    //public Frequency[] getFrequency();
  }
}

