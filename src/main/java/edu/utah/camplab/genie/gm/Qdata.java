//******************************************************************************
// Qdata.java
//******************************************************************************
package edu.utah.camplab.genie.gm;

import edu.utah.camplab.genie.util.Ut;

import java.util.ArrayList;
import java.util.List;

//==============================================================================
public class Qdata {

  private double[] qvals;

  //----------------------------------------------------------------------------
  public Qdata(double[] quants) {
    qvals = quants;
  }

  //----------------------------------------------------------------------------
  public String toString() {
    List l = new ArrayList();
    for (int i = 0; i < qvals.length; ++i) {
      String qv = Double.toString(qvals[i]);
      l.add(qv);
    }
    return Ut.join(l, " ");
  }

  //----------------------------------------------------------------------------
  public double getQdata(int i) {
    return qvals[i];
  }
}

