//******************************************************************************
// Repeat.java
//******************************************************************************
package edu.utah.camplab.genie.gm;

import edu.utah.camplab.genie.util.GEException;

//==============================================================================
public interface Repeat {
  public int[][] getGroup(int[] lociRange, int groupSize)
    throws GEException;
}
