//******************************************************************************
// GtypeMatcher.java
//******************************************************************************
package edu.utah.camplab.genie.gm;

//import java.util.regex.Pattern;
//import java.util.regex.PatternSyntaxException;

import edu.utah.camplab.genie.util.GEException;

import java.util.StringTokenizer;
import java.util.TreeMap;

//==============================================================================
public class GtypeMatcher {

  public final byte[][] originalPatterns;
  public int[] iLoci;
  //private final Pattern[] rxPatterns;
  private byte[][] rxPatterns;
  private byte missingData;
  private GDef gdef;
  private AlleleFormat alleleformat;
  private boolean decoderflag;

  //----------------------------------------------------------------------------
  public GtypeMatcher(int[] selected_locus_indices, GDef gdef) {
    iLoci = selected_locus_indices;
    //rxPatterns = new Pattern[iLoci.length];
    rxPatterns = new byte[iLoci.length][];
    originalPatterns = new byte[iLoci.length][];
    this.gdef = gdef;
    alleleformat = gdef.getAlleleFormat();
    decoderflag = gdef.getDecoderFlag();
    //for ( int i = 0 ; i < iLoci.length; i++ ) 
    //System.out.println("this gtypematcher is for locus : " + iLoci[i]);
  }

  //----------------------------------------------------------------------------
  public void updateSelectedLocus(int locus_index) {
    iLoci = new int[1];
    iLoci[0] = locus_index;
    if (decoderflag) {
      changeRegex(0, locus_index);
    }
  }

  //----------------------------------------------------------------------------
  public void updateSelectedLocus(int[] locus_index) {
    iLoci = locus_index;
    if (decoderflag) {
      for (int i = 0; i < iLoci.length; i++) {
        changeRegex(i, iLoci[i]);
      }
    }
  }

  //----------------------------------------------------------------------------
  public boolean matchesGtype(Gtype gt) {
    boolean matched = true;

    //System.out.println("missing Data : " + missingData);
    if (gt == null) {
      return false;
    }

    for (int i = 0; i < iLoci.length; ++i) {
      if (matched) {
        AllelePair ap = gt.getAllelePairAt(iLoci[i]);
        matched = false;

        if (ap != null) {
          byte a1 = ap.getAlleleCode(true);
          byte a2 = ap.getAlleleCode(false);
          int j = 0;
          int nPatterns = rxPatterns[i].length;

          while (j < nPatterns) {
            //System.out.println("i : " + i + " j : " + j);
            if ((rxPatterns[i][j] == missingData || rxPatterns[i][j] == a1) &&
              (rxPatterns[i][j + 1] == missingData || rxPatterns[i][j + 1] == a2)) {
              j = nPatterns;
              matched = true;
            }
            else {
              j += 2;
            }
          }
        }
      }
      else {
        return matched;
      }
    }
    return matched;
  }

  //----------------------------------------------------------------------------
  public boolean matchesGtype(Gtype gt, int allelePosition) {
    boolean first = true;
    if (allelePosition == 1) {
      first = false;
    }
    return matchesGtype(gt, first);
  }

  //----------------------------------------------------------------------------
  public boolean matchesGtype(Gtype gt, boolean allelePosition) {
    boolean matched = true;

    if (gt == null) {
      return false;
    }

    for (int i = 0; i < iLoci.length; ++i) {
      if (matched == true) {
        AllelePair ap = gt.getAllelePairAt(iLoci[i]);
        matched = false;

        if (ap != null) {
          byte a1 = ap.getAlleleCode(allelePosition);
          //System.out.println("GtypeMatcher; loci : " + i + " allele : " + a1);
          int j = 0;
          int nPatterns = rxPatterns[i].length;

          while (j < nPatterns) {
            //System.out.println("pattern to be matched : " + rxPatterns[i][j]);
            if (rxPatterns[i][j] == missingData || rxPatterns[i][j] == a1) {
              //System.out.println("matched okay");
              j = nPatterns;
              matched = true;
            }
            else {
              j++;
            }
          }
        }
        else {
          //System.out.println("ap is null ");
          return false;
        }
      }
      else {
        //System.out.println("failed to match pattern");
        return false;
      }
    }
    String out = new String((matched == true) ? "true" : "false");
    //System.out.println("returning value : " + out);
    return matched;
  }

  //----------------------------------------------------------------------------
  public void setRegex(int i, String regex) throws GEException {
    missingData = alleleformat.getMissingData();
    String ss = regex.replaceAll("[ \t\n\f\r]", "");
    StringTokenizer st = new StringTokenizer(ss, "/|()");
    originalPatterns[i] = new byte[st.countTokens()];
    rxPatterns[i] = new byte[st.countTokens()];
    int j = 0;
    while (st.hasMoreTokens()) {
      String token = st.nextToken();
      if (token.equals(".")) {
        originalPatterns[i][j] = missingData;
      }
      else {
        if (decoderflag) {
          originalPatterns[i][j] = alleleformat.convertNumAllele(token);
        }
        else {
          originalPatterns[i][j] = alleleformat.convertAllele(token);
        }
      }
      String orig = Byte.toString(originalPatterns[i][j]);
      j++;
    }
    for (int k = 0; k < originalPatterns[i].length; k++) {
      rxPatterns[i][k] = originalPatterns[i][k];
    }
  }

  //----------------------------------------------------------------------------
  public void changeRegex() {
    for (int i = 0; i < iLoci.length; i++) {
      changeRegex(i, iLoci[i]);
    }
  }

  public void changeRegex(int i, int locus_index) {
    //System.out.println("Changing Pattern for locus index " + locus_index);
    GDef.Locus locus = gdef.getLocus(locus_index);
    TreeMap<Byte, FreqDataSet> decoderMap = locus.getDecoderMap();
    //for ( Iterator it = decoderMap.keySet().iterator(); it.hasNext(); ) {
    //Byte key = (Byte) it.next();
    //Byte value = decoderMap.get(key);
    //System.out.println(" key : " + key.toString() + " value : " + value.toString());
    //}

    if (decoderMap != null) {
      for (int j = 0; j < originalPatterns[i].length; j++) {
        Byte bb = Byte.valueOf(originalPatterns[i][j]);
        //Byte value = decoderMap.get(bb);
        FreqDataSet value = decoderMap.get(bb);
        if (value != null) {
          rxPatterns[i][j] = (value.getCode())[0];
        }
        else {
          rxPatterns[i][j] = originalPatterns[i][j];
        }
      }
    }
  }
}
