//******************************************************************************
// AlleleFormat.java
//******************************************************************************
package edu.utah.camplab.genie.gm;

//==============================================================================
public class AlleleFormat {
  //public static byte missingData = (byte) 0;
  public byte missingData;
  public String format;

  public AlleleFormat(String informat) {
    format = informat;
  }

  //----------------------------------------------------------------------------
  public byte convertNumAllele(String inString) {
    try {
      return Byte.valueOf(inString);
    } catch (NumberFormatException e) {
      System.out.println("change allele format from " + format + " to character");
      format = "character";
      return convertCharAllele(inString);
    }
  }

  //----------------------------------------------------------------------------
  public byte convertCharAllele(String inString) {
    //return Integer.valueOf(inString.codePointAt(0)).byteValue();
    if (inString.length() == 1) {
      char c = inString.charAt(0);
      return (byte) (c & 0xFF);
    }
    else {
      System.out.println("Failed to read allele has more than a single character.");
      return missingData;
    }
  }

  //----------------------------------------------------------------------------
  public byte convertAllele(String inString) {
    //System.out.println("allele format is " + format);
    if (format.startsWith("num")) {
      return convertNumAllele(inString);
    }
    else {
      return convertCharAllele(inString);
    }
  }

  //----------------------------------------------------------------------------
  public String toString(byte inbyte) {
    String returnString = "";
    //if (format.startsWith("num") )
    //  returnString = "" + inbyte;
    //else 
    if (inbyte == missingData) {
      returnString = ".";
    }
    else if (format.startsWith("char")) {
      returnString += (char) (inbyte & 0xFF);
    }
    else {
      returnString += (inbyte & 0xFF);
    }
    return returnString;
    /*
    int intValue = inbyte;
    if ( format.startsWith("num") ) 
      return String.valueOf(intValue);
    else {
      char[] ch = Character.toChars(intValue);
      if ( ch.length == 1 )
        return Character.toString(ch[0]);
      else {
        System.out.println ("Failed to convert Allele from byte back to String, input byte convert to more than a single char!" );
        return null;
      }
    }
    **/
  }

  //----------------------------------------------------------------------------
  public byte getMissingData() {
    return missingData;
  }

  //----------------------------------------------------------------------------
  public void setMissingData(String inString) {
    //setMissingData( Integer.valueOf(inString.codePointAt(0)).byteValue() );
    byte md;
    if (format.startsWith("num")) {
      md = convertNumAllele(inString);
    }
    else {
      md = convertCharAllele(inString);
    }

    setMissingData(md);
  }

  //----------------------------------------------------------------------------
  public void setMissingData(byte inbyte) {
    missingData = inbyte;
  }

  //----------------------------------------------------------------------------
  public String getFormat() {
    return format;
  }
}
