import alun.genio.*;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * This program removes individuals from the pedigree unless they have
 * a minimum number of loci at least partially observed or they are
 * ancestors of someone else that is observed.
 *
 * <ul>
 * Usage : <b> java TrimPed input.par input.ped min_calls </b>
 * </ul>
 * where
 * <ul>
 * <li> <b> input.par </b> is the original LINKAGE paramter file </li>
 * <li> <b> input.ped </b> is the original LINKAGE pedigree file </li>
 * <li> <b> min_calls </b> is the minimum number of loci with some information </li>
 * </ul>
 * A new LINKAGE pedigree data file is written to standard output.
 */

public class TrimPed {
  public static void main(String[] args) {
    try {
      int mingenos = 0;

      switch (args.length) {
        case 3:
          mingenos = new Integer(args[2]).intValue();
          break;
        default:
          System.err.println("Usage: java TrimPed input.par input.ped min_genos");
          System.exit(1);
      }

      LinkageDataSet data = new LinkageDataSet(args[0], args[1]);
      LinkagePedigreeData ped = data.getPedigreeData();
      GeneticDataSource gen = new LinkageInterface(data);

      LinkageIndividual[] ind = ped.getIndividuals();
      Set<LinkageIndividual> keep = new LinkedHashSet<LinkageIndividual>();

      for (int i = 0; i < ind.length; i++) {
        int count = 0;
        for (int j = 0; j < gen.nLoci() && count < mingenos; j++) {
          if (gen.penetrance(i, j) != null) {
            count++;
          }
        }

        if (count >= mingenos) {
          put(ind[i], keep, ped);
        }
      }

      System.out.println(new LinkagePedigreeData(keep));
    } catch (Exception e) {
      System.err.println("Caught in TrimPed:main()");
      e.printStackTrace();
    }
  }

  public static void put(LinkageIndividual a, Set<LinkageIndividual> s, LinkagePedigreeData p) {
    if (a != null && !s.contains(a)) {
      s.add(a);
      put(p.pa(a), s, p);
      put(p.ma(a), s, p);
    }
  }
}
