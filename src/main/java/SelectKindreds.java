import alun.genio.LinkageFormatter;
import alun.genio.LinkagePedigreeData;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * This program selects a subset of kindreds from a LINKAGE pedigree file.
 * <ul>
 * Usage : <b> java SelectKindreds < input.par [k1] [k2] ... </b>
 * </ul>
 * where
 * <ul>
 * <li> <b> input.ped </b> is the original LINKAGE pedigree file </li>
 * <li> <b> [k1] [k2] ... </b> is a list of the kindred identifiers specifying which to include in the
 * output</li>
 * </ul>
 * The new LINKAGE pedigree data is written to the standard output file.
 * You can probably do the same thing with a grep command.
 */

public class SelectKindreds {
  public static void main(String[] args) {
    try {
      LinkageFormatter lf = new LinkageFormatter(new BufferedReader(new InputStreamReader(System.in)), "Ped file");
      LinkagePedigreeData p = new LinkagePedigreeData(lf, null, false);
      LinkagePedigreeData[] ped = p.splitByPedigree();

      for (int j = 0; j < args.length; j++) {
        int pedid = new Integer(args[j]).intValue();
        for (int i = 0; i < ped.length; i++) {
          if (pedid == ped[i].pedid()) {
            System.out.println(ped[i]);
          }
        }
      }
    } catch (Exception e) {
      System.err.println("Caught in SelectKindreds:main()");
      e.printStackTrace();
    }
  }
}
