package alun.genio;

import alun.graph.EdgedNetwork;
import alun.util.Triplet;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;

public class Pedigree {
  private EdgedNetwork<Object, LinkedHashSet<Object>> g = null;
  private LinkedHashMap<Object, Triplet> h = null;
  private LinkedHashSet<Object> males = null;
  private LinkedHashSet<Object> females = null;

  public Pedigree() {
    g = new EdgedNetwork<Object, LinkedHashSet<Object>>();
    h = new LinkedHashMap<Object, Triplet>();
    males = new LinkedHashSet<Object>();
    females = new LinkedHashSet<Object>();
  }

  public void addTriplet(Triplet t) {
    LinkedHashSet<Object> s = g.connection(t.y, t.z);
    if (t.y != null && t.z != null) {
      if (s == null) {
        s = new LinkedHashSet<Object>();
        g.connect(t.y, t.z, s);
      }
      s.add(t.x);
    }
    h.put(t.x, t);
    males.add(t.y);
    females.add(t.z);
  }

  public void addTriplet(Triplet[] t) {
    for (int i = 0; i < t.length; i++) {
      addTriplet(t[i]);
    }
  }

  public Triplet getTriplet(Object x) {
    return h.get(x);
  }

  public Object[] kids(Object x) {
    Collection ss = g.getNeighbours(x);
    if (ss == null) {
      Object[] result = {};
      return result;
    }

    LinkedHashSet<Object> k = new LinkedHashSet<Object>();
    for (Iterator i = ss.iterator(); i.hasNext(); ) {
      k.addAll(g.connection(x, i.next()));
    }
    return k.toArray();
  }

  public Family[] nuclearFamilies() {
    EdgedNetwork<Object, LinkedHashSet<Object>> n = new EdgedNetwork<Object, LinkedHashSet<Object>>(g);
    LinkedHashSet<Family> f = new LinkedHashSet<Family>();
    LinkedHashSet<Object> vertices = new LinkedHashSet<Object>(n.getVertices());
    for (Object u : vertices) //n.getVertices())
    {
      if (males.contains(u)) {
        for (Object v : n.getNeighbours(u)) {
          Family ff = new Family();
          ff.setPa(u);
          ff.setMa(v);
          for (Object w : n.connection(u, v)) {
            ff.addKid(w);
          }
          f.add(ff);
        }
        n.remove(u);
      }
    }
    return (Family[]) f.toArray(new Family[0]);
  }
}
