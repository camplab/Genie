package alun.genio;

public interface GeneticDataSource {
  /**
   * The identifier of this data source.
   */
  public String name();

  /**
   * The number of genetic loci for which data is available.
   * The loci will be accessed by index from 0 to nLoci()-1
   * and are assumed to be in corresponding physical order.
   */
  public int nLoci();

  /**
   * Returns a String identifier for the jth locus.
   */
  public String locName(int j);

  /**
   * The number of alleles at the jth genetic locus.
   */
  public int nAlleles(int j);

  /**
   * Returns a string representation of the observation for the ith individuals at
   * the jth locus.
   */
  public String call(int i, int j);

  /**
   * The number of phenotypes at the jth genetic locus.
   * Must be greater than zero and may be infinite.
   * Phenotype is used to mean any observation for the locus, including
   * genotype information. So for a co-dominant genetic marker nPhenotypes(j)
   * should return the same as nAlleles(j)*(nAlleles(j)-1)/2.
   */
  public int nPhenotypes(int j);

  /**
   * The frequencies of the alleles at the jth genetic locus.
   */
  public double[] alleleFreqs(int j);

  /**
   * Returns the physical distance in Mb between loci i and j.
   */
  public double getPhysicalDistance(int i, int j);

  /**
   * Returns the genetic distance in cM between loci i and j.
   */
  public double getGeneticDistance(int i, int j);

  /**
   * Returns the probability of a recombination between loci i and j in
   * a single meiosis in females.
   */
  public double getFemaleRecomFrac(int i, int j);

  /**
   * Returns the probability of a recombination between loci i and j in
   * a single meiosis in males.
   */
  public double getMaleRecomFrac(int i, int j);

  /**
   * Returns the number of individuals who are being considered.
   * Individuals will be accessed by integers between 0 and
   * nIndiviuals()-1 so repeated calls to the methods below must
   * give consistent responses.
   */
  public int nIndividuals();

  /**
   * Returns the position in the list of individuals of the father
   * of the individual in the ith position in the list. If father
   * is not in the list -1 should be returned.
   */
  public int pa(int i);

  /**
   * Returns the position in the list of individuals of the mother
   * of the individual in the ith position in the list. If mother
   * is not in the list -1 should be returned.
   */
  public int ma(int i);

  /**
   * Returns the positions in the list of individuals of the children
   * of the individual in the ith position in the list. If there
   * are no offspring an array of length zero is returned.
   */
  public int[] kids(int i);

  /**
   * Returns a matrix of integers indexing the positions of a
   * nuclear family. One row per family. The first element in
   * the row is the father, the second the mother and the remainder
   * are the indexes of the children.
   **/
  public int[][] nuclearFamilies();

  /**
   * Returns a String identifyer for the ith person in the list.
   */
  public String id(int i);

  /**
   * If k = 0, returns the first allele for the ith individual
   * at the jth locus. If k = 1, returns the second allele
   * for the ith individual at the jth locus.
   * Allele numbers should start at 0. If such a clear allele call
   * is not possible return -1.
   */
  public int indAllele(int i, int j, int k);

  /**
   * Returns the penetrance function for the ith person in the list
   * at the jth genetic locus.
   * This is a matrix of dimension nAlleles(j) x nAlleles(j) where the
   * entry at row a, column b is the probability of the individual's
   * observed phenotype given that they have allele number a on the
   * paternal chromosome and allele number b on the maternal chromosome.
   * Again, phenotype is used in a very general way so this function is
   * also used to express genotypic information. If an individual i
   * is observed with genotype (x,y) at locus j then
   * penetrance(i,j)[x,y] = penetrance(i,j)[y,x] = 1
   * with zeros elsewhere.
   * If the locus is not perfectly co-dominant, or there is error
   * in the observation, this should be expressed appropriately in this
   * matrix.
   * If no information is present for the ith individual at the jth locus
   * this method can return null.
   */
  public double[][] penetrance(int i, int j);
}
