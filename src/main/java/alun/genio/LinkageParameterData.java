package alun.genio;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * This structure hold the information associated with the data
 * from a linkage parameter file.
 */
public class LinkageParameterData {
  public int nloci = 0;
  public int risklocus = 0;
  public int sexlinked = 0;
  public int programcode = 0;
  public String line1comment = null;
  public int mutlocus = 0;
  public double mutmale = 0;
  public double mutfemale = 0;
  public int disequilibrium = 0;
  public String line2comment = null;
  public int[] order = null;
  public String line3comment = null;
  public LinkageLocus[] locus = null;
  public int sexdifference = 0;
  public int interference = 0;
  public String line4comment = null;
  public double[] distances = null;
  public String line5comment = null;
  public int variablelocus = 0;
  public double increment = 0;
  public double stoppingvalue = 0;
  public String line6comment = null;

  /**
   * Creates a new parameter file structure using data read from
   * the given input formatter.
   */
  public LinkageParameterData(LinkageFormatter b) throws IOException {
    b.readLine();
    nloci = b.readInt("number of loci", 0, true, true);
    risklocus = b.readInt("risk locus number", 0, false, false);
    sexlinked = b.readInt("sex linkage code", 0, false, false);
    programcode = b.readInt("program code", 0, false, false);
    line1comment = b.restOfLine();

    b.readLine();
    mutlocus = b.readInt("mutation locus code", 0, false, false);
    mutmale = b.readDouble("male mutation rate", 0, false, false);
    mutfemale = b.readDouble("female mutation rate", 0, false, false);
    disequilibrium = b.readInt("disequilibrium code", 0, false, false);
    line2comment = b.restOfLine();

    b.readLine();
    order = new int[nloci];
    boolean ok = true;
    for (int i = 0; ok && i < order.length; i++) {
      if (b.newToken() && b.nextIsInt()) {
        order[i] = b.getInt();
      }
      else {
        ok = false;
      }
    }
    if (!ok) {
      b.warn("Can't read physical order of loci.\n\tAssumed to be in same order as in file.");
      for (int i = 0; i < order.length; i++) {
        order[i] = i + 1;
      }
    }
    line3comment = b.restOfLine();

    locus = new LinkageLocus[nloci];
    for (int i = 0; i < locus.length; i++) {
      b.readLine();
      int type = b.readInt("locus code", 0, true, true);

      switch (type) {
        case LinkageLocus.AFFECTION_STATUS:
          locus[i] = new AffectionStatusLocus(b);
          break;
        case LinkageLocus.NUMBERED_ALLELES:
          locus[i] = new NumberedAlleleLocus(b);
          break;
        case LinkageLocus.QUANTITATIVE_VARIABLE:
          locus[i] = new QuantitativeLocus(b);
          break;

        case LinkageLocus.BINARY_FACTORS:
          //locus[i] = new LinkageBinaryLocus(b);
        default:
          b.crash("LINKAGE locus code " + type + " is not yet implemented in these programs.");
          break;
      }
    }

    b.readLine();
    sexdifference = b.readInt("sex recombination difference code", 0, false, false);
    interference = b.readInt("interference code", 0, false, false);
    line4comment = b.restOfLine();

    b.readLine();
    distances = new double[nloci - 1];
    ok = true;
    for (int j = 0; ok && j < distances.length; j++) {
      if (b.newToken() && b.nextIsDouble()) {
        distances[j] = b.getDouble();
      }
      else {
        b.crash("Can't read " + distances.length + " recombiantion fractions as doubles.");
      }
    }
    line5comment = b.restOfLine();

    ok = true;
    for (int j = 0; ok && j < distances.length; j++) {
      if (distances[j] < 0) {
        b.crash("Negative recombination fraction specified " + distances[j] + ".");
      }
      if (distances[j] > 0.5) {
        ok = false;
      }
    }

    if (!ok) {
      b.warn("At least one recombination fraction is greater than 0.5." +
        "\n\tWill assume distances are centi Morgans and convert using Kosambi mapping function");
      for (int j = 0; j < distances.length; j++) {
        distances[j] = cMToTheta(distances[j]);
      }
    }

    b.readLine();
    variablelocus = b.readInt("variable locus code", 0, false, false);
    increment = b.readDouble("increment amount", 0, false, false);
    stoppingvalue = b.readDouble("stoppint value", 0, false, false);
    line6comment = b.restOfLine();
  }
  public LinkageParameterData() {
  }

  public LinkageParameterData(LinkageParameterData p) {
    nloci = p.nloci;
    risklocus = p.risklocus;
    sexlinked = p.sexlinked;
    programcode = p.programcode;
    line1comment = p.line1comment;
    mutlocus = p.mutlocus;
    mutmale = p.mutmale;
    mutfemale = p.mutfemale;
    disequilibrium = p.disequilibrium;
    line2comment = p.line2comment;
    line3comment = p.line3comment;
    sexdifference = p.sexdifference;
    interference = p.interference;
    line4comment = p.line4comment;
    line5comment = p.line5comment;
    variablelocus = p.variablelocus;
    increment = p.increment;
    stoppingvalue = p.stoppingvalue;
    line6comment = p.line6comment;

    order = new int[p.order.length];
    for (int i = 0; i < order.length; i++) {
      order[i] = p.order[i];
    }

    distances = new double[p.distances.length];
    for (int i = 0; i < distances.length; i++) {
      distances[i] = p.distances[i];
    }

    locus = new LinkageLocus[p.locus.length];
    for (int i = 0; i < locus.length; i++) {
      switch (p.locus[i].type) {
        case LinkageLocus.AFFECTION_STATUS:
          locus[i] = new AffectionStatusLocus((AffectionStatusLocus) p.locus[i]);
          break;
        case LinkageLocus.NUMBERED_ALLELES:
          locus[i] = new NumberedAlleleLocus((NumberedAlleleLocus) p.locus[i]);
          break;
        case LinkageLocus.QUANTITATIVE_VARIABLE:
          locus[i] = new QuantitativeLocus((QuantitativeLocus) p.locus[i]);
          break;

        case LinkageLocus.BINARY_FACTORS:
          //locus[i] = new BinaryLocus((BinaryLocus)p.locus[i]));
        default:
          //b.crash("LINKAGE locus code "+type+" is not yet implemented in these programs.");
          break;
      }
    }
  }
  public LinkageParameterData(LinkageParameterData p, int[] x) {
    nloci = x.length;
    risklocus = p.risklocus;
    sexlinked = p.sexlinked;
    programcode = p.programcode;
    line1comment = p.line1comment;

    mutlocus = p.mutlocus;
    mutmale = p.mutmale;
    mutfemale = p.mutfemale;
    disequilibrium = p.disequilibrium;
    line2comment = p.line2comment;

    order = new int[nloci];
    for (int i = 0; i < order.length; i++) {
      order[i] = i + 1;
    }
    line3comment = p.line3comment;

    locus = new LinkageLocus[x.length];
    for (int i = 0; i < locus.length; i++) {
      locus[i] = p.locus[x[i]];
    }

    sexdifference = p.sexdifference;
    interference = p.interference;
    line4comment = p.line4comment;

    distances = new double[nloci - 1];
    for (int i = 0; i < distances.length; i++) {
      distances[i] = p.getTheta(x[i], x[i + 1]);
    }
    line5comment = p.line5comment;
/*
		distances = new double[nloci-1];
		for (int i=0; i<distances.length; i++)
			distances[i] = p.getGeneticDistance(x[i],x[i+1]);
		line5comment = p.line5comment;
*/

    variablelocus = p.variablelocus;
    increment = p.increment;
    stoppingvalue = p.stoppingvalue;
    line6comment = p.line6comment;
  }

  /**
   * Converts Megabases to centiMorgans at 1 to 1.
   */
  public static double MbTocM(double p) {
    return p;
  }

  /**
   * Converts centiMorgans to Megabases at 1 to 1.
   */
  public static double cMToMb(double g) {
    return g;
  }

  /**
   * Converts centiMorgans to recombination fractions using the Kosambi conversion.
   */
  public static double cMToTheta(double d) {
    double t = d / 100.0;
    t = Math.exp(-4 * t);
    t = 0.5 * (1 - t) / (1 + t);
    return t;
  }

  /**
   * Converts recombination fractions to centiMorgans using the Kosambi conversion.
   */
  public static double thetaTocM(double t) {
    double d = (1 - 2 * t) / (1 + 2 * t);
    d = -0.25 * Math.log(d);
    d = d * 100;
    return d;
  }

  /**
   * Test main.
   */
  public static void main(String[] args) {
    try {
      LinkageFormatter f = new LinkageFormatter(new BufferedReader(new InputStreamReader(System.in)), "Par file");
      LinkageParameterData l = new LinkageParameterData(f);
      System.out.println(l);
/*
			double d = 0.025;
			System.out.println(d);
			d = thetaTocM(d);
			System.out.println(d);
			d = cMToTheta(d);
			System.out.println(d);
*/
    } catch (Exception e) {
      System.err.println("Caught in LinkageParameterData:main()");
      e.printStackTrace();
    }
  }

  /**
   * Returns the array of loci in this set of data.
   */
  public LinkageLocus[] getLoci() {
    return locus;
  }

  /**
   * Returns the number of loci in this set of data.
   */
  public int nLoci() {
    return locus.length;
  }

  /**
   * Returns a string representing the data for a collection of
   * loci in the same format as in the .par file from which it
   * was read.
   */
  public String toString() {
    StringBuffer s = new StringBuffer();

    s.append(nloci + " " + risklocus + " " + sexlinked + " " + programcode + " " + line1comment + "\n");

    s.append(mutlocus + " " + mutmale + " " + mutfemale + " " + disequilibrium + " " + line2comment + "\n");

    for (int i = 0; i < order.length; i++) {
      s.append(order[i] + " ");
    }
    s.append(line3comment + "\n");

    for (int i = 0; i < nloci; i++) {
      s.append(locus[i]);
    }

    s.append(sexdifference + " " + interference + " " + line4comment + "\n");

/*
		for (int i=0; i<distances.length; i++)
			s.append(StringFormatter.format(distances[i],2,6)+" ");
*/
    for (int i = 1; i < nLoci(); i++)
    //s.append(StringFormatter.format(getTheta(i-1,i),2,6)+" ");
    {
      s.append(getTheta(i - 1, i) + " ");
    }

    s.append(line5comment + "\n");

    s.append(variablelocus + " " + increment + " " + stoppingvalue + " " + line6comment + "\n");

    s.setLength(s.length() - 1);
    return s.toString();
  }

  public double getGeneticDistance(int i, int j) {
    double g = 0;
    int ii = i;
    int jj = j;
    if (j < i) {
      ii = j;
      jj = i;
    }

    for (int k = ii; k < jj; k++) {
      g += thetaTocM(distances[k]);
    }
    return g;
  }

  public double getPhysicalDistance(int i, int j) {
    return cMToMb(getGeneticDistance(i, j));
  }

  public double getTheta(int i, int j) {
    return cMToTheta(getGeneticDistance(i, j));
  }
}
