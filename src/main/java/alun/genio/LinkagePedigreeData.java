package alun.genio;

import alun.util.Triplet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Vector;

/**
 * A structure representing the data from a linkage .ped file.
 */
public class LinkagePedigreeData {
  private LinkageIndividual[] ind = null;
  private Pedigree ped = null;
  private boolean outputpremake = false;

  public LinkagePedigreeData() {
  }

  public LinkagePedigreeData(LinkageFormatter b, LinkageParameterData par) throws IOException {
    this(b, par, false);
  }

  public LinkagePedigreeData(LinkagePedigreeData p, int[] x) {
    Vector<LinkageIndividual> v = new Vector<LinkageIndividual>();

    LinkageIndividual[] ind = p.getIndividuals();
    for (int i = 0; i < ind.length; i++) {
      v.add(new LinkageIndividual(ind[i], x));
    }

    set(v);
  }

  /**
   * Creates a new data structure by reading pedigree and phenotype data from
   * the given input formatter. The data is checked for consistency with the
   * parameters in the given parameter data structure.
   */
  public LinkagePedigreeData(LinkageFormatter b, LinkageParameterData par, boolean premake) throws IOException {
    Vector<LinkageIndividual> v = new Vector<LinkageIndividual>();
    for (; b.newLine(); ) {
      v.addElement(new LinkageIndividual(b, par, premake));
    }
    set(v);
  }

  public LinkagePedigreeData(LinkageFormatter b, LinkageParameterData par, int format) throws IOException {
    Vector<LinkageIndividual> v = new Vector<LinkageIndividual>();
    for (; b.newLine(); ) {
      v.addElement(new LinkageIndividual(b, par, format));
    }
    set(v);
  }

  public LinkagePedigreeData(Collection<LinkageIndividual> v) {
    set(v);
  }

  /**
   * Main reads in a linkage parameter file and a linkage pedigree file which is
   * in short form and output the linkage pedigree file in long form.
   */

  public static void main(String[] args) {
    try {
      boolean premake = false;

      switch (args.length) {
        case 1:
          if (args[0].equals("-s")) {
            premake = true;
          }
          break;
      }

      LinkageFormatter f = new LinkageFormatter(new BufferedReader(new InputStreamReader(System.in)), "Ped file");
      LinkagePedigreeData ped = new LinkagePedigreeData(f, null, premake);
      System.out.println(ped);
    } catch (Exception e) {
      System.err.println("Caught in LinkagePedigreeData:main()");
      e.printStackTrace();
    }
  }

  public void set(Collection<LinkageIndividual> v) {
    ind = (LinkageIndividual[]) v.toArray(new LinkageIndividual[v.size()]);
    for (int i = 0; i < ind.length; i++) {
      ind[i].index = i;
    }

    LinkedHashMap<String, LinkageIndividual> h = new LinkedHashMap<String, LinkageIndividual>();
    for (int i = 0; i < ind.length; i++) {
      String s = ind[i].pedid + "::" + ind[i].id;
/*
			if (h.get(s) != null)
				throw new LinkageException("Multiple input lines for kinship="+ind[i].pedid+" id="+ind[i].id);
*/
      h.put(s, ind[i]);
    }

    Triplet<LinkageIndividual, LinkageIndividual, LinkageIndividual>[] t =
      (Triplet<LinkageIndividual, LinkageIndividual, LinkageIndividual>[]) new Triplet[ind.length];
    for (int i = 0; i < ind.length; i++) {
      String s = ind[i].pedid + "::" + ind[i].paid;
      LinkageIndividual pa = (LinkageIndividual) h.get(s);
      s = ind[i].pedid + "::" + ind[i].maid;
      LinkageIndividual ma = (LinkageIndividual) h.get(s);
      t[i] = new Triplet<LinkageIndividual, LinkageIndividual, LinkageIndividual>(ind[i], pa, ma);
    }

    ped = new Pedigree();
    ped.addTriplet(t);
  }

  public LinkageIndividual pa(LinkageIndividual x) {
    for (int i = 0; i < ind.length; i++) {
      if (ind[i].id == x.paid) {
        return ind[i];
      }
    }
    return null;
  }

  public LinkageIndividual ma(LinkageIndividual x) {
    for (int i = 0; i < ind.length; i++) {
      if (ind[i].id == x.maid) {
        return ind[i];
      }
    }
    return null;
  }

  /**
   * Returns the array of individual data structures.
   */
  public LinkageIndividual[] getIndividuals() {
    return ind;
  }

  public int nIndividuals() {
    return ind.length;
  }

// Private data.

  public int pedid() {
    if (ind == null || ind.length == 0) {
      return -1;
    }
    else {
      return ind[0].pedid;
    }
  }

  /**
   * Returns a string representing the data for the given pedigrees in
   * the same format as it was read in from the linkage .ped file.
   */
  public String toString() {
    StringBuffer s = new StringBuffer();
    if (outputpremake) {
      for (int i = 0; i < ind.length; i++) {
        s.append(ind[i].shortString() + "\n");
      }
    }
    else {
      for (int i = 0; i < ind.length; i++) {
        s.append(ind[i].longString() + "\n");
      }
    }

    if (ind.length > 0) {
      s.deleteCharAt(s.length() - 1);
    }
    return s.toString();
  }

  /**
   * Creates a checked Pedigree structure from the list of individuals.
   */
  public Pedigree getPedigree() {
    return ped;
  }

  public LinkagePedigreeData[] splitByPedigree() {
    LinkedHashMap<Integer, Vector<LinkageIndividual>> h = new LinkedHashMap<Integer, Vector<LinkageIndividual>>();
    for (int i = 0; i < ind.length; i++) {
      Integer x = new Integer(ind[i].pedid);
      Vector<LinkageIndividual> v = h.get(x);
      if (v == null) {
        v = new Vector<LinkageIndividual>();
        h.put(x, v);
      }
      v.add(ind[i]);
    }

    Vector<LinkageIndividual>[] u = (Vector<LinkageIndividual>[]) h.values().toArray(new Vector[0]);
    LinkagePedigreeData[] lpd = new LinkagePedigreeData[u.length];
    for (int i = 0; i < lpd.length; i++) {
      lpd[i] = new LinkagePedigreeData(u[i]);
    }

    return lpd;
  }
}
