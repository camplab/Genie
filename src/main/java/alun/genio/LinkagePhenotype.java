package alun.genio;

import alun.util.StringFormatter;

/**
 * This is a base class from with the linkage phenotype classes
 * are derived.
 */
public class LinkagePhenotype {
  protected static final StringFormatter f = new StringFormatter();
  private LinkageLocus l = null;

// Protected data.

  /**
   * Gets the locus associated with this phenotype.
   */
  public LinkageLocus getLocus() {
    return l;
  }

// Private data.

  /**
   * Sets the locus associated with this phenotype.
   */
  public void setLocus(LinkageLocus l) {
    this.l = l;
  }
}
