package alun.mcld;

import alun.graph.Graph;
import alun.graph.Network;

import java.util.Collection;
import java.util.LinkedHashSet;

public class DecomposableSearch implements GraphMHScheme {
  int np = 0;
  int code = 0;
  private Network<Locus> g = null;
  private Locus[] v = null;
  private Locus[] x = null;
  private Locus[] y = null;

// Privat data.
  private Locus xx = null;
  private Locus yy = null;
  private Collection<Locus> xn = null;
  private Collection<Locus> yn = null;
  public DecomposableSearch(Collection<Locus> l) {
    g = new Network<Locus>();
    for (Locus loc : l) {
      g.add(loc);
    }
    v = (Locus[]) g.getVertices().toArray(new Locus[0]);

    np = 20;
    x = new Locus[np];
    y = new Locus[np];
  }

  public Graph<Locus> getGraph() {
    return g;
  }

  public void initialize() {
    for (int i = 0; i < v.length; i++) {
      for (int j = 0; j < i; j++) {
        g.disconnect(v[i], v[j]);
      }
    }
  }

  public void propose() {
    if (v.length < 2) {
      code = 0;
      return;
    }

    double r = Math.random();
    if (r < 0.5) {
      code = 1;
    }
    else if (r < 0.7) {
      code = 2;
    }
    else if (r < 0.9) {
      code = 3;
    }
    else {
      code = 4;
    }

    if (code < 3) {
      np = 1;
      while (Math.random() < 0.5)
        np++;
      if (np > x.length) {
        np = x.length;
      }
      if (np > v.length) {
        np = v.length;
      }
    }

    switch (code) {
      case 0:
        break;

      case 1:
        choose(x, y, np);
        for (int i = 0; i < np; i++) {
          flip(x[i], y[i]);
        }
        break;

      case 2:
        choose(x, np);
        for (int i = 0; i < np; i++) {
          for (int j = 0; j < i; j++) {
            flip(x[i], x[j]);
          }
        }
        break;

      case 3:
        choose();
        xn = g.getNeighbours(xx);
        yn = g.getNeighbours(yy);
        xn.remove(yy);
        yn.remove(xx);
        for (Locus z : xn) {
          g.disconnect(xx, z);
        }
        for (Locus z : yn) {
          g.connect(xx, z);
        }
        for (Locus z : yn) {
          g.disconnect(yy, z);
        }
        for (Locus z : xn) {
          g.connect(yy, z);
        }
        break;

      case 4:
        choose();
        xn = g.getNeighbours(xx);
        yn = new LinkedHashSet<Locus>();
        while (yn.size() < xn.size()) {
          Locus z = null;
          do {
            z = v[(int) (Math.random() * v.length)];
          }
          while (z == xx || yn.contains(z));
          yn.add(z);
        }

        for (Locus z : xn) {
          g.disconnect(xx, z);
        }
        for (Locus z : yn) {
          g.connect(xx, z);
        }
    }
  }

  public void reject() {
    switch (code) {
      case 0:
        break;
      case 1:
        for (int i = 0; i < np; i++) {
          flip(x[i], y[i]);
        }
        break;

      case 2:
        for (int i = 0; i < np; i++) {
          for (int j = 0; j < i; j++) {
            flip(x[i], x[j]);
          }
        }
        break;

      case 3:
        for (Locus z : yn) {
          g.disconnect(xx, z);
        }
        for (Locus z : xn) {
          g.connect(xx, z);
        }
        for (Locus z : xn) {
          g.disconnect(yy, z);
        }
        for (Locus z : yn) {
          g.connect(yy, z);
        }
        break;

      case 4:
        for (Locus z : yn) {
          g.disconnect(xx, z);
        }
        for (Locus z : xn) {
          g.connect(xx, z);
        }
        break;
    }
  }

  public void accept() {
  }

  private void flip(Locus x, Locus y) {
    if (g.connects(x, y)) {
      g.disconnect(x, y);
    }
    else {
      g.connect(x, y);
    }
  }

  private void choose() {
    int a = (int) (Math.random() * v.length);
    int b = a;
    while (b == a)
      b = (int) (Math.random() * v.length);
    xx = v[a];
    yy = v[b];
  }

  private void choose(Locus[] x, int np) {
    for (int i = 0; i < np; i++) {
      boolean ok = true;
      do {
        x[i] = v[(int) (Math.random() * v.length)];
        ok = true;
        for (int j = 0; ok && j < i; j++) {
          if (x[i] == x[j]) {
            ok = false;
          }
        }
      }
      while (!ok);
    }
  }

  private void choose(Locus[] x, Locus[] y, int np) {
    for (int i = 0; i < np; i++) {
      int a = (int) (Math.random() * v.length);
      int b = a;
      while (b == a)
        b = (int) (Math.random() * v.length);
      x[i] = v[a];
      y[i] = v[b];
    }
  }
}
