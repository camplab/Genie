package alun.mcld;

import alun.graph.Graph;
import alun.markov.DenseTable;
import alun.markov.Table;
import alun.markov.Variable;
import alun.util.DoubleValue;
import alun.util.IntArray;

import java.util.*;

public class LikelihoodCalculator {
  private HaplotypeSource snpd = null;
  private LinkedHashMap<Set<Locus>, LogLikelihood> hash;

  public LikelihoodCalculator(HaplotypeSource s) {
    snpd = s;
    hash = new LinkedHashMap<Set<Locus>, LogLikelihood>();
  }

  public LDModel getLDModel(Graph<Locus> g) {
    return getLDModel(g, true);
  }

  public LDModel getLDModel(Graph<Locus> g, boolean sample) {
    Locus[] s = maxCardinality(g);
    if (s == null) {
      return null;
    }

    Vector<Table> tops = new Vector<Table>();
    Vector<Table> bots = new Vector<Table>();

    for (int i = 0; i < s.length; i++) {
      Set<Variable> ct = new LinkedHashSet<Variable>(s[i].getInvol());
      Set<Variable> cl = new LinkedHashSet<Variable>();
      cl.add(s[i]);
      cl.addAll(ct);
      tops.add(new DenseTable(cl));
      bots.add(new DenseTable(ct));
    }

    if (sample) {
      for (int j = 0; j < tops.size(); j++) {
        setUniform(tops.get(j), bots.get(j));
      }
    }

    LDModel ld = new LDModel(g.getVertices());
    Locus[] loc = (Locus[]) ld.getLoci().toArray(new Locus[0]);

    for (int i = 0; i < snpd.nHaplotypes(); i++) {
      for (int j = 0; j < loc.length; j++) {
        loc[j].setState(snpd.getAllele(i, j));
      }

      for (int j = 0; j < tops.size(); j++) {
        double x = 1;
        if (sample) {
          x = -Math.log(Math.random());
        }
        tops.get(j).increase(x);
        bots.get(j).increase(x);
      }
    }

    for (int i = 0; i < s.length; i++) {
      for (tops.get(i).init(); tops.get(i).next(); ) {
        double bott = bots.get(i).getValue();
        tops.get(i).setValue(bott > 0 ? tops.get(i).getValue() / bott : 0);
      }
      ld.add(tops.get(i));
    }

    return ld;
  }

  private void setUniform(Table a, Table b) {
    a.initToZero();
    b.initToZero();

    Variable[] u = a.getVariables().toArray(new Variable[0]);
    for (int i = 0; i < u.length; i++) {
      u[i].init();
    }

    for (int i = 0; i >= 0; ) {
      if (!u[i].next()) {
        i--;
      }
      else {
        if (++i == u.length) {
          double x = -Math.log(Math.random());
          a.increase(x);
          b.increase(x);
          i--;
        }
      }
    }
  }

  private void simplify(Locus[] s, Vector<Set<Locus>> cliques, Vector<Set<Locus>> cutsets) {
    for (int i = 0; i < s.length; i++) {
      LinkedHashSet<Locus> l = new LinkedHashSet<Locus>();
      l.addAll(s[i].getInvol());
      l.add(s[i]);
      cliques.add(l);
      l = new LinkedHashSet<Locus>();
      l.addAll(s[i].getInvol());
      cutsets.add(l);
    }

    for (int i = 0; i < cliques.size(); ) {
      Set<Locus> l = cliques.get(i);
      if (cutsets.contains(l)) {
        cliques.remove(l);
        cutsets.remove(l);
      }
      else {
        i++;
      }
    }
  }

  public LogLikelihood calc(Graph<Locus> g) {
    Locus[] s = maxCardinality(g);
    if (s == null) {
      return null;
    }
    Vector<Set<Locus>> cliques = new Vector<Set<Locus>>();
    Vector<Set<Locus>> cutsets = new Vector<Set<Locus>>();
    simplify(s, cliques, cutsets);
    LogLikelihood res = new LogLikelihood(0, 0);
    for (int i = 0; i < cliques.size(); i++) {
      res.add(calc(cliques.get(i)));
    }
    for (int i = 0; i < cutsets.size(); i++) {
      res.subtract(calc(cutsets.get(i)));
    }
    return res;
  }

// Protected methods.

  public HaplotypeSource getHaplotypeSource() {
    return snpd;
  }

  public void clear() {
    hash.clear();
    System.gc();
  }

// Private data.

  protected Locus[] maxCardinality(Graph<Locus> g) {
    Locus[] v = (Locus[]) g.getVertices().toArray(new Locus[0]);
    Set<Locus>[] l = (Set<Locus>[]) new Set[v.length];
    for (int i = 0; i < l.length; i++) {
      l[i] = new LinkedHashSet<Locus>();
      v[i].reset();
      l[0].add(v[i]);
    }

    for (int i = 0, j = 0; i < v.length; i++) {
      Locus x = l[j].iterator().next();
      l[j].remove(x);

      Locus[] n = (Locus[]) g.getNeighbours(x).toArray(new Locus[0]);
      for (int k = 0; k < n.length; k++) {
        if (n[k].getDone()) {
          for (Iterator<Locus> it = x.getInvol().iterator(); it.hasNext(); ) {
            if (!g.connects(n[k], it.next())) {
              return null;
            }
          }
          x.getInvol().add(n[k]);
        }
        else {
          int cc = n[k].getCount();
          l[cc].remove(n[k]);
          cc++;
          n[k].setCount(cc);
          l[cc].add(n[k]);
        }
      }

      v[i] = x;
      x.setDone(true);

      if (++j == l.length) {
        j--;
      }
      for (; j >= 0 && l[j].isEmpty(); j--) {
        ;
      }
    }

    return v;
  }

  protected LogLikelihood calc(Set<Locus> s) {
    LogLikelihood res = null;
    if (hash != null) {
      res = hash.get(s);
    }

    if (res == null) {
      int[] index = new int[s.size()];
      Locus[] obs = (Locus[]) s.toArray(new Locus[0]);
      double df = 1;

      // calc degrees of freedom
      for (int i = 0; i < obs.length; i++) {
        index[i] = obs[i].getIndex();
        df *= snpd.nAlleles(index[i]);
      }
      df -= 1;

      // calc log likelihood.
      LinkedHashMap<IntArray, DoubleValue> hh = new LinkedHashMap<IntArray, DoubleValue>();
      for (int i = 0; i < snpd.nHaplotypes(); i++) {
        int[] xx = new int[index.length];
        for (int j = 0; j < xx.length; j++) {
          xx[j] = snpd.getAllele(i, index[j]);
        }

        IntArray a = new IntArray(xx);
        DoubleValue cc = hh.get(a);
        if (cc == null) {
          cc = new DoubleValue(0);
          hh.put(a, cc);
        }

        cc.x += 1;
      }

      double ll = 0;
      double nn = 0;
      for (Iterator<DoubleValue> e = hh.values().iterator(); e.hasNext(); ) {
        DoubleValue c = e.next();
        ll += c.x * Math.log(c.x);
        nn += c.x;
      }

      res = new LogLikelihood(ll - nn * Math.log(nn), df);
      if (hash != null) {
        hash.put(s, res);
      }
    }

    return res;
  }
}
