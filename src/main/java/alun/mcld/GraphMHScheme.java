package alun.mcld;

import alun.graph.Graph;

public interface GraphMHScheme {
  public Graph<Locus> getGraph();

  public void initialize();

  public void propose();

  public void reject();

  public void accept();
}
