package alun.mcld;

import alun.util.InputFormatter;
import alun.util.StringFormatter;

import java.io.IOException;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.Vector;

public class PerfectHaplotypes implements HaplotypeSource {
  protected LinkedHashSet<Locus> locs = null;
  protected Vector<int[]> v = null;

  public PerfectHaplotypes() {
  }

  public PerfectHaplotypes(InputFormatter f) throws IOException {
    v = new Vector<int[]>();
    while (f.newLine()) {
      int[] t = new int[f.itemsLeftOnLine()];
      for (int j = 0; j < t.length; j++) {
        f.newToken();
        //t[j] = f.getInt() - 1;
        t[j] = f.getInt();
      }
      v.addElement(t);
    }

    Set<Integer>[] s = (Set<Integer>[]) new Set[v.get(0).length];
    for (int i = 0; i < s.length; i++) {
      s[i] = new LinkedHashSet<Integer>();
    }

    for (int i = 0; i < v.size(); i++) {
      for (int j = 0; j < v.get(i).length; j++) {
        if (v.get(i)[j] >= 0) {
          s[j].add(new Integer(v.get(i)[j]));
        }
      }
    }

    locs = new LinkedHashSet<Locus>();
    for (int i = 0; i < s.length; i++) {
      if (s[i].size() < 2) {
        locs.add(new Locus(i, 2, (1.0 + i)));
      }
      else {
        locs.add(new Locus(i, s[i].size(), (1.0 + i)));
      }
    }
  }

  /**
   * Test main.
   */
  public static void main(String[] args) {
    try {
      System.out.println(new PerfectHaplotypes(new InputFormatter()));
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public Set<Locus> getLoci() {
    return locs;
  }

  public int nHaplotypes() {
    return v.size();
  }

  public int nLoci() {
    return locs.size();
  }

  public int nAlleles(int i) {
    Vector<Locus> x = new Vector<Locus>(locs);
    return x.get(i).getNStates();
  }

  public int[] getHaplotype(int i) {
    return v.get(i);
  }

  public int getAllele(int i, int j) {
    return v.get(i)[j];
  }

// Private data and methods.

  public boolean update(LDModel m) {
    return false;
  }

  public boolean maximize(LDModel m) {
    return false;
  }

  public String toString() {
    StringBuffer s = new StringBuffer();
    for (int i = 0; i < v.size(); i++) {
      int[] t = v.get(i);
      for (int j = 0; j < t.length; j++)
      //	s.append(StringFormatter.format((1+t[j]),2));
      {
        s.append(StringFormatter.format(t[j], 2));
      }
      s.append("\n");
    }

    if (v.size() > 0) {
      s.deleteCharAt(s.length() - 1);
    }

    return s.toString();
  }
}
