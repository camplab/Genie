package alun.mcld;

import alun.genepi.Allele;
import alun.genepi.Error;
import alun.genepi.ErrorPrior;
import alun.markov.GraphicalModel;
import alun.markov.Product;
import alun.util.InputFormatter;
import alun.util.StringFormatter;

import java.io.IOException;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.TreeSet;
import java.util.Vector;

public class Haplotypes extends PerfectHaplotypes {
  private Vector<int[]> u = null;
  private Vector<Observation> obs = null;
  private Product p = null;
  private Product ldprod = null;

  public Haplotypes(InputFormatter f, double errprob) throws IOException {
//		super(f);

// constructor copies here.

    v = new Vector<int[]>();
    while (f.newLine()) {
      int[] t = new int[f.itemsLeftOnLine()];
      for (int j = 0; j < t.length; j++) {
        f.newToken();
        t[j] = f.getInt() - 1;
      }
      v.addElement(t);
    }

    Set<Integer>[] ss = (Set<Integer>[]) new Set[v.get(0).length];
    for (int i = 0; i < ss.length; i++) {
      ss[i] = new LinkedHashSet<Integer>();
    }

    for (int i = 0; i < v.size(); i++) {
      for (int j = 0; j < v.get(i).length; j++) {
        if (v.get(i)[j] >= 0) {
          ss[j].add(new Integer(v.get(i)[j]));
        }
      }
    }

    locs = new LinkedHashSet<Locus>();
    for (int i = 0; i < ss.length; i++) {
      if (ss[i].size() < 2) {
        locs.add(new Locus(i, 2, (1.0 + i)));
      }
      else {
        locs.add(new Locus(i, ss[i].size(), (1.0 + i)));
      }
    }

// constructor copied here.

    u = v;
    v = new Vector<int[]>();
    for (int i = 0; i < u.size(); i++) {
      v.add(new int[u.get(i).length]);
    }

    p = new Product();
    obs = new Vector<Observation>();

    for (int j = 0; j < nLoci(); j++) {
      Set<Integer> s = new TreeSet<Integer>();
      for (int i = 0; i < u.size(); i++) {
        if (u.get(i)[j] >= 0) {
          s.add(new Integer(u.get(i)[j]));
        }
      }
      int k = 0;
      int[] pos = new int[nAlleles(j)];
      for (Integer i : s) {
        pos[k++] = i.intValue();
      }
      int kk = 0;
      while (k < pos.length)
        pos[k++] = --kk;

      Error e = new Error();
      p.add(new ErrorPrior(e, errprob));

      Allele a = new Allele(nAlleles(j));
      Observation o = new Observation(a, e, pos);
      p.add(o);
      obs.add(o);
    }

    update(null);
  }

// Private data and methods.

  public static void main(String[] args) {
    try {
      System.out.println(new Haplotypes(new InputFormatter(), 0.00));
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public boolean update(LDModel m) {
    return update(m, true);
  }

  public boolean maximize(LDModel m) {
    return update(m, false);
  }

  public boolean update(LDModel m, boolean sample) {
    Vector<Allele> alleles = new Vector<Allele>();
    for (int i = 0; i < obs.size(); i++) {
      alleles.add(obs.get(i).getAllele());
    }

    if (ldprod != null) {
      p.removeProduct(ldprod);
      ldprod = null;
    }

    if (m != null) {
      ldprod = m.replicate(alleles);
      p.addProduct(ldprod);
    }

    GraphicalModel g = new GraphicalModel(p);
    g.allocateOutputTables();
    g.allocateInvolTables();

    for (int i = 0; i < u.size(); i++) {
      for (int j = 0; j < u.get(i).length; j++) {
        obs.get(j).fix(u.get(i)[j]);
      }
      g.collect();

      if (sample) {
        g.drop();
      }
      else {
        g.max();
      }

      for (int j = 0; j < v.get(i).length; j++) {
        v.get(i)[j] = alleles.get(j).getState();
      }
    }

    g.clearOutputTables();
    g.clearInvolTables();

    return true;
  }

  public String toString() {
    StringBuffer s = new StringBuffer();
    for (int i = 0; i < v.size(); i++) {
      int[] t = v.get(i);
      for (int j = 0; j < t.length; j++) {
        s.append(StringFormatter.format((1 + t[j]), 2));
      }
      s.append("\n");
    }

    if (v.size() > 0) {
      s.deleteCharAt(s.length() - 1);
    }

    return s.toString();
  }
}
