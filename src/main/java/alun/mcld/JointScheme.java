package alun.mcld;

import alun.graph.Graph;
import alun.graph.Network;

import java.io.PrintWriter;

public class JointScheme {
  private HaplotypeSource gibbs = null;
  private GraphMHScheme metro = null;
  private LikelihoodCalculator calc = null;
  private Graph<Locus> current = null;
  private Network<Locus> best = null;
  private LogLikelihood bestll = null;
  private LogLikelihood curll = null;
  private double parampen = 1;

  public JointScheme(HaplotypeSource s, GraphMHScheme m) {
    this(s, m, 0.5 * Math.log(s.nHaplotypes()));
  }

  public JointScheme(HaplotypeSource s, GraphMHScheme m, double pen) {
    setParameterPenalty(pen);
    gibbs = s;
    calc = new LikelihoodCalculator(gibbs);

    metro = m;
    current = metro.getGraph();
    metro.initialize();

    curll = calc.calc(current);
    saveCurrent();
  }

  public GraphMHScheme getSearchScheme() {
    return metro;
  }

  public HaplotypeSource getHaplotypeSource() {
    return gibbs;
  }

  public LikelihoodCalculator getCalculator() {
    return calc;
  }

  public void reconstruct() {
    gibbs.maximize(calc.getLDModel(best, false));
  }

  public boolean gibbsUpdate() {
    return gibbsUpdate(1);
  }

  public boolean gibbsUpdate(double temp) {
    return gibbsUpdate(calc.getLDModel(current, temp > 0.00001));
/*
		if (gibbs.update(calc.getLDModel(current,temp > 0.00001)))
			calc.clear();
		curll = calc.calc(current);
		if (value(curll) > value(bestll))
		{
			saveCurrent();
			return true;
		}

		return false;
*/
  }

  public boolean gibbsUpdate(LDModel mod) {
    if (gibbs.update(mod)) {
      calc.clear();
    }
    curll = calc.calc(current);
    if (value(curll) > value(bestll)) {
      saveCurrent();
      return true;
    }
    return false;
  }

  public boolean metropolisUpdate() {
    return metropolisUpdate(1);
  }

// Private data

  public boolean metropolisUpdate(double temp) {
    metro.propose();
    LogLikelihood ll = calc.calc(current);

    if (ll == null || value(ll) < value(curll) - temp * rexp()) {
      metro.reject();
    }
    else {
      metro.accept();
      curll = ll;
      if (value(curll) > value(bestll)) {
        saveCurrent();
        return true;
      }
    }

    return false;
  }

  public double getParameterPenalty() {
    return parampen;
  }

  public void setParameterPenalty(double x) {
    parampen = x;
  }

  public double getBestScore() {
    return value(bestll);
  }

  public Graph<Locus> getBestGraph() {
    return best;
  }

  public Graph<Locus> getCurrentGraph() {
    return current;
  }

  public double getCurrentScore() {
    return value(curll);
  }

  public void addRemoveAnalysis(PrintWriter w) {
    Network<Locus> g = new Network<Locus>(best);
    Locus[] c = (Locus[]) g.getVertices().toArray(new Locus[0]);

    w.println("Scores for decomposable models  had by adding and removing single \nedges from final model.");
    w.println("\n");
    w.println("\t\t\t Score \t\t Log likelihood \t df");
    w.println("Final model");
    w.println("\t\t" + value(bestll) + "\t" + bestll.ll() + "\t" + bestll.df());
    w.println("Disconnecting edges");
    for (int i = 0; i < c.length; i++) {
      for (int j = 0; j < i; j++) {
        if (g.connects(c[i], c[j])) {
          g.disconnect(c[i], c[j]);
          LogLikelihood l = calc.calc(g);
          g.connect(c[i], c[j]);
          if (l != null) {
            w.println("\t(" + c[i] + "," + c[j] + ")\t" + value(l) + "\t" + l.ll() + "\t" + l.df());
          }
        }
      }
    }
    w.println("Connecting edges");
    for (int i = 0; i < c.length; i++) {
      for (int j = 0; j < i; j++) {
        if (!g.connects(c[i], c[j])) {
          g.connect(c[i], c[j]);
          LogLikelihood l = calc.calc(g);
          g.disconnect(c[i], c[j]);
          if (l != null) {
            w.println("\t(" + c[i] + "," + c[j] + ")\t" + value(l) + "\t" + l.ll() + "\t" + l.df());
          }
        }
      }
    }
  }

  private void saveCurrent() {
    best = new Network<Locus>(current);
    bestll = calc.calc(best);
  }

  private double rexp() {
    return -Math.log(Math.random());
  }

  private double value(LogLikelihood l) {
    return l.ll() - parampen * l.df();
  }
}
