package alun.mcld;

import java.util.Set;

public interface HaplotypeSource {
  public int nHaplotypes();

  public Set<Locus> getLoci();

  public int nLoci();

  public int nAlleles(int j);

  public int[] getHaplotype(int i);

  public int getAllele(int i, int j);

  public boolean update(LDModel m);

  public boolean maximize(LDModel m);
}
