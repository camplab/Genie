package alun.mcld;

import alun.graph.Graph;
import alun.util.IntervalTreeGraph;

import java.util.Collection;

public class IntervalSearch implements GraphMHScheme {
  protected IntervalTreeGraph<Locus> g = null;
  protected Locus[] loc = null;
  protected int x = 0;
  protected double mid = 0;
  protected double len = 0;

  public IntervalSearch(Collection<Locus> l) {
    double minoverlap = 0; //0.01;

    loc = new Locus[l.size()];
    int i = 0;
    for (Locus ll : l) {
      loc[i++] = ll;
    }

    double bot = loc[0].getPosition();
    double top = bot;
    for (i = 0; i < loc.length; i++) {
      if (bot > loc[i].getPosition()) {
        bot = loc[i].getPosition();
      }
      if (top < loc[i].getPosition()) {
        top = loc[i].getPosition();
      }
      loc[i].setLength(0.5);
      loc[i].setMiddle(loc[i].getPosition());
    }

    g = new IntervalTreeGraph<Locus>(bot - 1, top + 1, minoverlap);
    for (Locus ll : l) {
      g.add(ll);
    }
  }

// Privat data.

  public Graph<Locus> getGraph() {
    return g;
  }

  public void initialize() {
    for (int i = 0; i < loc.length; i++) {
      g.remove(loc[i]);
    }
    for (int i = 0; i < loc.length; i++) {
      loc[i].setMiddle(loc[i].getPosition());
      loc[i].setLength(0.5);
      g.add(loc[i]);
    }
  }

  public void propose() {
    x = (int) (Math.random() * loc.length);
    mid = loc[x].getMiddle();
    len = loc[x].getLength();

    g.remove(loc[x]);
    loc[x].setMiddle(g.lowerBound() + Math.random() * (g.upperBound() - g.lowerBound()));

    double nl = loc[x].getLength();
    if (Math.random() < 0.5) {
      nl += Math.log(Math.random());
    }
    else {
      nl -= Math.log(Math.random());
    }
    if (nl < 0) {
      nl = -nl;
    }
    loc[x].setLength(nl);
    g.add(loc[x]);
  }

  public void reject() {
    g.remove(loc[x]);
    loc[x].setMiddle(mid);
    loc[x].setLength(len);
    g.add(loc[x]);
  }

  public void accept() {
  }
}
