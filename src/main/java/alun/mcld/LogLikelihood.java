package alun.mcld;

public class LogLikelihood {
  private double ll = 0;
  private double df = 0;

  public LogLikelihood(double x, double i) {
    ll = x;
    df = i;
  }

  public final void add(LogLikelihood x) {
    ll += x.ll;
    df += x.df;
  }

  public final void subtract(LogLikelihood x) {
    ll -= x.ll;
    df -= x.df;
  }

  public final double ll() {
    return ll;
  }

// Private data.

  public final double df() {
    return df;
  }

  public String toString() {
    return ll + " " + df;
  }
}
