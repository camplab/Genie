package alun.graph;

import java.util.Collection;
import java.util.Set;

public interface Graph<V> {
  public boolean contains(Object x);

  public boolean connects(Object x, Object y);

  public Set<V> getVertices();

  public Collection<V> getNeighbours(Object x);
}
