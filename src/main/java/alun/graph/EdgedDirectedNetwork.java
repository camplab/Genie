package alun.graph;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;

public class EdgedDirectedNetwork<V, E> extends EdgedNetwork<V, E> implements DirectedGraph<V> {
  public EdgedDirectedNetwork() {
    f = new LinkedHashMap<V, Collection<V>>();
    b = new LinkedHashMap<V, Collection<V>>();
  }

  public Collection<V> getNeighbours(Object x) {
    LinkedHashSet<V> s = new LinkedHashSet<V>();
    s.addAll(f.get(x));
    s.addAll(b.get(x));
    return s;
  }
}
