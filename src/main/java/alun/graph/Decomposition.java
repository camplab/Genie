package alun.graph;

import java.util.*;

/**
 * A Decomposition is a directed graph of sets of vertices of
 * a given graph. Each set contains the vertices in a prime subgraph
 * of the given graph. Each set is connected forward to one other set such that
 * the intersection of the two sets is equal to the intersection of
 * the first set with all other sets later than it in the partial ordering.
 * If the given graph is triangulated then the sets will contain its maximal
 * cliques, and viewed as an undirected graph the Decomposition is a junction
 * tree.
 * The iterator of the vertex set obtained by a call to getVertices() iterates
 * over the prime subgraphs, or cliques, in an order such that running intersection
 * property holds.
 * This is an implementation of the lexicographic search method of Leimer (1992),
 * Discrete Mathematics, 113, 1-25.
 * In peeling terms, as per Thompson, Cannings and Skolnick (1978), iterating
 * over the set of vertices of the Decomposition gives the correctly ordered
 * "invol" sets for a peeling sequence.
 */

public class Decomposition<V> extends DirectedNetwork<LinkedHashSet<V>> {
  protected Decomposition() {
  }

  public Decomposition(Graph<V> h, Collection<V> kk) {
    Decomposition<V> d = new Decomposition<V>(h);

    ArrayList<LinkedHashSet<V>> v = new ArrayList<LinkedHashSet<V>>(d.getVertices());
    LinkedHashSet<V> keep = new LinkedHashSet<V>(kk);
    for (int i = 0; i < v.size(); i++) {
      if (d.next(v.get(i)) == null) {
        d.connect(v.get(i), keep);
      }
    }

    LinkedHashMap<LinkedHashSet<V>, LinkedHashSet<V>> otn = new LinkedHashMap<LinkedHashSet<V>, LinkedHashSet<V>>();
    for (Iterator<LinkedHashSet<V>> i = d.getVertices().iterator(); i.hasNext(); ) {
      LinkedHashSet<V> o = i.next();
      otn.put(o, new LinkedHashSet<V>(o));
    }

    keep = new LinkedHashSet<V>(kk);
    for (Iterator<LinkedHashSet<V>> i = d.getVertices().iterator(); i.hasNext(); ) {
      LinkedHashSet<V> o = i.next();
      for (Iterator<V> j = o.iterator(); j.hasNext(); ) {
        V k = j.next();
        if (keep.contains(k)) {
          for (LinkedHashSet<V> oo = o; oo != null; oo = d.next(oo)) {
            otn.get(oo).add(k);
          }
          //keep.remove(k);
        }
      }
    }

    for (Iterator<LinkedHashSet<V>> i = d.getVertices().iterator(); i.hasNext(); ) {
      add(otn.get(i.next()));
    }

    for (Iterator<LinkedHashSet<V>> i = d.getVertices().iterator(); i.hasNext(); ) {
      LinkedHashSet<V> o = i.next();
      LinkedHashSet<V> x = d.next(o);
      if (x != null) {
        connect(otn.get(o), otn.get(x));
      }
    }
  }

  public Decomposition(Graph<V> g) {
    // First perform a lexicographic depth first search.
    LinkedHashMap<V, IntList> h = lexSearch(g);

    // Then get from the lexicographic search the sets of 
    // vertices corresponding to each vertex elimination and put these
    // sets into a directed network.
    DirectedNetwork<LinkedHashSet<V>> net = new DirectedNetwork<LinkedHashSet<V>>();
    ArrayList<V> u = new ArrayList<V>(h.keySet());

    int[] t = new int[u.size()];
    int nt = 0;
    t[nt++] = u.size() - 1;
    for (int i = u.size() - 2; i >= 0; i--) {
      if (h.get(u.get(i + 1)).compareTo(h.get(u.get(i))) >= 0) {
        t[nt++] = i;
      }
    }

    int[] f = new int[nt];
    int[] e = new int[nt];
    e[nt - 1] = 0;
    f[nt - 1] = t[nt - 1];
    for (int i = e.length - 2; i >= 0; i--) {
      f[i] = t[i];
      e[i] = f[i + 1] + 1;
    }

    ArrayList<LinkedHashSet<V>> invol = new ArrayList<LinkedHashSet<V>>();
    ArrayList<LinkedHashSet<V>> cuts = new ArrayList<LinkedHashSet<V>>();
    for (int i = 0; i < f.length; i++) {
      invol.add(new LinkedHashSet<V>());
      cuts.add(new LinkedHashSet<V>());

      int[] x = h.get(u.get(f[i])).asArray();
      for (int j = 0; j < x.length; j++) {
        cuts.get(i).add(u.get(x[j]));
      }

      for (int j = e[i]; j <= f[i]; j++) {
        invol.get(i).add(u.get(j));
      }

      invol.get(i).addAll(cuts.get(i));

      net.add(invol.get(i));
      if (!cuts.get(i).isEmpty()) {
        for (int j = 0; j < i; j++) {
          if (invol.get(j).containsAll(cuts.get(i))) {
            net.connect(invol.get(i), invol.get(j));
            break;
          }
        }
      }
    }

    // This step merges the sets found above so that the merged sets
    // contain the prime subgraphs.
    for (int i = 0; i < f.length; i++) {
      if (isClique(g, cuts.get(i))) {
        continue;
      }

      LinkedHashSet<V> next = net.next(invol.get(i));
      LinkedHashSet<V> both = new LinkedHashSet<V>();
      both.addAll(invol.get(i));
      both.addAll(next);
      net.add(both);

      for (Iterator<LinkedHashSet<V>> j = net.outNeighbours(next).iterator(); j.hasNext(); ) {
        net.connect(both, j.next());
      }
      for (Iterator<LinkedHashSet<V>> j = net.inNeighbours(next).iterator(); j.hasNext(); ) {
        net.connect(j.next(), both);
      }
      for (Iterator<LinkedHashSet<V>> j = net.inNeighbours(invol.get(i)).iterator(); j.hasNext(); ) {
        net.connect(j.next(), both);
      }

      net.remove(invol.get(i));
      net.remove(next);
    }

    // The final stage is to put the sets into the directed network so that the
    // order has the running intersection property.
    // First add the sets.
    for (Iterator<LinkedHashSet<V>> i = net.getVertices().iterator(); i.hasNext(); ) {
      LinkedHashSet<V> cur = i.next();
      do {
        if (net.next(cur) == null) {
          break;
        }
        else {
          cur = net.next(cur);
        }
      }
      while (true);

      insertAdd(cur, net);
    }

    // The add the connections between the sets.
    for (Iterator<LinkedHashSet<V>> i = net.getVertices().iterator(); i.hasNext(); ) {
      LinkedHashSet<V> cur = i.next();
      if (net.next(cur) != null) {
        connect(cur, net.next(cur));
      }
    }
  }

  /**
   * Test main.
   */
  public static void main(String[] args) {
    try {
/*
			Network<String> nn = Network.read(new java.io.BufferedReader(new java.io.InputStreamReader(System.in)));
			Decomposition<String> dd = new Decomposition<String>(nn);
			System.out.println(dd);
*/
/*
			IntList a = new IntList();
			IntList b = new IntList();
			IntList c = new IntList();
			IntList d = new IntList();
			
			for (int i=0; i<8*17; i+=17)
			{
				a.append(i);
				b.append(i%5);
				if (i < 50)
					c.append(i%7);
				d.append(i%7);
			}

			//java.util.Collection s = new java.util.TreeSet();
			java.util.PriorityQueue s = new java.util.PriorityQueue();
			s.add(a);
			s.add(b);
			s.add(c);
			s.add(d);
		
			for (Object x = s.poll(); x != null; x = s.poll())
				System.out.println(x);

			System.out.println(a);
			System.out.println(b);
			System.out.println(a.compareTo(b));
*/

      Integer[] x = new Integer[50];
      for (int i = 0; i < x.length; i++) {
        x[i] = new Integer(i);
      }

      Network<Integer> g = new Network<Integer>();
      g.connect(x[1], x[2]);
      g.connect(x[2], x[3]);
      g.connect(x[3], x[1]);
      g.connect(x[3], x[4]);
      g.connect(x[4], x[5]);
      g.connect(x[6], x[6]);

      Decomposition<Integer> d = new Decomposition<Integer>(g);

      LinkedHashMap<Integer, IntList> h = d.lexSearch(g);
      for (Integer v : h.keySet()) {
        System.out.println(v + " \t" + h.get(v));
      }

      System.out.println();

      for (Set<Integer> s : d.getVertices()) {
        for (Integer i : s) {
          System.out.print(" " + i);
        }
        System.out.print("\t:\t");
        Set<Integer> t = d.next(s);
        if (t != null) {
          for (Integer i : t) {
            System.out.print(" " + i);
          }
        }
        System.out.println();
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  protected void insertAdd(LinkedHashSet<V> x, DirectedNetwork<LinkedHashSet<V>> net) {
    if (!contains(x)) {
      for (Iterator<LinkedHashSet<V>> j = net.inNeighbours(x).iterator(); j.hasNext(); ) {
        insertAdd(j.next(), net);
      }
      add(x);
    }
  }

  private boolean isClique(Graph<V> g, LinkedHashSet<V> xx) {
    ArrayList<V> x = new ArrayList<V>(xx);
    for (int i = 0; i < x.size(); i++) {
      for (int j = 0; j < i; j++) {
        if (!g.connects(x.get(i), x.get(j))) {
          return false;
        }
      }
    }
    return true;
  }

  /**
   * This method performs a lexicographic depth first search on the vertices of the graph
   * and returns a map linking each vertex to its label.
   * The order of of the vertices given by the iterator of the key set of the map
   * corresponds to the order found by the search.
   */

  protected LinkedHashMap<V, IntList> lexSearch(Graph<V> g) {
    ArrayList<V> u = new ArrayList<V>(g.getVertices());
    LinkedHashMap<V, IntList> hh = new LinkedHashMap<V, IntList>();
    for (int i = 0; i < u.size(); i++) {
      hh.put(u.get(i), new IntList());
    }
    LinkedHashSet<V> done = new LinkedHashSet<V>();

    for (int i = u.size() - 1; i > 0; i--) {
      done.add(u.get(i));
      LinkedHashSet<V> reached = new LinkedHashSet<V>();

      ArrayList<V> v = new ArrayList<V>();
      LinkedHashMap<V, IntList> h = new LinkedHashMap<V, IntList>();

      for (Iterator<V> j = g.getNeighbours(u.get(i)).iterator(); j.hasNext(); ) {
        V n = j.next();
        if (!done.contains(n)) {
          hh.get(n).append(i);
          reached.add(n);
          h.put(n, hh.get(n));
          insert(n, v, h);
        }
      }

      for (int j = 0; j < v.size(); j++) {
        V w = v.get(j);
        for (Iterator<V> k = g.getNeighbours(w).iterator(); k.hasNext(); ) {
          V z = k.next();
          if (!done.contains(z) && !reached.contains(z)) {
            if (h.get(w).compareTo(hh.get(z)) < 0) {
              hh.get(z).append(i);
              h.put(z, hh.get(z));
            }
            else {
              h.put(z, h.get(w));
            }
            reached.add(z);
            insert(z, v, h);
          }
        }
      }

      for (int j = 0; j < i; j++) {
        for (int k = j; k > 0 && hh.get(u.get(k)).compareTo(hh.get(u.get(k - 1))) < 0; k--) {
          V x = u.get(k);
          u.set(k, u.get(k - 1));
          u.set(k - 1, x);
        }
      }
    }

    LinkedHashMap<V, IntList> hhh = new LinkedHashMap<V, IntList>();
    for (int i = 0; i < u.size(); i++) {
      hhh.put(u.get(i), hh.get(u.get(i)));
    }

    return hhh;
  }

  private void insert(V x, ArrayList<V> v, LinkedHashMap<V, IntList> h) {
    int i = 0;
    while (i < v.size() && h.get(x).compareTo(h.get(v.get(i))) >= 0)
      i++;
    v.add(i, x);
  }
}

class IntList implements Comparable<IntList> {
  private Link head = null;
  private Link tail = null;

  public IntList() {
    head = null;
    tail = null;
  }

  public void append(int i) {
    Link l = new Link(i);

    if (head == null) {
      head = l;
      tail = l;
    }
    else {
      tail.next = l;
      tail = l;
    }
  }

  public int size() {
    int i = 0;
    for (Link l = head; l != null; l = l.next) {
      i++;
    }
    return i;
  }

  public int[] asArray() {
    int[] x = new int[size()];
    int i = 0;
    for (Link l = head; l != null; l = l.next) {
      x[i++] = l.i;
    }
    return x;
  }

  public String toString() {
    StringBuffer s = new StringBuffer();
    int[] x = asArray();
    for (int i = 0; i < x.length; i++) {
      s.append(" " + x[i]);
    }
    return s.toString();
  }

  public int compareTo(IntList l) {
    Link i = head;
    Link j = l.head;

    do {
      if (i == null && j == null) {
        return 0;
      }
      if (i == null) {
        return -1;
      }
      if (j == null) {
        return 1;
      }
      if (j.i < i.i) {
        return 1;
      }
      if (i.i < j.i) {
        return -1;
      }
      j = j.next;
      i = i.next;
    } while (true);
  }

  private class Link {
    public Link next = null;
    public int i = 0;
    public Link(int ii) {
      i = ii;
    }
  }
}
