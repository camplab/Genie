package alun.graph;

public interface MutableEdgedGraph<V, E> extends MutableGraph<V>, EdgedGraph<V, E> {
  public void connect(V u, V v, E e);
}
