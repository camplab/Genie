package alun.graph;

import java.util.Collection;

public interface EdgedGraph<V, E> extends Graph<V> {
  public E connection(Object u, Object v);

  public Collection<E> getEdges();
}
