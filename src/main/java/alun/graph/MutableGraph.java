package alun.graph;

public interface MutableGraph<V> extends Graph<V> {
  public void clear();

  public void add(V v);

  public void remove(Object v);

  public void connect(V u, V v);

  public void disconnect(Object u, Object v);
}
