package alun.graph;

import java.util.Collection;

public interface DirectedGraph<E> extends Graph<E> {
  public Collection<E> inNeighbours(Object x);

  public Collection<E> outNeighbours(Object x);
}
