package alun.graph;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;

public class EdgedNetwork<V, E> extends Network<V> implements MutableEdgedGraph<V, E> {
  public EdgedNetwork() {
    f = new LinkedHashMap<V, Collection<V>>();
    b = f;
  }

  public EdgedNetwork(EdgedGraph<? extends V, ? extends E> g) {
    this();
    GraphFunction.unite(this, g);
  }

  public Collection<V> makeCollection() {
    return new MapAsCollection<V, E>();
  }

  public E connection(Object u, Object v) {
    MapAsCollection<V, E> m = (MapAsCollection<V, E>) f.get(u);
    if (m == null) {
      return null;
    }
    return m.get(v);
  }

  public void connect(V x, V y, E e) {
    if (!contains(x)) {
      add(x);
    }
    if (!contains(y)) {
      add(y);
    }

    ((MapAsCollection<V, E>) f.get(x)).put(y, e);
    ((MapAsCollection<V, E>) b.get(y)).put(x, e);
  }

  public Collection<E> getEdges() {
    Collection<E> s = new LinkedHashSet<E>();
    for (V v : f.keySet()) {
      s.addAll(((MapAsCollection) f.get(v)).values());
    }
    return s;
  }
}
