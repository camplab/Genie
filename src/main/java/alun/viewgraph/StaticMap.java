package alun.viewgraph;

import alun.graph.*;

import java.awt.*;
import java.util.*;

public class StaticMap<V> extends Map<V> {
  private MutableEdgedGraph<Mappable, Line> hide = null;
  private MaskedNetwork<Mappable, Line> show = null;
  private Color edgecol = Color.black;

  public StaticMap(MutableEdgedGraph<Mappable, Line> h) {
    super(null);
    hide = h;
    show = new MaskedNetwork<Mappable, Line>(hide);
  }

  public StaticMap(Graph<V> gg) {
    super(gg);

    hide = new EdgedNetwork<Mappable, Line>();

    for (Iterator<V> i = gg.getVertices().iterator(); i.hasNext(); ) {
      hide.add(om.get(i.next()));
    }

    for (Iterator<V> i = gg.getVertices().iterator(); i.hasNext(); ) {
      V x = i.next();
      Mappable mx = om.get(x);
      for (Iterator<V> j = gg.getNeighbours(x).iterator(); j.hasNext(); ) {
        V y = j.next();
        Mappable my = om.get(y);
        if (gg.connects(x, y) && !hide.connects(mx, my)) {
          hide.connect(mx, my, new Line(mx, my, edgecol));
        }
      }
    }

    show = new MaskedNetwork<Mappable, Line>(hide);
  }

  public void paint(Graphics g) {
    super.paint(g);
    for (Iterator<Line> i = show.getEdges().iterator(); i.hasNext(); ) {
      i.next().paint(g);
    }
    for (Iterator<Mappable> i = show.getVertices().iterator(); i.hasNext(); ) {
      i.next().paint(g);
    }
  }

  public Mappable getShowing(double x, double y) {
    ArrayList<Mappable> a = new ArrayList<Mappable>(show.getVertices());
    for (int i = a.size() - 1; i >= 0; i--) {
      if (a.get(i).contains(x, y)) {
        return a.get(i);
      }
    }
    return null;
  }

  public void show(Mappable x) {
    show.show(x);
  }

  public void show(Collection<? extends Mappable> x) {
    show.show(x);
  }

  public void hide(Mappable x) {
    show.hide(x);
  }

  public void hide(Collection<? extends Mappable> x) {
    show.hide(x);
  }

  public Set<Mappable> getShownVertices() {
    return show.getVertices();
  }

  public Set<Mappable> getShownNeighbours(Mappable x) {
    return show.getNeighbours(x);
  }

  public Set<Mappable> getShownOutNeighbours(Mappable x) {
    Set<Mappable> s = new LinkedHashSet<Mappable>(getShownNeighbours(x));
    for (Iterator<Mappable> i = getShownNeighbours(x).iterator(); i.hasNext(); ) {
      Mappable y = i.next();
      if (!show.connects(x, y)) {
        s.remove(y);
      }
    }
    return s;
  }

  public Set<Mappable> getShownInNeighbours(Mappable x) {
    Set<Mappable> s = new LinkedHashSet<Mappable>(getShownNeighbours(x));
    for (Iterator<Mappable> i = getShownNeighbours(x).iterator(); i.hasNext(); ) {
      Mappable y = i.next();
      if (!show.connects(y, x)) {
        s.remove(y);
      }
    }
    return s;
  }

  public Set<Mappable> getShownComponent(Mappable x) {
    return GraphFunction.reachables(show, x);
  }

// Private data.

  public Set<Mappable> getAllVertices() {
    return hide.getVertices();
  }

  public Collection<Mappable> getAllNeighbours(Mappable x) {
    return hide.getNeighbours(x);
  }

  public Set<Mappable> getAllComponent(Mappable x) {
    return GraphFunction.reachables(hide, x);
  }
}
