package alun.viewgraph;

import alun.graph.Graph;

import java.awt.*;
import java.util.*;

abstract public class Map<V> implements MappableGraph {
  protected Graph<V> g = null;
  protected LinkedHashMap<V, Mappable> om = null;
  protected LinkedHashMap<Mappable, V> mo = null;
  protected Font font = null;

  public Map() {
    om = new LinkedHashMap<V, Mappable>();
    mo = new LinkedHashMap<Mappable, V>();
  }

// Private data.

  public Map(Graph<V> gg) {
    this();
    g = gg;

    if (g != null) {
      for (V o : g.getVertices()) {
        Mappable m = makeRep(o);
        om.put(o, m);
        mo.put(m, o);
      }
    }
  }

  public Mappable makeRep(V a) {
    LabelledBlob b = new LabelledBlob(a.toString());
    b.setSize(16, 16);
    b.setColor(Color.yellow);
    b.setTextColor(Color.black);
    b.setShowText(true);
    b.setShape(LabelledBlob.RECTANGLE);
    return b;
  }

  public void setFontSize(int k) {
    font = new Font(null, Font.BOLD, k);
  }

  public void paint(Graphics g) {
    if (font != null) {
      g.setFont(font);
    }
  }

  protected Collection<Mappable> reps(Collection<V> o) {
    LinkedHashSet<Mappable> s = new LinkedHashSet<Mappable>();
    for (Iterator i = o.iterator(); i.hasNext(); ) {
      s.add(om.get(i.next()));
    }
    s.remove(null);
    return s;
  }

  protected Set<V> obs(Set<Mappable> m) {
    LinkedHashSet<V> s = new LinkedHashSet<V>();
    for (Iterator i = m.iterator(); i.hasNext(); ) {
      s.add(mo.get(i.next()));
    }
    s.remove(null);
    return s;
  }
}
