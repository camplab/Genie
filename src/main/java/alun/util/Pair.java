package alun.util;

public class Pair<E, F> {
  public E x = null;
  public F y = null;

  public Pair() {
  }
  public Pair(E a, F b) {
    x = a;
    y = b;
  }
}
