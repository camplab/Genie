package alun.util;

public interface CartesianPoint {
  public double getX();

  public void setX(double x);

  public double getY();

  public void setY(double y);
}
