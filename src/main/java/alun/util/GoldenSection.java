package alun.util;

public class GoldenSection {
  public static double gold = 0.61803390;

  public static XYPair maximum(Curve p, double lo, double hi, double tol) {
    return golden(p, lo, lo + (1 - gold) * (hi - lo), hi, tol);
  }

  public static XYPair golden(Curve p, double lo, double mid, double hi, double tol) {
    XYPair r = new XYPair(lo, p.f(lo));
    XYPair s = null;
    XYPair t = null;
    XYPair u = new XYPair(hi, p.f(hi));

    if (Math.abs(mid - lo) < Math.abs(hi - mid)) {
      s = new XYPair(mid, p.f(mid));
      double x = mid + (hi - mid) * (1 - gold);
      t = new XYPair(x, p.f(x));
    }
    else {
      double x = lo + (mid - lo) * (1 - gold);
      s = new XYPair(x, p.f(x));
      t = new XYPair(mid, p.f(mid));
    }

    return golden(p, r, s, t, u, tol);
  }

  public static XYPair golden(Curve p, XYPair r, XYPair s, XYPair t, XYPair u, double tol) {
    while (Math.abs(r.x - u.x) > tol) {
      if (t.y > s.y) {
        r = s;
        s = t;
        double x = gold * s.x + (1 - gold) * u.x;
        t = new XYPair(x, p.f(x));
      }
      else {
        u = t;
        t = s;
        double x = (1 - gold) * r.x + gold * t.x;
        s = new XYPair(x, p.f(x));
      }
    }

    XYPair z = r;
    if (s.y > z.y) {
      z = s;
    }
    if (t.y > z.y) {
      z = t;
    }
    if (u.y > z.y) {
      z = u;
    }
    return z;
  }

  public static void main(String[] args) {
    try {
      System.out.println(GoldenSection.maximum(new TestCurve(), 0, 1, 0.001));
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}

class XYPair implements CartesianPoint {
  public double x = 0;
  public double y = 0;

  public XYPair(double a, double b) {
    x = a;
    y = b;
  }

  public XYPair() {
  }

  public String toString() {
    return x + " " + y;
  }

  public double getX() {
    return x;
  }

  public void setX(double d) {
    x = d;
  }

  public double getY() {
    return y;
  }

  public void setY(double d) {
    y = d;
  }
}

class TestCurve implements Curve {
  int count = 0;

  public double f(double x) {
    System.err.println((++count) + " " + x);
    return x * x * (1 - x) * (1 - x) * (1 - x) * (1 - x) * (1 - x) * (1 - x) * (1 - x) * (1 - x);
    //return x*x;
  }
}
