package alun.util;

public interface Interval {
  public double getLength();

  public void setLength(double x);

  public double getMiddle();

  public void setMiddle(double x);

  public double getRight();

  public void setRight(double r);

  public double getLeft();

  public void setLeft(double l);

  public boolean intersects(double p);

  public boolean intersects(Interval i);

  public double intersection(Interval i);
}
