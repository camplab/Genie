package alun.genepi;

import alun.markov.GraphicalModel;

import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

abstract public class AuxiliaryVariable extends GeneticVariable {
  protected LinkedHashSet<Inheritance> pats = null;
  protected LinkedHashSet<Inheritance> mats = null;
  private LocusVariables lig = null;

  public AuxiliaryVariable(int i, LocusVariables ligs) {
    super(i);
    lig = ligs;
  }

  abstract public void set();

  public GraphicalModel getBlock() {
    return new GraphicalModel(lig.fixedInheritancesProduct());
  }

  public void init() {
    for (Iterator<Inheritance> i = pats.iterator(); i.hasNext(); ) {
      i.next().remember();
    }
    for (Iterator<Inheritance> i = mats.iterator(); i.hasNext(); ) {
      i.next().remember();
    }
    super.init();
  }

// Protected data and methods.

  public boolean next() {
    if (!super.next()) {
      return false;
    }
    set();
    return true;
  }

  public LinkedHashSet<Inheritance> getPats() {
    return pats;
  }

  public LinkedHashSet<Inheritance> getMats() {
    return mats;
  }

  protected void flip(Set<Inheritance> s) {
    for (Iterator<Inheritance> i = s.iterator(); i.hasNext(); ) {
      i.next().flip();
    }
  }
}
