package alun.genepi;

import alun.genio.GeneticDataSource;
import alun.genio.LinkageDataSet;
import alun.genio.LinkageInterface;
import alun.markov.GraphicalModel;
import alun.markov.Product;
import alun.util.StringFormatter;

public class TwoPoints {
  private LociVariables l = null;
  private Theta theta = null;

  public TwoPoints(GeneticDataSource d, double[] thetas) {
    l = new LociVariables(d);
    theta = new Theta(thetas);
  }

// Private data.

  public static void main(String[] args) {
    try {
      double[] thetas = {0.0, 0.01, 0.1, 0.2, 0.3, 0.4, 0.5};
      LinkageInterface d = null;

      switch (args.length) {
        case 2:
          LinkageDataSet data = new LinkageDataSet(args[0], args[1]);
          data.downCode(false);
          d = new LinkageInterface(data);
          break;
        default:
          System.err.println("Usage: java Tlods " + "input.par input.ped [n_samples]");
          System.exit(1);
      }

      TwoPoints twos = new TwoPoints(d, thetas);

      for (int j = 1; j < d.nLoci(); j++) {
        double[] lik = twos.likelihood(j);
        double lhalf = lik[lik.length - 1];
        for (int i = 0; i < lik.length; i++) {
          System.out.print(" " + StringFormatter.format(Math.log(lik[i] / lhalf) / Math.log(10), 2, 4));
        }
        System.out.println();
      }
    } catch (Exception e) {
      System.err.println("Caught in Tlods:main()");
      e.printStackTrace();
    }
  }

  public double[] likelihood(int j) {
    Product p = l.unrelatedLocusProduct(j);
    GraphicalModel m = new GraphicalModel(p);
    m.allocateOutputTables();
    m.allocateInvolTables();
    m.reduceStates();
    m.clearInvolTables();
    m.clearOutputTables();

    p = l.locusProduct(j);
    m = new GenotypeModel(p);
    m.allocateOutputTables();
    m.allocateInvolTables();
    m.reduceStates();
    m.clearInvolTables();
    m.clearOutputTables();

    p = l.twoPointProduct(0, j, theta);
    p.triangulate();
    m = new GenotypeModel(p, theta);
    m.allocateOutputTables();
    m.peel();
    Product f = m.getFinals();
    m.clearOutputTables();

    double[] lik = new double[theta.getNStates()];
    for (theta.init(); theta.next(); ) {
      lik[theta.getState()] = f.getValue();
    }

    return lik;
  }

  public double[] log10Likelihood(int j) {
    Product p = l.unrelatedLocusProduct(j);
    GraphicalModel m = new GraphicalModel(p);
    m.allocateOutputTables();
    m.allocateInvolTables();
    m.reduceStates();
    m.clearInvolTables();
    m.clearOutputTables();

    p = l.locusProduct(j);
    m = new GenotypeModel(p);
    m.allocateOutputTables();
    m.allocateInvolTables();
    m.reduceStates();
    m.clearInvolTables();
    m.clearOutputTables();

    OneTheta t = new OneTheta();
    p = l.twoPointProduct(0, j, t);
    p.triangulate();
    m = new GenotypeModel(p, t);
    m.allocateOutputTables();

    double[] out = new double[theta.getNStates()];
    for (theta.init(); theta.next(); ) {
      t.fix(theta.getValue());
      out[theta.getState()] = m.log10Peel();
    }
    m.clearOutputTables();

    return out;
  }
}
