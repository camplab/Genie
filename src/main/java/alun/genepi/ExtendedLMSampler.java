package alun.genepi;

import alun.genio.LinkageDataSet;
import alun.genio.LinkageInterface;
import alun.markov.DenseGraphicalModel;
import alun.markov.GraphicalModel;

public class ExtendedLMSampler extends LocusMeiosisSampler {
  private static int[] defups = {4};
  private LinkageVariables vars = null;
  private int[] frame = null;
  private int[] randup = null;

  public ExtendedLMSampler(LinkageVariables vars) {
    this(vars, defups);
  }

// Private data.

  public ExtendedLMSampler(LinkageVariables l, int[] updates) {
    super(l);
    randup = updates;

    int n = 0;
    for (int i = 0; i < l.nIndividuals(); i++) {
      if (l.getInheritance(i, 1, 0) != null && l.getInheritance(i, 1, 1) != null) {
        n++;
      }
    }
    frame = new int[n];
    n = 0;
    for (int i = 0; i < l.nIndividuals(); i++) {
      if (l.getInheritance(i, 1, 0) != null && l.getInheritance(i, 1, 1) != null) {
        frame[n++] = i;
      }
    }

    vars = l;
  }

  /**
   * Test main.
   */
  public static void main(String[] args) {
    try {
      int runs = 10;
      LinkageSampler mc = null;

      switch (args.length) {
        case 2:
          LinkageDataSet data = new LinkageDataSet(args[0], args[1]);
          data.downCode(false);
          mc = new ExtendedLMSampler(new LinkageVariables(new LinkageInterface(data)));
          break;
        default:
          System.err.println("Usage: java LocusSampler " + "input.par input.ped");
          System.exit(1);
      }

      for (int i = 0; i < runs; i++) {
        mc.sample();
        System.out.print(".");
      }
      System.out.println();
    } catch (Exception e) {
      System.err.println("Caught in LocusMeiosisSampler:main()");
      e.printStackTrace();
    }
  }

  public void sample() {
    int size = 0;
    try {
      vars.save();
      for (int i = 0; i < randup.length; i++) {
        size = randup[i];
        sampleMeioses(randup[i]);
        System.gc();
      }
    } catch (OutOfMemoryError e) {
      System.err.println("Warning: failed to make random multi-meiosis update for " + size + " individuals.");
      System.err.println(e);
      System.err.println("Skipping this update.");
      vars.restore();
    }

    sample(getMeiosisBlocks());
    sample(getLocusBlocks());
  }

  public void sampleMeioses(int nind) {
    int nins = nind;
    if (nins > frame.length) {
      nins = frame.length;
    }

    int[] x = new int[nins];
    for (int i = 0; i < nins; i++) {
      int k = i + (int) ((frame.length - i) * Math.random());
      int temp = frame[i];
      frame[i] = frame[k];
      frame[k] = temp;
      x[i] = frame[i];
    }

    sampleMeioses(x);
  }

  public void sampleMeioses(int[] x) {
    if (x == null || x.length == 0) {
      return;
    }

    GraphicalModel m = new DenseGraphicalModel(vars.peeledSetOfMeiosesProduct(x));
    //GraphicalModel m = new DenseGraphicalModel(vars.droppedSetOfMeiosesProduct(x));
    //System.err.print("-");

    m.allocateOutputTables();
    m.allocateInvolTables();
    m.collect();
    m.drop();
    m.clearInvolTables();
    m.clearOutputTables();
  }
}
