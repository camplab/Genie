package alun.genepi;

import alun.genio.LinkageDataSet;
import alun.genio.LinkageInterface;
import alun.markov.GraphicalModel;

import java.util.Iterator;
import java.util.LinkedHashSet;

public class IndependentLocusSampler extends LinkageSampler {
  private LinkedHashSet<GraphicalModel> gs = null;
  private LinkedHashSet<GraphicalModel> gi = null;

  public IndependentLocusSampler(LociVariables lv) {
    this(lv, 1);
  }

  public IndependentLocusSampler(LociVariables lv, int first) {
    gs = new LinkedHashSet<GraphicalModel>();
    gi = new LinkedHashSet<GraphicalModel>();

    for (int j = first; j < lv.nLoci(); j++) {
      GraphicalModel h = new GenotypeModel(lv.unrelatedLocusProduct(j));
      h.allocateOutputTables();
      h.allocateInvolTables();
      h.reduceStates();

      h = new GenotypeModel(lv.locusProduct(j));
      h.allocateOutputTables();
      h.allocateInvolTables();
      h.reduceStates();
      h.allocateOutputTables();
      h.allocateInvolTables();
      h.collect();
      h.drop();
      h.clearOutputTables();
      h.clearInvolTables();

      gs.add(h);
      gi.add(h);
    }
  }

  /**
   * Test main.
   */
  public static void main(String[] args) {
    try {
      int runs = 10;
      IndependentLocusSampler mc = null;

      switch (args.length) {
        case 2:
          LinkageDataSet data = new LinkageDataSet(args[0], args[1]);
          data.downCode(false);
          mc = new IndependentLocusSampler(new LociVariables(new LinkageInterface(data)));
          break;
        default:
          System.err.println("Usage: java LocusSampler " + "input.par input.ped");
          System.exit(1);
      }

      for (int i = 0; i < runs; i++) {
        mc.sample();
        System.out.print(".");
      }
      System.out.println();
    } catch (Exception e) {
      System.err.println("Caught in IndependentLocusSampler:main()");
      e.printStackTrace();
    }
  }

  public void sample() {
    sample(gs);
  }

  public void sample(LinkedHashSet<GraphicalModel> g) {
    for (Iterator<GraphicalModel> i = g.iterator(); i.hasNext(); ) {
      GraphicalModel m = i.next();
      m.allocateOutputTables();
      m.allocateInvolTables();
      m.collect();
      m.drop();
      m.clearInvolTables();
      m.clearOutputTables();
    }
  }

  public void initialize() {
    sample(gi);
  }

  public LinkedHashSet<GraphicalModel> getLocusBlocks() {
    return gs;
  }

// Private data.

  public void setLocusBlocks(LinkedHashSet<GraphicalModel> bl) {
    gs = bl;
  }

  public LinkedHashSet<GraphicalModel> getInitialBlocks() {
    return gi;
  }

  public void setInitialBlocks(LinkedHashSet<GraphicalModel> inbl) {
    gi = inbl;
  }
}
