package alun.genepi;

import alun.genio.GeneticDataSource;
import alun.genio.LinkageDataSet;
import alun.genio.LinkageInterface;
import alun.markov.GraphicalModel;

public class ObligatoryErrors {
  public static void main(String[] args) {
    try {
      LinkageDataSet x = null;

      switch (args.length) {
        case 2:
          x = new LinkageDataSet(args[0], args[1]);
          x.downCode(false);
          break;
        default:
          System.err.println("Usage: java ObligatoryErrors parfile pedfile");
          System.exit(1);
      }

      LinkageDataSet[] xx = x.splitByPedigree();
      for (int i = 0; i < xx.length; i++) {
        GeneticDataSource d = new LinkageInterface(xx[i]);
        for (int j = 0; j < d.nLoci(); j++) {
          if (obligatoryError(d, j)) {
            System.out.println("Pedigree " + (1 + i) + " " + xx[i].name()
              + "\tlocus " + (1 + j) + " " + d.locName(j) + "\tobligatory error");
          }
        }
      }
    } catch (Exception e) {
      System.err.println("Caught in ObligatoryErrors:main().");
      e.printStackTrace();
    }
  }

  public static boolean obligatoryError(GeneticDataSource d, int j) {
    LocusVariables g = new LocusVariables(d, j);
    GraphicalModel m = null;

    if (d.nAlleles(j) > 2) {
      m = new GraphicalModel(g.makeUnrelatedProduct());
      m.allocateOutputTables();
      m.allocateInvolTables();
      m.reduceStates();
    }

    m = new GenotypeModel(g.makeLocusProduct());
    m.allocateOutputTables();
    //System.out.println(m.peel());
    return m.peel() <= 0;
  }
}
