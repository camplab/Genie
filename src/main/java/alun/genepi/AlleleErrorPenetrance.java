package alun.genepi;

import alun.markov.Variable;

import java.util.LinkedHashSet;

public class AlleleErrorPenetrance extends GeneticFunction {
  private Allele pat = null;
  private Allele mat = null;
  private Error err = null;
  private double[][] p = null;
  private double q = 0;

// Private data.

  public AlleleErrorPenetrance(Allele paternal, Allele maternal, Error e, double[][] penet) {
    pat = paternal;
    mat = maternal;
    err = e;
    fix(penet);
  }

  public LinkedHashSet<Variable> getVariables() {
    LinkedHashSet<Variable> s = new LinkedHashSet<Variable>();
    s.add(pat);
    s.add(mat);
    s.add(err);
    return s;
  }

  public double getValue() {
    if (p == null) {
      return 1;
    }
    return err.getState() == 0 ? p[pat.getState()][mat.getState()] : q;
  }

  public String toString() {
    return "APENET " + getVariables();
  }

  public void fix(double[][] penet) {
    p = penet;
    if (p != null) {
      q = 1.0 / (p.length * p.length);
    }
  }
}
