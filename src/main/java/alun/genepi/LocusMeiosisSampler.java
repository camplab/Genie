package alun.genepi;

import alun.genio.LinkageDataSet;
import alun.genio.LinkageInterface;
import alun.markov.GraphicalModel;
import alun.markov.Product;

import java.util.LinkedHashSet;

// Note that this class does not extend LinkedLocusSampler,
// makes the locus blocks itself.
//
public class LocusMeiosisSampler extends IndependentLocusSampler {
  private LinkedHashSet<GraphicalModel> mblocks = null;

  public LocusMeiosisSampler(LinkageVariables vars) {
    this(vars, 2);
  }

  public LocusMeiosisSampler(LinkageVariables vars, int level) {
    super(vars);

    setLocusBlocks(locusBlocks(vars));

    LinkedHashSet<GraphicalModel> s = new LinkedHashSet<GraphicalModel>();
    switch (level) {
      case 2:
        s.addAll(threeGenMeiosisBlocks(vars));
      case 1:
        s.addAll(familyMeiosisBlocks(vars));
      case 0:
      default:
        s.addAll(individualMeiosisBlocks(vars));
    }
    setMeiosisBlocks(s);
  }

  /**
   * Test main.
   */
  public static void main(String[] args) {
    try {
      int runs = 10;
      LinkageSampler mc = null;

      switch (args.length) {
        case 2:
          LinkageDataSet data = new LinkageDataSet(args[0], args[1]);
          data.downCode(false);
          mc = new LocusMeiosisSampler(new LinkageVariables(new LinkageInterface(data)));
          break;
        default:
          System.err.println("Usage: java LocusSampler " + "input.par input.ped");
          System.exit(1);
      }

      for (int i = 0; i < runs; i++) {
        mc.sample();
        System.out.print(".");
      }
      System.out.println();
    } catch (Exception e) {
      System.err.println("Caught in LocusMeiosisSampler:main()");
      e.printStackTrace();
    }
  }

  public void sample() {
    sample(getMeiosisBlocks());
    // Sample locus blocks last because other block updates leave genotypes
    // and error variables in indeterminate states, updating only inheritances.
    sample(getLocusBlocks());
  }

  protected LinkedHashSet<GraphicalModel> locusBlocks(LinkageVariables l) {
    LinkedHashSet<GraphicalModel> s = new LinkedHashSet<GraphicalModel>();
    s.add(new GenotypeModel(l.connectedLocusProduct(1, false, true)));
    for (int j = 2; j < l.nLoci(); j++) {
      s.add(new GenotypeModel(l.connectedLocusProduct(j, true, true)));
    }
    return s;
  }

  protected LinkedHashSet<GraphicalModel> individualMeiosisBlocks(LinkageVariables l) {
    LinkedHashSet<GraphicalModel> s = new LinkedHashSet<GraphicalModel>();
    for (int i = 0; i < l.nIndividuals(); i++) {
      if (l.getInheritance(i, 0, 0) != null && l.getInheritance(i, 0, 1) != null) {
        s.add(new AuxiliaryModel(l.individualMeiosisProduct(i)));
      }
    }
    return s;
  }

  protected LinkedHashSet<GraphicalModel> familyMeiosisBlocks(LinkageVariables l) {
    LinkedHashSet<GraphicalModel> s = new LinkedHashSet<GraphicalModel>();
    for (int i = 0; i < l.nFamilies(); i++) {
      Product p = l.familyMeiosisProduct(i);
      if (p != null) {
        s.add(new AuxiliaryModel(p));
      }
    }
    return s;
  }

  protected LinkedHashSet<GraphicalModel> threeGenMeiosisBlocks(LinkageVariables l) {
    LinkedHashSet<GraphicalModel> s = new LinkedHashSet<GraphicalModel>();
    for (int i = 0; i < l.nFamilies(); i++) {
      Product p = l.threeGenMeiosisProduct(i);
      if (p != null) {
        s.add(new AuxiliaryModel(p));
      }
    }
    return s;
  }

// Private data

  protected LinkedHashSet<GraphicalModel> getMeiosisBlocks() {
    return mblocks;
  }

  protected void setMeiosisBlocks(LinkedHashSet<GraphicalModel> g) {
    mblocks = g;
  }
}
