package alun.genepi;

abstract public class LinkageSampler {
  abstract public void initialize();

  abstract public void sample();
}
