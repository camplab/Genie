package alun.genepi;

import alun.genio.GeneticDataSource;
import alun.genio.LinkageDataSet;
import alun.genio.LinkageInterface;
import alun.markov.GraphicalModel;
import alun.markov.Product;
import alun.markov.Variable;
import alun.util.StringFormatter;

public class Tlods {
  private LociVariables lv = null;
  private Theta theta = null;
  private GraphicalModel twoptmod = null;

  public Tlods(LociVariables linkv, double[] t) {
    lv = linkv;
    theta = new Theta(t);
    Product tp = lv.traitProduct(theta);

    for (Variable v : tp.getVariables()) {
      if (v != theta) {
        tp.fillIn(v, theta);
      }
    }

    twoptmod = new GenotypeModel(tp, theta);
    twoptmod.allocateOutputTables();
  }

/*
	public double[] combine(double[][] x)
	{
		double thresh = 0.000001;
		double[] z = new double[x[0].length];
		for (int j=0; j<z.length; j++)
			z[j] = 1;
		
		for (double error = 1000; error > thresh; )
		{
			double[] y = new double[z.length];
			for (int i=0; i<x.length; i++)
			{
				double sum = 0;
				for (int j=0; j<z.length; j++)
					sum += x[i][j]/z[j];

				for (int j=0; j<y.length; j++)
					y[j] += x[i][j]/sum;
			}

			error = 0;
			for (int j=0; j<y.length; j++)
				error += Math.abs(y[j]-z[j]); 
			z = y;
		}

		return z;
	}
*/

// Private data.

  /**
   * Test main.
   */
  public static void main(String[] args) {
    try {
      double[] thetas = {0.0, 0.01, 0.1, 0.2, 0.3, 0.4, 0.5};
      int[] randups = {5};
      int n_samples = 50;
      boolean report = false;

      GeneticDataSource data = null;

      switch (args.length) {
        case 4:
          if (args[3].equals("-v")) {
            report = true;
          }
        case 3:
          n_samples = new Integer(args[2]).intValue();
        case 2:
          LinkageDataSet dt = new LinkageDataSet(args[0], args[1]);
          dt.downCode(false);
          data = new LinkageInterface(dt);
          break;
        default:
          System.err.println("Usage: java Tlods " + "input.par input.ped [n_samples]");
          System.exit(1);
      }

      LociVariables lv = new LociVariables(data);
      //LinkageSampler mc = new ExtendedLMSampler(lv,randups);
      LinkageSampler mc = new IndependentLocusSampler(lv);
      Tlods tl = new Tlods(lv, thetas);

      double[][] lods = new double[lv.nLoci()][tl.nThetas()];

      for (int i = 0; i < n_samples; i++) {
        mc.sample();

        for (int j = 1; j < lods.length; j++) {
          double[] lik = tl.twoPointLikelihood(j);
          double lhalf = lik[lik.length - 1];
          for (int k = 0; k < lik.length; k++) {
            lik[k] /= lhalf;
            lods[j][k] += lik[k];
          }

          if (report) {
            for (int k = 0; k < lods[j].length; k++) {
              System.out.print("\t" +
                StringFormatter.format(Math.log(lods[j][k] / (i + 1)) / Math.log(10), 2, 6));
            }
            System.out.println();
          }
        }
        if (report) {
          System.out.println();
        }
        System.err.print(".");
      }
      System.err.println();

      for (int j = 1; j < lods.length; j++) {
        for (int k = 0; k < lods[j].length; k++) {
          lods[j][k] = Math.log(lods[j][k] / n_samples) / Math.log(10);
          System.out.print(StringFormatter.format(lods[j][k], 2, 6) + " ");
        }
        System.out.println();
      }

      System.out.println();
    } catch (Exception e) {
      System.err.println("Caught in Tlods:main()");
      e.printStackTrace();
    }
  }

  public int nThetas() {
    return theta.getNStates();
  }

  public LociVariables getVariables() {
    return lv;
  }

  public double[] twoPointLikelihood(int j) {
    LociVariables lv = getVariables();
    for (int i = 0; i < lv.nIndividuals(); i++) {
      Inheritance h = null;
      h = lv.getInheritance(i, j, 0);
      if (h != null) {
        lv.recombination(i, 0).fix(h.getState());
      }
      h = lv.getInheritance(i, j, 1);
      if (h != null) {
        lv.recombination(i, 1).fix(h.getState());
      }
    }

    twoptmod.allocateOutputTables();
    twoptmod.peel();
    Product marg = twoptmod.getFinals();
    double[] lik = new double[nThetas()];
    for (theta.init(); theta.next(); ) {
      lik[theta.getState()] = marg.getValue();
    }
    twoptmod.clearOutputTables();

    return lik;
  }
}
