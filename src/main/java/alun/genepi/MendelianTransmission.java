package alun.genepi;

import alun.markov.Variable;

import java.util.LinkedHashSet;

public class MendelianTransmission extends GeneticFunction {
  protected Genotype x = null;
  protected Genotype pa = null;
  protected Genotype ma = null;

  public MendelianTransmission(Genotype bod, Genotype father, Genotype mother) {
    x = bod;
    pa = father;
    ma = mother;
  }

  public LinkedHashSet<Variable> getVariables() {
    LinkedHashSet<Variable> v = new LinkedHashSet<Variable>();
    v.add(x);
    v.add(pa);
    v.add(ma);
    return v;
  }

// Protected data.

  public double getValue() {
    return inherit(x.pat(), pa.pat(), pa.mat()) * inherit(x.mat(), ma.pat(), ma.mat());
  }

  public final double inherit(int a, int b, int c) {
    return a == b ? (a == c ? 1 : 0.5) : (a == c ? 0.5 : 0);
  }

  public String toString() {
    return "TRANS " + getVariables();
  }
}
