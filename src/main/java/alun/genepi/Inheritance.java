package alun.genepi;

public class Inheritance extends GeneticVariable {
  private int mem = 0;

  public Inheritance() {
    super(2);
    mem = 0;
  }

  public void remember() {
    mem = getState();
  }

  public void recall() {
    setState(mem);
  }

  public void flip() {
    setState(1 - getState());
  }
}
