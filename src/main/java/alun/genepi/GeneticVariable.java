package alun.genepi;

import alun.markov.MutableSpaceVariable;

public class GeneticVariable extends MutableSpaceVariable {
  private int memory = 0;

  public GeneticVariable(int n) {
    super(n);
  }

  public void save() {
    //memory = getState();
    memory = getStateIndex();
  }

  public void restore() {
    //setState(memory);
    setStateIndex(memory);
  }
}
