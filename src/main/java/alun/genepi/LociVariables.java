package alun.genepi;

import alun.genio.GeneticDataSource;
import alun.markov.Product;

public class LociVariables {
  protected GeneticDataSource d = null;
  protected LocusVariables[] loc = null;
  private VariableRecombination[] prec = null;
  private VariableRecombination[] mrec = null;
  private VariableRecombination[] precl = null;
  private VariableRecombination[] mrecl = null;
  private VariableRecombination[] precr = null;
  private VariableRecombination[] mrecr = null;

  public LociVariables(GeneticDataSource data, double errorpri) {
    d = data;

    loc = new LocusVariables[d.nLoci()];
    for (int i = 0; i < loc.length; i++) {
      loc[i] = new LocusVariables(d, i, errorpri);
    }
  }

  public LociVariables(GeneticDataSource data) {
    this(data, -1);
  }

  public Product locusProduct(int i) {
    return loc[i].makeInheritanceLocusProduct();
  }

  public Product unrelatedLocusProduct(int i) {
    return loc[i].makeUnrelatedProduct();
  }

  public Product twoPointProduct(int a, int b, Theta t) {
    Product p = new Product();
    p.add(locusProduct(a).getFunctions());
    p.add(locusProduct(b).getFunctions());

    for (int i = 0; i < loc[a].genotypes().length; i++) {
      if (getInheritance(i, a, 0) != null) {
        p.add(new Recombination(getInheritance(i, a, 0), getInheritance(i, b, 0), t));
      }
      if (getInheritance(i, a, 1) != null) {
        p.add(new Recombination(getInheritance(i, a, 1), getInheritance(i, b, 1), t));
      }
    }

    return p;
  }

  public LocusVariables[] getLocusVariables() {
    return loc;
  }

  public Inheritance getInheritance(int i, int j, int k) {
    Inheritance[] t = (k == 0 ? loc[j].patInheritances() : loc[j].matInheritances());
    if (t != null) {
      return t[i];
    }
    else {
      return null;
    }
  }

  public Error getError(int i, int j) {
    return loc[j].error(i);
  }

  public Genotype getGenotype(int i, int j) {
    return loc[j].genotypes()[i];
  }

  public boolean isFounder(int i) {
    return loc[0].isFounder(i);
  }

// Private data.

  public int nLoci() {
    return loc.length;
  }

  public int nAlleles(int j) {
    return d.nAlleles(j);
  }

  public int nIndividuals() {
    return loc[0].genotypes().length;
  }

  public int nFounders() {
    return loc[0].founderGenotypes().length;
  }

  public int nFamilies() {
    return d.nuclearFamilies().length;
  }

  public Product traitProduct(Theta theta) {
    Product p = locusProduct(0);
    if (prec == null) {
      prec = new VariableRecombination[loc[0].genotypes().length];
    }
    if (mrec == null) {
      mrec = new VariableRecombination[loc[0].genotypes().length];
    }

    for (int j = 0; j < prec.length; j++) {
      if (getInheritance(j, 0, 0) != null) {
        prec[j] = new VariableRecombination(getInheritance(j, 0, 0), theta);
        p.add(prec[j]);
      }
      if (getInheritance(j, 0, 1) != null) {
        mrec[j] = new VariableRecombination(getInheritance(j, 0, 1), theta);
        p.add(mrec[j]);
      }
    }

    return p;
  }

  public VariableRecombination recombination(int j, int k) {
    return k == 0 ? prec[j] : mrec[j];
  }

  public VariableRecombination recombination(int j, int k, int l) {
    if (l == 0) {
      return k == 0 ? precr[j] : mrecr[j];
    }
    else {
      return k == 0 ? precl[j] : mrecl[j];
    }
  }
}
