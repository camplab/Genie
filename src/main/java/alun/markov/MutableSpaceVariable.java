package alun.markov;

/**
 * Implements a variable whose state space can be reset.
 */
public class MutableSpaceVariable implements MutableStateSpace {
  private static int count = 0;
  private int s = 0;
  private int[] v = null;
  private int[] u = null;
  private int index = 0;

  public MutableSpaceVariable(int[] x) {
    setStates(x);
    index = count++;
  }

  public MutableSpaceVariable(int n) {
    this(arrayof(n));
  }

  private static int[] arrayof(int n) {
    int[] x = new int[n];
    for (int i = 0; i < x.length; i++) {
      x[i] = i;
    }
    return x;
  }

  public void setStates(int[] x) {
    v = x;
    int m = 0;
    for (int i = 0; i < v.length; i++) {
      if (m < v[i]) {
        m = v[i];
      }
    }
    u = new int[m + 1];
    for (int i = 0; i < u.length; i++) {
      u[i] = -1;
    }
    for (int i = 0; i < v.length; i++) {
      u[v[i]] = i;
    }
  }

  public void setStates(Function f) {
    int n = 0;
    for (s = 0; s < v.length; s++) {
      if (f.getValue() > 0) {
        n++;
      }
    }
    int[] vv = new int[n];
    n = 0;
    for (s = 0; s < v.length; s++) {
      if (f.getValue() > 0) {
        vv[n++] = v[s];
      }
    }
    setStates(vv);
  }

  public String toString() {
    return "IV" + index;
  }

  public void init() {
    s = -1;
  }

// Private data.

  public boolean next() {
    if (++s == v.length) {
      s = -1;
      return false;
    }
    else {
      return true;
    }
  }

  public final int getState() {
    return v[s];
  }

  public final int getStateIndex() {
    return s;
  }

  public final void setStateIndex(int i) {
    s = i;
  }

  public final int getNStates() {
    return v.length;
  }

  public final boolean setState(int x) {
    if (x < 0 || x >= u.length || u[x] < 0) {
      return false;
    }
    else {
      s = u[x];
      return true;
    }
  }
}
