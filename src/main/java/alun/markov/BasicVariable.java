package alun.markov;

public class BasicVariable implements Variable {
  private static int count = 0;
  private int n = 0;
  private int s = 0;
  private int index = 0;

  public BasicVariable(int ns) {
    n = ns;
    s = -1;
    index = count++;
  }

  public void init() {
    s = -1;
  }

  public final int getState() {
    return s;
  }

  public final int getStateIndex() {
    return s;
  }

  public final void setStateIndex(int i) {
    setState(i);
  }

  public final boolean setState(int x) {
    if (x < 0 || x >= n) {
      return false;
    }
    s = x;
    return true;
  }

// Private data.

  public final int getNStates() {
    return n;
  }

  public void setNStates(int i) {
    n = i;
  }

  public boolean next() {
    if (++s == n) {
      s = -1;
      return false;
    }

    return true;
  }

  public String toString() {
    return "V" + index;
  }
}
