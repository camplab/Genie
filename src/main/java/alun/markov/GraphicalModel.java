package alun.markov;

import alun.graph.Decomposition;

import java.util.*;

public class GraphicalModel {
  protected Clique[] jtree = null;
  protected LinkedHashMap<Variable, Table> marg = null;

  public GraphicalModel(Product p) {
    this(p, (Collection<Variable>) null);
  }

  public GraphicalModel(Product p, Variable keep) {
    this(p, wrapper(keep));
  }

  public GraphicalModel(Product p, Collection<Variable> keep) {
    Decomposition<Variable> d = null;
    if (keep == null) {
      d = new Decomposition<Variable>(p);
    }
    else {
      d = new Decomposition<Variable>(p, keep);
    }

    ArrayList<LinkedHashSet<Variable>> v = new ArrayList<LinkedHashSet<Variable>>(d.getVertices());
    LinkedHashMap<LinkedHashSet<Variable>, Clique> h = new LinkedHashMap<LinkedHashSet<Variable>, Clique>();
    jtree = new Clique[v.size()];
    for (int i = v.size() - 1; i >= 0; i--) {
      jtree[i] = makeClique(v.get(i), h.get(d.next(v.get(i))), p);
      h.put(v.get(i), jtree[i]);
    }

    Set<Function> f = p.getFunctions();
    for (int i = 0; i < v.size(); i++) {
      LinkedHashSet<Function> g = p.getFunctions(jtree[i].peeled());
      g.retainAll(f);
      f.removeAll(g);
      jtree[i].inputs().addAll(g);
    }
  }

  protected static LinkedHashSet<Variable> wrapper(Variable k) {
    LinkedHashSet<Variable> s = new LinkedHashSet<Variable>();
    s.add(k);
    return s;
  }

  public void allocateOutputTables() {
    for (int i = 0; i < jtree.length; i++) {
      jtree[i].setOutputTable(new DenseTable(jtree[i].cutset()));
    }
  }

  public void clearOutputTables() {
    for (int i = 0; i < jtree.length; i++) {
      jtree[i].setOutputTable(null);
    }
  }

  public void allocateInvolTables() {
    for (int i = 0; i < jtree.length; i++) {
      jtree[i].setInvolTable(new SparseTable(jtree[i].invol()));
    }
  }

  public void clearInvolTables() {
    for (int i = 0; i < jtree.length; i++) {
      jtree[i].setInvolTable(null);
    }
  }

  public double peel() {
    double x = 1;
    for (int i = 0; i < jtree.length && x > 0; i++) {
      x *= jtree[i].peel();
    }
    return x;
  }

  public double logPeel() {
    double x = 0;
    for (int i = 0; i < jtree.length; i++) {
      x += Math.log(jtree[i].peel());
    }
    return x;
  }

  public double log10Peel() {
    return logPeel() / Math.log(10);
  }

  public void collect() {
    for (int i = 0; i < jtree.length; i++) {
      jtree[i].collect();
    }
  }

  public void max() {
    for (int i = 0; i < jtree.length; i++) {
      jtree[i].max();
    }
  }

  public void drop() {
    for (int i = jtree.length - 1; i >= 0; i--) {
      jtree[i].drop();
    }
  }

  public void findMarginals() {
    marg = new LinkedHashMap<Variable, Table>();
    collect();
    for (int i = jtree.length - 1; i >= 0; i--) {
      marg.putAll(jtree[i].distribute());
    }
  }

  public Function getMarginal(Variable v) {
    return marg.get(v);
  }

  public void reduceStates() {
    findMarginals();
    for (Iterator<Variable> i = marg.keySet().iterator(); i.hasNext(); ) {
      Variable v = i.next();
      if (v instanceof MutableStateSpace) {
        ((MutableStateSpace) v).setStates(marg.get(v));
      }
    }
  }

  public Clique makeClique(Set<Variable> v, Clique next, Product p) {
    return new BasicClique(v, next);
  }

  public String toString() {
    StringBuffer s = new StringBuffer();
    for (int i = 0; i < jtree.length; i++) {
      s.append(jtree[i] + "\n");
    }
    if (jtree.length > 0) {
      s.deleteCharAt(s.length() - 1);
    }
    return s.toString();
  }

  public double getMax() {
    return jtree[jtree.length - 1].getOutputTable().getValue();
  }

// Private data.

  public Function getFinal() {
    return jtree[jtree.length - 1].getInvolTable();
  }

  public Product getFinals() {
    Product p = new Product();
    p.add(jtree[jtree.length - 1].inputs());
    for (Iterator<Clique> i = jtree[jtree.length - 1].previous().iterator(); i.hasNext(); ) {
      p.add(i.next().getOutputTable());
    }
    return p;
  }

  public Product meanGetFinals() {
    for (int i = 0; i < jtree.length - 1; i++) {
      jtree[i].setOutputTable(new DenseTable(jtree[i].cutset()));
      jtree[i].peel();
      for (Iterator<Clique> j = jtree[i].previous().iterator(); j.hasNext(); ) {
        j.next().setOutputTable(null);
      }
    }

    Product p = new Product();
    p.add(jtree[jtree.length - 1].inputs());
    for (Iterator<Clique> i = jtree[jtree.length - 1].previous().iterator(); i.hasNext(); ) {
      p.add(i.next().getOutputTable());
    }
    return p;
  }
}
