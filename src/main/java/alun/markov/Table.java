package alun.markov;

import java.util.Collection;

/**
 * A Table is an implementation of a Function that allows the
 * value associated with the states of the Variables to be
 * set and altered.
 */
abstract public class Table extends CompoundVariable implements Function {
  public Table(Collection<? extends Variable> vv) {
    super(vv);
  }

  /**
   * Returns the value associated with the current states of the variables.
   */
  abstract public double getValue();

  /**
   * Sets the value associated with the current states of the variables.
   */
  abstract public void setValue(double d);

  /**
   * Increases the value associated with the current states of the variables
   * by the given amount.
   */
  abstract public void increase(double d);

  /**
   * Multiplies the value associated with the current states of the variables
   * by the given amount.
   */
  abstract public void multiply(double d);

  /**
   * Sets the value associated with each state to zero.
   */
  abstract public void initToZero();

  /**
   * Returns the sum over all possible variables states of the values of the
   * function.
   */
  abstract public double sum();

  /**
   * Multiplies all possible values of the function by d.
   */
  abstract public void scale(double d);

  abstract public int size();

/*
	public void remove()
	{
		setValue(0);
	}
*/

  public String toString() {
    return "TABLE " + getVariables().toString();
  }
}
