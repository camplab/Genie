package alun.markov;

import java.util.Collection;

public class DenseGraphicalModel extends GraphicalModel {
  public DenseGraphicalModel(Product p) {
    super(p);
  }

  public DenseGraphicalModel(Product p, Variable v) {
    super(p, v);
  }

  public DenseGraphicalModel(Product p, Collection<Variable> v) {
    super(p, v);
  }

  public void allocateOutputTables() {
    for (int i = 0; i < jtree.length; i++) {
      jtree[i].setOutputTable(new DenseTable(jtree[i].cutset()));
    }
  }

  public void allocateInvolTables() {
    for (int i = 0; i < jtree.length; i++) {
      jtree[i].setInvolTable(new DenseTable(jtree[i].invol()));
    }
  }
}
