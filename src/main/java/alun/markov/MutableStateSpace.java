package alun.markov;

/**
 * This interface is for Variables whose state space can be changed.
 */
public interface MutableStateSpace extends Variable {
  /**
   * Sets the states of the variable to those which are associated
   * with non zero values of the given function.
   */
  public void setStates(Function f);
}
