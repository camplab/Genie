import alun.genepi.LinkageVariables;
import alun.genio.GeneticDataSource;
import alun.genio.LinkageDataSet;
import alun.genio.LinkageInterface;
import alun.mcld.*;
import alun.util.InputFormatter;
import alun.view.FrameQuitter;
import alun.viewgraph.DynamicMap;

import java.awt.*;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

/**
 * This program estimates a graphical model for linkage disequilibrium
 * or allelic associateion from genetic data.
 * There are a number of ways to use it.
 * <ul>
 * Usage : <b> java HapGraph < haplotypes </b>
 * </ul>
 * where
 * <ul>
 * <li> <b> haplotypes </b> is a file containing observed or reconstructed haplotypes.
 * The file must contain a line for each haplotype with the alleles of each haplotype
 * separated on the line by white space. The alleles must be specified as positive
 * integers.
 * </li>
 * <p>
 * Used this way, with the data being read from the standard input file, the program
 * will assume that the haplotypes are perfectly observed with no missing data.
 * </ul>
 *
 * <br>
 * <ul>
 * Usage : <b> java HapGraph haplotypes </b>
 * </ul>
 * where
 * <ul>
 * <li> <b> haplotypes </b> is a file containing observed or reconstructed haplotypes.
 * The file is as specified above except that 0 can be used to indicate a missing value.
 * </li>
 * Used this way, with the data being read from a file named on the command line,
 * the program will iterate between inferring the missing data given the current
 * graphical model for LD and  re estimating the graphical model given the
 * imputed data.
 * </ul>
 *
 * <br>
 * <ul>
 * Usage : <b> java HapGraph input.par input.ped [-ped] [-1]</b>
 * </ul>
 * where
 * <ul>
 * <li> <b> input.par </b> is a LINKAGE paramter file </li>
 * <li> <b> input.ped </b> is a LINKAGE pedigree file </li>
 * Used in this way, HapGraph will estimate a graphical model for linkage
 * disequilibrium from diploid data specifed in the linkage files.
 * Again the program iterates between inferring complete, phase know,
 * data given the current model for LD and re estimating the model
 * given the currently imputed phase known data.
 *
 * <p>
 * The default mode is to assume that the individuals in the pedigree file
 * are an unrelated population sample regardless of any pedigree information
 * in the file. This allows far faster computations
 * to be made because each individual's complete phase known data can be
 * updated as a single blocked Gibbs update.
 * <p>
 * If the <b> -ped </b> option is specified the individuals are placed in a
 * pedigree as given in the input file. This requires that updating phases
 * involves several separate blocked Gibbs updates on the pedigree, which
 * is compuationally demanding.
 * <p>
 * If the <b> -1 </b> option is specified the first locus is ignored. This
 * is because it is often a putative disease locus and an LD model may be
 * wanted for marker loci only. For creating an LD model for inputting
 * to McLink use
 * <ul>
 * <b> java HapGraph input.par input1.ped -1 </b>
 * </ul>
 * to create an LD model in the file <b> ld.model </b> say.
 * Then run McLink using
 * <ul>
 * <b> java McLink input.par input2.ped 1000 -v ld.model </b>
 * </ul>
 * or similar.
 * <b> input1.ped </b> and <b> input2.ped </b> are a random population
 * sample of genotyped individuals suitable for estimating an LD model from,
 * and a pedigree on which to perform linkage analysis with LD.
 * </ul>
 *
 * <p>
 * The mouse, key and button controls are similar to those for
 * <a href="ViewGraph.html"> ViewGraph </a>
 * with a few extra options.
 * <ul>
 * <li> An additional slider bar controls the simulated annealing temperature.</li>
 * <li> Additional text fields show the number of iterations, current score, best score,
 * and time taken.</li>
 * <li> There is an additional button to stop the search. Once stopped, further buttons
 * appear to control output.</li>
 * <li> There is a button to output reconstruted haploytpes. There is one haplotype per
 * line. If haplotypes were input the output is in the same order. If diplotypes
 * were input there are two reconstructed haplotypes for each diplotype in the
 * same order as input.</li>
 * <li> There is a button to output the estimated graphical model. There are other programs,
 * such as
 * <a href="HaploFreqs.html"> HaploFreqs </a>,
 * that read this output file and provide summaries.</li>
 * </ul>
 */

public class HapGraph {
  public static void usage() {
    System.err.println("Usage:");
    System.err.println("\t java HapGraph linkage.parfile linkage.pedfile -ped [-1]");
    System.err.println("\t java HapGraph linkage.parfile linkage.pedfile [-1]");
    System.err.println("\t java HapGraph haplotypefile");
    System.err.println("\t java HapGraph < perfecthaplotypefile");
    System.exit(1);
  }

  public static void main(String[] args) {
    try {
      int option = 0;
      int metropergibbs = 10000;
      int reportrate = 100;
      int firstlocus = 0;

      HaplotypeSource haps = null;

      switch (args.length) {
        case 3:
          if (args[2].equals("-ped")) {
            LinkageDataSet dat3 = new LinkageDataSet(args[0], args[1]);
            GeneticDataSource gds3 = new LinkageInterface(dat3);
            haps = new LDSampler(new LinkageVariables(gds3), firstlocus);
            if (!gds3.locName(0).equals("")) {
              int i = firstlocus;
              for (Locus l : haps.getLoci()) {
                l.setName(gds3.locName(i++));
              }
            }

            break;
          }
          else if (args[2].equals("-1")) {
            firstlocus = 1;
          }
          else {
            usage();
          }

        case 2:
          LinkageDataSet dat2 = new LinkageDataSet(args[0], args[1]);
          GeneticDataSource gds2 = new LinkageInterface(dat2);
          haps = new Diplotypes(gds2, 0.0, firstlocus);
          if (!gds2.locName(0).equals("")) {
            int i = firstlocus;
            for (Locus l : haps.getLoci()) {
              l.setName(gds2.locName(i++));
            }
          }
          break;

        case 1:
          BufferedReader buff1 = new BufferedReader(new FileReader(args[0]));
          buff1.mark(10000);
          StringTokenizer st1 = new StringTokenizer(buff1.readLine());
          if (!st1.nextToken().equals("Labels:")) {
            st1 = null;
            buff1.reset();
          }

          haps = new Haplotypes(new InputFormatter(buff1), 0);

          if (st1 != null) {
            for (Locus l : haps.getLoci()) {
              l.setName(st1.nextToken());
            }
          }
          break;

        case 0:
          BufferedReader buff0 = new BufferedReader(new InputStreamReader(System.in));
          buff0.mark(10000);
          StringTokenizer st0 = new StringTokenizer(buff0.readLine());
          if (!st0.nextToken().equals("Labels:")) {
            st0 = null;
            buff0.reset();
          }

          haps = new PerfectHaplotypes(new InputFormatter(buff0));

          if (st0 != null) {
            for (Locus l : haps.getLoci()) {
              l.setName(st0.nextToken());
            }
          }
          break;

        default:
          usage();
      }

      GraphMHScheme sch = null;
      switch (option) {
        case 0:
          sch = new DecomposableSearch(haps.getLoci());
          metropergibbs = 10000;
          reportrate = 100;
          break;

        case 1:
          sch = new IntervalSearch(haps.getLoci());
          metropergibbs = 100;
          reportrate = 10;
          break;

        case 2:
          sch = new TiedIntervalSearch(haps.getLoci());
          metropergibbs = 100;
          reportrate = 10;
          break;
      }

      JointScheme search = new JointScheme(haps, sch);
      System.err.println("BIC parameter is " + search.getParameterPenalty());

      DiploGUI gui = new DiploGUI(search);
      gui.setMap(new DynamicMap<Locus>(sch.getGraph()));
      gui.getCanvas().setBackground(new Color(150, 200, 225));

      Frame fr = new Frame();
      fr.addWindowListener(new FrameQuitter());
      fr.add(gui);
      fr.pack();
      fr.setVisible(true);

      gui.start();

      long starttime = System.currentTimeMillis();

      for (int i = 1; !gui.getDone(); i++) {
        search.metropolisUpdate(gui.getAnneal());

        if (i % reportrate == 0) {
          gui.showIterations(i);
          gui.showCurrent(search.getCurrentScore());
          gui.showBest(search.getBestScore());
          gui.showTime(System.currentTimeMillis() - starttime);
        }

        if (i % metropergibbs == 0) {
          gui.flash(Color.red);
          if (search.gibbsUpdate(gui.getAnneal())) {
            gui.showBest(search.getBestScore());
          }
          gui.flash(Color.yellow);
        }
      }

      gui.showTime(System.currentTimeMillis() - starttime);
      gui.setMap(new DynamicMap<Locus>(search.getBestGraph()));
      gui.showCurrent(search.getBestScore());
    } catch (Exception e) {
      System.err.println("Caught in HapGraph:main()");
      e.printStackTrace();
    }
  }
}
