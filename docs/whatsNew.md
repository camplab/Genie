# What's New in Geniea

New in Genie3.0.0
-----------------

Added build system

Gradle (version 6)

Added SCV system

git

Added public code repository

[Genie@gitlab](https://gitlab.com/camplab/PedGenie)

Added Jira issue tracker

[Genie@campjira](https://campjira.chpc.utah.edu/projects/GNE/issues)

New in Genie2.7.2
-----------------

GUI

Allow user to upload an existing .rgen parameter file for modification

Interval Check

New attribute intervalcheck, if set to "yes", an automatic simulation interval check will take place at 1000 x 10^n simulations, where n starts from 0, increment by 1 at each interval. The simulation will stop if there are 10 or more simulated test result >= observed result.

Re-code allele

New attribute decode, default set to "no". There are 2 ways user can use this option. Option 1. User can set up the analysis table with allele code 1 = common and 2 = rare, regardless of the input data format and frequencies. Genie will calulate the observed data allele frequencies and re-code the analysis table pattern allele 1 to match the common allele in the observed data set, and 2 to match the rare allele. Option 2. Use both attribute decode="yes" and decodefile="filename". User can specify a file with re-coding allele codes and their frequencies for all markers. Based on these frequencies Genie will convert analysis table pattern accordingly.

Results formatting

All test results will be output in scientific notation.

X link Association Test

In order to preform X link Association test, the top and drop simulation should setup as, top="XTopSim" and drop="XDropSim". All analysis table should setup to run Allelic test, type="allele". During the process of binning individual alleles into the correct table cells for the analysis, all male individuals will assume as homozygotes and will only be counted once. And they will also be gene-dropped as homozygotes at each simulation.

New in Genie2.6.7
-----------------

new GUI

for setting up .rgen parameter file.

Error all tables are zero

This error is related to Meta analysis. Fixed August 4, 09.

Error: SibTDT statistic

Selected incorrected value for #M affected, it should be #m affected. Fixed September 5, 09

New in Genie2.6.3
-----------------

Gene-Gene Interactions

Added new 2x2 Gene-Gene Interactions Test.

Weighted Index

Uses simulation to calculate all individuals weighted index. Available statistics : Chi-squared, Chi-Squared Trend, Quantitative - difference in means test.

IS\_TOGETHER

New PedQuery.Predicate IS\_TOGETHER, for selecting both case and control individuals who have genotype data.

New in Genie2.6.2
-----------------

Hardy-Weinburg Equilibrium

Added new Hardy-Weinburg Equilibrium Test.

Q Test for Odds Ratio

Added new Q test statistic for hetergeneity.

Loci range

Allow user to define a loci range for a subset analysis

Odds Ratio

The OddsRatios statistic has been changed, Confidence Intervals calculation has been removed to improve preformance.

Odds Ratio with Confidence Intervals

Added new OddsRatiosWithCI for odds ratio statistic with Confidence Intervals.

Meta Odds Ratio

The Meta OddsRatios statistic has been changed, Confidence Intervals calculation has been removed to improve preformance.

Meta Odds Ratio with Confidence Intervals

Added new MetaOddsRatiosWithCI for odds ratio statistic with Confidence Intervals.

[Home](index)  [PedGenie](PedGenieDetail)
