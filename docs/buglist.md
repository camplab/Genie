Genie Bug List table.setfont { font-size: 10pt }

Genie Bug List
--------------

Reported : Ryan

Date : August 13, 2007

Specification.ColumnImp.subsumeGtype()

For analysis type = "Allele" and CCTable has more than one pattern group for matching. Failed to test the second Allele or Haplotype against the other pattern group, if the first allele or haplotype matched successfullly against the first pattern group.  
Fixed - August 14, 2007. send each allele or haplotype separately through column pattern group.

[Home](index.html)
