# Referencing Genie Components

Please cite:

Allen-Brady K, Wong J, Camp NJ (2006).

[PedGenie: An Analysis Approach for Genetic Association Testing in Extended Pedigrees and Genealogies of Arbitrary Size.](articles/1471-2105-7-209.pdf)

BMC Bioinformatics 2006 7:209

Curtin K, Wong J, Allen-Brady K, Camp NJ (2007).

[PedGenie: Meta Genetic Association Testing in Mixed Family and Case-Control Designs.](articles/1471-2105-8-448.pdf)

BMC Bioinformatics 2007 8:448

Curtin K, Wong J, Allen-Brady K, Camp NJ (2007).

[Meta-genetic association of rheumatoid arthritis and PTPN22 using PedGenie 2.1](articles/1753-6561-1-S1-S12.pdf)

BMC Proc. 2007;1 Suppl 1:S12. Epub 2007 Dec 18

[Home](index.html)
