# PedGenie / QTDT Interface Home Page  

PedGenie interfaces with the freely available [QTDT](http://www.sph.umich.edu/csg/abecasis/QTDT/) statistical package to run the following quantitative transmission disequilibrium tests TDTQ5 [(Allison 1997)](References.html#Allison1997), [(Rabinowitz 1997)](References.html#Rabinowitz1997), and [(Monks et al 1998)](References.html#Monks1998). These traditional statistical tests typically allow only a single trio or a single sibship to be present in a kindred. PedGenie allows multiple trios and/or multiple sibships to be present in a kindred, as well as multiple affected/unaffected siblings to be present within a family. By generating an empirical p-value that corrects for familial correlations, PedGenie is able to handle pedigrees of any size. PedGenie communicates the real data and each simulation to the QTDT package, and an empirical p-value is calculated. The QTDT package contains other tests, such as [(Abecasis et al. 2000)](References.html#Abecasis2000) and [(Fulker et al 1999)](References.html#Fulker1999) as part of the package, but these tests require an inherited-by-descent (IBD) file to generate valid results. As PedGenie does not utilize an IBD file, interfacing with these tests is not recommended.

[How to Execute QTDT Interface](QTDTRequirements.html)

1.  Execute PedGenie to create TDT Dump files
2.  Run each of the dump files thru the modified QTDT program to produce a single output file.
3.  Execute the Pval package with the modified QTDT output file.

Output Report

Output Report will be written to \[Pedigree file name\].\[rgen parameter file name\].out.pval

[QTDT Interface Example](QTDTExample.html)

[Download Software](download.html)

Development Software

This QTDTpval package was developed in Java version 1.4.

[Bug List](buglist.html)   [FAQ](FAQ.html)   [PedGenie](index.html)   [Genetic Epidemiology](http://bioinformatics.med.utah.edu)
