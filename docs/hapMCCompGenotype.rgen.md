# Example .rgen: hapMC Comp. Genotype

<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE ge:rgen SYSTEM "hapMC-rgen.dtd">

<ge:rgen rseed="777" nsims="2000" >

  <ge:locus id="1" marker="SNP1"/>
  <ge:locus id="2" marker="SNP2" dist="0.00" />  <!-- option: dist="(theta or cM)" -->
  <ge:datafile studyname="Testing" genotypedata="./GenotypeData.dat" quantitative="./Trait.dat"/>
  
  <!-- optional modules -->
  <ge:param name="ccstat1">ChiSquared</ge:param>
  <ge:param name="ccstat2">ChiSquaredTrend</ge:param>
  <ge:param name="ccstat3">OddsRatios</ge:param>
  <ge:param name="ccstat4">Quantitative</ge:param>
  <ge:param name="covar1">1</ge:param>

  <!-- other optional parameters -->
  <ge:param name="top-sample">all</ge:param> 


<!-- SNP1-SNP2 tested jointly -->
<!-- Note: Within a defined <ge:g> block, each <ge:a> line listed is joined --> 
<!-- together by an 'and' statement.  Between <ge:g> blocks are 'or' statements. -->
<!-- Thus below, in the first <ge:g> block, the first <ge:a> line requires that the -->
<!-- first locus in the 'id' list above is a (1/1) genotype AND the second <ge:a> line  -->
<!-- states that the second locus can be any genotype.  Also within this weight -->
<!-- (the lowest weight is always the reference group), the reverse scenario is listed. -->
<!-- The comparison group (wt=1), requires a '2' in at least one position for both the first -->
<!-- and second loci.  The 'or' statement (i.e., '|') allows for unphased genotype data. --> 
<!-- All statistics in the above 'ccstat' list will be run as no 'stats' are defined.  -->


 <!-- Dom-Dom Model -->
 <ge:cctable model="Dom-Dom">
    <ge:col wt="0">
      <ge:g>
        <ge:a>1/1</ge:a>
	  <ge:a>./.</ge:a>
      </ge:g>
      <ge:g>
        <ge:a>./.</ge:a>
	  <ge:a>1/1</ge:a>
      </ge:g>
    </ge:col>
    <ge:col wt="1">
      <ge:g>
	  <ge:a>(2/.)|(./2)</ge:a>
	  <ge:a>(2/.)|(./2)</ge:a>
      </ge:g>
    </ge:col>
  </ge:cctable>


<!-- Rec-Rec Model -->
 <ge:cctable model="Rec-Rec">
    <ge:col wt="0">
      <ge:g>
        <ge:a>(1/.)|(./1)</ge:a>
	  <ge:a>(./.)</ge:a>
      </ge:g>
      <ge:g>
        <ge:a>(./.)</ge:a>
	  <ge:a>(1/.)|(./1)</ge:a>
      </ge:g>
    </ge:col>
    <ge:col wt="1">
      <ge:g>
	  <ge:a>(2/2)</ge:a>
	  <ge:a>(2/2)</ge:a>
      </ge:g>
    </ge:col>
  </ge:cctable>



<!-- Dom & Rec -->
<ge:cctable model="Dom-Rec">
    <ge:col wt="0">
      <ge:g>
        <ge:a>1/1</ge:a>
	  <ge:a>(./.)</ge:a>
      </ge:g>
      <ge:g>
        <ge:a>./.</ge:a>
	  <ge:a>(1/.)|(./1)</ge:a>
      </ge:g>
    </ge:col>
    <ge:col wt="1">
      <ge:g>
	  <ge:a>(1/2)|(2/1)</ge:a>
	  <ge:a>(2/2)</ge:a>
      </ge:g>
      <ge:g>
        <ge:a>(2/.)|(./2)</ge:a>
	  <ge:a>(2/2)</ge:a>
      </ge:g>
    </ge:col>
  </ge:cctable>


<!-- Rec & Dom -->
<ge:cctable model="Rec-Dom">
    <ge:col wt="0">
      <ge:g>
        <ge:a>(./.)</ge:a>
	  <ge:a>1/1</ge:a>
      </ge:g>
      <ge:g>
        <ge:a>(1/.)|(./1)</ge:a>
	  <ge:a>(./.)</ge:a>
      </ge:g>
    </ge:col>
    <ge:col wt="1">
      <ge:g>
	  <ge:a>(2/2)</ge:a>
	  <ge:a>(2/.)|(./2)</ge:a>
      </ge:g>
    </ge:col>
  </ge:cctable>

</ge:rgen>
