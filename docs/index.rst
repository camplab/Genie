.. Genie documentation master file, created by
   sphinx-quickstart on Fri Sep 11 12:57:13 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Genie's documentation!
=================================

.. toctree::

  buglist.md
  contact.md
  download.md
  Examples.md
  FAQ.md
  FrequencyDesc.md
  genieIndex.md
  hapConstructor2.0_example.md
  hapConstructor2.0_rgen.md
  hapconstructor.details.md
  hapConstructor.md
  hapMC2_example.md
  hapMC2_rgen.md
  hapMCCompGenotypeExample.md
  hapMCCompGenotype.report.md
  hapMCCompGenotype.rgen.md
  hapMCDetail.md
  hapMCExamples.md
  hapMCHaplotypeExample.md
  hapMCHaplotype.report.md
  hapMCHaplotype.rgen.md
  MetaExample.md
  PedGenieCompGenotypeExample.md
  PedGenieDetail.md
  PedGenieExamples.md
  PedGenieHaplotypeExample.md
  PedGenieHaplotype.rgen.md
  pedigreeDesc.md
  Publications.md
  QTDTExample.md
  QTDTinterface.md
  QTDTRequirements.md
  References.md
  Requirements.md
  rgenDescription.md
  SingleGeneCountExample.md
  SingleLocusExample.md
  SingleLocus.rgen.md
  topSim.md
  ToReferenceGenie.md
  traitDesc.md
  WeightedIndexExample.md
  WeightingAlgorithm.md
  whatsNew.md
  


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
