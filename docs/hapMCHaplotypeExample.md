# hapMC Haplotype Detail Example

The Haplotype example allows a user to examine the joint inheritance of two or more markers, using phased data. A haplotype frequency file is required for this analysis. We have also included quantitative data in this analysis. If you have problems opening a file, at the error page try using toolbar _View_, _Source_ or click on _Page_, _View Source_ to view the file.

Files required :

[GenotypeData.dat](GenotypeData.dat) - Linkage pre-makeped pedigree file with a header line  
[Trait.dat](Trait.dat) - Trait / Covariate data file  
[hapMCHaplotype.rgen](hapMCHaplotype.rgen) - .rgen Parameter File

Output file :

[hapMCHaplotype.report](hapMCHaplotype.report)

To run this analysis:

_java -jar Genie.jar hapMC hapMCHaplotype.rgen_

**GenotypeData.dat**  
The example [Genotype Data file](pedigreeDesc.html) is composed of 16 three generation families. Each individual has information on two markers.

**Trait.dat**  
The quantitative test requires a quantitative [trait file](traitDesc.html). It contains the trait and covariate information for each individual.

**Haplotype.rgen**  
The set-up of Haplotype.rgen is similar to the set-up of the [hapMCCompGenotype.rgen](hapMCCompGenotype.rgen). Only differences between the two files will be described in this section.

1.  In the locus section, loci are defined, as is the distance ('dist') between the loci. The 'dist' is defined as the distance between the marker and the proceeding marker. If the 'dist' is listed as a value <=0.5, it is assumed to be a recombination fraction. If the 'dist' is listed as a value >0.5 it is assumed to a value in centiMorgans (cM). The default value for 'dist' is 0.5, assuming linkage equilibrium between markers.
2.  In the first cctable, we compare haplotype 1-2 versus the most common haplotype 1-1. Under wt=0, we define all of the situations where a haplotype 1-1 is observed. We point out to users that within a defined block, each line listed is joined together by an 'AND' statement. Between blocks are 'OR' statements. Thus in the first block, the first line states that the first locus (SNP1) is a '1/1' genotype AND the second line states that the second locus (SNP2) also is the '1/1' genotype. In the second block, we require that the first locus (SNP1) be a '1/2' genotype and the second locus is the '1/1' genotype. We continue on, listing five situations where the 1-1 phased haplotype is observed. Under wt=1, we define all of the scenarios possible that include haplotype 1-2. Thus, in the first block, we require SNP1 to be the 1/1 genotype and SNP2 must have a '2' at either the first position or the second position \[(./2) | (2/.)\]. We list five scenarios where haplotype 1-2 is present.
3.  In the second and third cctable analyses, we illustrate how haplotype 2-2 can be compared to haplotype 1-1 and how haplotype 2-1 can be compared to haplotype 1-1.
4.  In the fourth cctable analysis, we illustrate how haplotype 1-2 can be compared to all other haplotypes. We take advantage of the wild card ('.') option available in PedGenie. The wild card option allows a user to exclude all patterns previously counted. Note that we begin with wt=1 rather than wt=0. The lowest weight group (in this case 'all other genotype combinations') is the reference group. We define the 1-2 haplotype, using the wild card option. Next, we define wt=0, and list for locus 1 (SNP1) that any other genotype is permissible (./.), and for SNP2 that any other genotype is permissible (./.). Although the reference group is wt=0, the order listed in the output file will be reversed. We have added to our Model name 'REV' as a reminder that the order in the output file is reversed.
5.  In the fifth and sixth cctable analyses, we illustrate how the 2-2 and 2-1 haplotypes can be compared to 'all other' haplotypes, respectively.
6.  In the seventh and eighth cctable analyses, we illustrate how the 2-2 haplotype can be analyzed in a dominant and recessive mode of inheritance, respectively.
7.  Finally in the ninth cctable analysis, we show the set-up for the additive test for haplotype 2-2. Three columns are defined, but in reverse order. All statistics available will be run, including the TDT tests.

**[hapMCHaplotype.report](hapMCHaplotype.report)** output file

We refer users to the [hapMCCompGenotype.report](hapMCCompGenotype.report) output for detailed information regarding the Haplotype.report output file. Only differences between the two files will be described here.

1.  We point out to the users that in Analysis5, the column weights were switched so that we take advantage of the wild card option in hapMC. Column 2 for this analysis is wt=0 and column 1 is wt=1.

[Home](index.html)  [hapMC](hapMCDetail.html)  [hapMC Examples](hapMCExamples.html)
