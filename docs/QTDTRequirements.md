# PedGenie / QTDT Interface Requirements

* How to Execute QTDT Interface

Download the freely available [QTDT](http://www.sph.umich.edu/csg/abecasis/QTDT/) software. We suggest that you change the top directory name from qtdt-version# to qtdtPval to reflect the PedGenie - QTDT Interface. Then overwrite QtdtUi.cpp file with our copy of the QtdtUi.cpp in qtdtPval/qtdt to parse the QTDT output. Recompile this new QTDT package. Download both PedGenie.jar and QTDTpval.jar packages. QTDTpval.jar computes the empirical p-value based on the QTDT output. In the .rgen parameter file be sure the dumper attribute value is listed as TDTDumper. We have written a simple kornshell script, [runQTDT.ksh](l404#runQTDT.ksh), that runs PedGenie to create the simulated data. The kornshell script communicates this simulated data plus the real data into the modified QTDT, then execute the QTDTpval.jar package to calculate the empirical p value, and creates the results report. The modified QTDT package also requires the presence of a QTDT specific data file and a trait file.

** Requirements

Java 1.8 or newer installed
PedGenie.jar: This Java archive file contains all PedGenie's class files.
QTDTpval.jar: Java archive file to calculate the empirical p value. It contains all QTDTpval's class files.
QtdtUi.cpp: Modified copy of the QtdtUi.cpp file
[.rgen Parameter File](rgenDescription): Parameter File for executing PedGenie
[Pedigree File](pedigreeDesc): A Linkage pre-makeped pedigree file
[Trait File](traitDesc): Trait / Covariate data file
[Abecasis QTDT data file](http://www.sph.umich.edu/csg/abecasis/QTDT/tour/tour1.html): QTDT data file

[PedGenie](index)   [QTDT Interface](QTDTinterface)   [download](download)
