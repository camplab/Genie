# Weighted Index Example

This example is similar to the Single Locus Example, but all the founders are assigned with an unique alleles, and gene-drop to the rest of the pedigrees for use in the [weighting algorithm](WeightingAlgorithm.htm). If you have problems opening a file, at the error page try using toolbar _View_, _Source_ or click on _Page_, _View Source_ to view the file.

Required Files :

[WeightedIndex.dat](WeightedIndex.dat) - Linkage pre-makeped pedigree file with a header line  
[WeightedIndex.rgen](WeightedIndex.rgen) - .rgen Parameter File

Output file :

[WeightedIndex.report](WeightedIndex.report)

To run this analysis:

_java -jar Genie.jar PedGenie WeightedIndex.rgen_

**WeightedIndex.dat**  
The example [WeightedIndex.dat](pedigreeDesc.html) is composed of 224 pedigrees 3 generation families of 4929 individuals with 1 marker information.

**WeightedIndex.rgen**  
The parameter is similar to the [SingleLocus.rgen](SingleLocus.rgen) file except the simulation method top="IndivWtTopSim" replaces the top="AlleleFreqTopSim" method. Please see notes on SingleLocus.rgen for other details.

**WeightedIndex.report** output file

This report file is similar to the SingleLocus.report, we refer users to the [SingleLocus.report](SingleLocus.report) output for detailed information regarding the SingleGeneCount.report output file.

[Home](index.html)  [PedGenie](PedGenieDetail.html)  [PedGenie Examples](PedGenieExamples.html)
