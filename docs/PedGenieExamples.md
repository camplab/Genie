# PedGenie Examples

[Single Locus Example](SingleLocusExample.html)

This example analyses each marker as a single locus.

[Single Locus GeneCountAllele Example](SingleGeneCountExample.html)

Similar to the Single Locus Example, but the allele frequency estimation method GeneCounter from the [GeneCountAlleles](http://bioinformatics.med.utah.edu/~alun/software/index.html) program is used.

[Composite Genotype Example](PedGenieCompGenotypeExample.html)

This example examines the joint inheritance of two or more markers, without considering phase of the data.

[Haplotype Example](PedGenieHaplotypeExample.html)

This example examines the joint inheritance of two or more markers, using phased data.

[Meta / CMH Analysis Example](MetaExample.html)

Example of Meta / CMH test statistics for multiple studies.

[QTDT Interface Example](QTDTExample.html)

This is a PedGenie - QTDT Interface example.

[Weighted Index Example](WeightedIndexExample.html)

This example is similar to the Single Locus Example, but the founders are assigned with unique alleles, and gene-drop to the rest of the pedigree for use in the [weighting algorithm](WeightingAlgorithm.html).

[Home](index.html)  [PedGenie](PedGenieDetail.html)
