# Genie References

Abecasis, G.R., Cardon, L.R. and Cookson, W.O. (2000a)

     A general test of association for quantitative traits in nuclear families. Am J Hum Genet, 66, 279-92.

Abecasis, G.R., Cookson, W.O. and Cardon, L.R. (2000b)

     Pedigree tests of transmission disequilibrium. Eur J Hum Genet, 8, 545-51.

Allison, D.B. (1997)

     Transmission-disequilibrium tests for quantitative traits. Am J Hum Genet, 60, 676-90

Fulker, D.W., Cherny, S.S., Sham, P.C., and Hewitt, J.K. (1999)

     Combined linkage and association analysis for quantitative traits. Am J Hum Genet, 64, 259-67

Monks, S.A., Kaplan, N.L. and Weir, B.S. (1998)

     A comparative study of sibship tests of linkage and/or association. Am J Hum Genet, 63, 1507-16

Rabinowitz, D. (1997)

     A transmission disequilibrium test for quantitative trait loci. Hum Hered, 47, 342-50

Spielman, R.S. and Ewens, W.J. (1998)

     A sibship test for linkage in the presence of association: the sib transmission/disequilibrium test. Am J Hum Genet, 62, 450-8

Spielman, R.S., McGinnis, R.E. and Ewens, W.J. (1993)

     Transmission test for linkage disequilibrium: the insulin gene region and insulin-dependent diabetes mellitus (IDDM). Am J Hum Genet, 52, 506-16

Thomas, A and Camp, N.J. (2006)

     Maximum likelihood estimates for allele frequencies and error rates from samples of related individuals by gene counting.

Zar, Jerrold H

     Biostatistical Analysis (4th Edition) - Trends among Proportions

Hall DB WR, Clarke WR, Jones MF (1997)

     Cochran-Mantel-Haenszel Techniques: Applications Involving Epidemiologic Survey Data. 1-31

Landis JR, Koch GG (1978)

     Average partial association in three-way contingency tables: a review and discussion of alternative tests. International Statistical Review 1978, 46(1978):237-254

[Home](index.html)
