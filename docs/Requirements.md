# Genie Requirements

* How to execute Genie

Genie is written in Java. Please make sure Java is installed. Download and unpack the archive file - Genie.zip - from gitlab. Add Genie.jar and genepi.jar to your CLASSPATH. Then set up a .rgen parameter file with all the required information. Together with the genotype data file, you can execute Genie as :

**java -jar Genie.jar analysis\_option \[.rgenfile\].rgen**

If you are running the Quantitative test, a Trait file must be included in the .rgen parameter file, with sub-element datafile and attribute of quantitative and the directory path and file name as the variable.

If you want to run a haplotype analysis, in the .rgen file the top attribute should equal "HapFreqTopSim". A frequency file must be included in the .rgen parameter file, with sub-element datafile and attribute of haplotype and the directory path and file name as the varible.

If you want to use the GeneCounterTopSim simulation option, from [GeneCountAlleles](References.html#Thomas2006) program, in the .rgen file the top attribute should specify "GeneCounterTopSim". A linkage parameter loci data file must be specify in the .rgen parameter file, with sub-element datafile and attribute of linkageparameter and the directory path and file name as the variable.

[Java 1.6](http://java.com/en/download/index.jsp)

Java 1.6 installed on your Java virtual machine

Genie.zip

This archive available from [gitlab](https://gitlab.com/camplab/PedGenie/-/pipelines)

[genepi.jar](http://www-genepi.med.utah.edu/~alun/index.html)

This Java archive file bundles all Alun Thomas's packages into a single archive file for easy access.

[.rgen parameter file](rgenDescription.html)

XML file defines parameters

[pedigree file](pedigreeDesc.html)

Linkage pre-makeped pedigree file

[trait file](traitDesc.html)

Trait / Covariate data file

[linkage loci parameter file](http://bioinformatics.med.utah.edu/~alun/software/index.html)

Linkage loci data desrciption file

[Frequency file](FrequencyDesc.html)

User defined Haplotype or Allele frequency file

To ensure proper installation of Genie, please download all example files and test run Genie prior to executing your own data. Compare your results with the provided output files. If your result is different from the provided example output, or you encountered errors when running the software, please check your Java 1.6 installation and re-download all Genie files.

**Run**

**Output file**

java -jar Genie.jar PedGenie SingleLocus.rgen

[SingleLocus.report](SingleLocus.report)

java -jar Genie.jar PedGenie SingleGeneCount.rgen

[SingleGeneCount.report](SingleGeneCount.report)

java -jar Genie.jar PedGenie CompositeGenotype.rgen

[PedGenieCompGenotype.report](PedGenieCompGenotype.report)

java -jar Genie.jar PedGenie Haplotype.rgen

[Haplotype.report](Haplotype.report)

[Home](index.html)
