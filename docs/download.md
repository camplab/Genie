# Download Software

All download archive files have been compressed by the **zip** command. After downloading the required file please decompress and restore it to its original form prior any execution.

To decompressed a _.zip_ file - **unzip _filename_**.  

[Genie.zip](Genie.zip)

A compressed archive file of Genie 2.7.2.1, including Genie.jar, genepi.jar, and all PedGenie and hapMC example files.

[GenieSource.zip](GenieSource.zip)

A compressed archive file with Genie2.7.2.1 source code only.

[hapc1.0.zip](hapc1.0.zip)

A compressed archive file of single gene hapConstructor 1.0, including hapConstructor1.0.jar, example files and post-processing script.

[hapc2.0.zip](hapc2.0.zip)

A compressed archive file of gene-gene hapConstructor 2.0, including hapConstructor2.0.jar, example files and post-processing script.

[PedGenie.zip](PedGenie.zip)

A compressed archive file of an old version of PedGenie 1.2.0.2, including PedGenie.jar and PedGenie example files.

[PedGenieSource.zip](PedGenieSource.zip)

A compressed archive file with all PedGenie 1.2.0.2 source code only ( including all required GeneCounter programs from GeneCountAlleles package )

[hapMC2.zip](hapMC2.zip)

A compressed archive file of an old verson of hapMC 2, including hapMC.jar and all hapMC example files.

[hapMC2Source.zip](hapMC2Source.zip)

A compressed archive file with all hapMC 2 source files only.

[QTDTpval.zip](QTDTpval.zip)

A compressed archive file including QTDTpval.jar, modified copy of QtdtUi.cpp, runQTDT.ksh and QTDT example files.

[QTDTpvalSource.zip](QTDTpvalSource.zip)

A compressed archive file with QTDTpval source code, modified copy of QtdtUi.cpp and runQTDT.ksh.

[GenieQTDTpval.zip](GenieQTDTpval.zip)

A compressed archive file with Genie 2.7.2.1, QTDTpval and genepi packages (GCHap programs) together with all required GeneCounter programs from GeneCountAlleles package, and all example files.

[PedGenieQTDTpval.zip](PedGenieQTDTpval.zip)

A compressed archive file with both PedGenie1.2.0.2 and QTDTpval packages together with all the required GeneCounter programs from GeneCountAlleles package, and both PedGenie and QTDTpval example files.

[Home](index.html)   [QTDT Interface](QTDTinterface.html)
